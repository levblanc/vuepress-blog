# Bitcoin 小史

<PublishDate date='2018-11-07' />

## Bitcoin (Satoshi Nakamoto, 2008)

Bitcoin 这个项目的目的（或者说本质）非常简单，就是一个**点对点电子货币系统**。希
望让两个人之间的转账可以直接进行，不需要通过银行或其他第三方机构。

如果你没有看过它的白皮书，下面是白皮书的 Title，作者其实已经说得非常明了：

![bitcoin-whitepaper](@blockchainImages/bitcoin-whitepaper.png)

很多人也许会对“白皮书”这个说法感到迷惑，其实就是一个项目说明书：

- 这个项目是做什么的
- 想要解决什么问题
- 怎么解决

那么，Bitcoin 如何解决“去掉中间商”这个问题呢？

它给出的答案是：利用一种叫做“区块链”的技术。

（我们可以先简单地把区块链理解为：一个记录着人与人之间转账记录的表格。）

区块链技术到底是怎么“去掉中间商”的呢？我们来看它的特点：

1. 每个记录**只能写入一次**，而且记录一旦写入以后，是**只读**的。不能改、不能删
   。
2. 网络上的每个节点，都是整个数据库的一个**完整备份**。

这两个特性，其实是在“强逼”交易双方自律，从而消除“受信任的第三方”。

首先，你要很确定这笔账是要转给什么人，多少金额，因为一旦转了，就是不可撤销的。

其次，区别于传统的数据库，任何人只要在电脑上跑一个 Bitcoin 程序，连上 Bitcoin 的
网络，就会成为网络上的一个节点，拥有整个数据库的完整备份。每当数据有变化，所有节
点都会更新数据。

也就是说：

1. 如果有人想使坏，尝试去改之前的记录，是无法做到的；
2. 如果有人尝试在数据库里面写入假的记录，所有节点都会知道，是谁，在什么时候，做
   的假。

第二个特性同时也是一个亮点：

如果你使用的是区块链网络，只要网络上还有一个节点在，你的数据就不会消失。

（在这里做一点延伸，买过域名的人都知道，域名是按年计费的。相信有心经营一个独立网
站的人，都会有过这样的伤感情绪：如果我死了，没人给我的域名续费，我的网站也就永远
消失了。而区块链似乎刚好可以解决这个问题。）

所以，这其实是一个程序员通过发动群众的力量共同建立网络，大家一齐守卫着一种简单的
纪律的故事。

（注：这个项目于 2009 年 1 月上线。）

## 资源

官方项目白皮书：[Bitcoin White Paper](https://bitcoin.org/bitcoin.pdf)

给普通人的白皮书
：[Bitcoin White Paper Made Simple](https://blockchainreview.io/wp-content/uploads/2018/02/Intrepid-Ventures-Bitcoin-White-Paper-Made-Simple-1.pdf)

Anders Brownworth 讲解区块链的视频
：[blockchain 101](https://andersbrownworth.com/blockchain/) （通俗易懂的讲解，
非常值得一看）

Anders 的 blockchain demo 项目代码
：[blockchain-demo](https://github.com/anders94/blockchain-demo) Anders 的
public/private keys & signing 项目代码
：[public-private-key-demo](https://github.com/anders94/public-private-key-demo)

<CopyRights />
