---
title: JS Interview Questions (Part 1)
date: 2019-02-17 
updated: 2019-02-17
categories: [front-end]
tags: [js, interview]
---

# JS Interview Questions Part 1

## Difference between `var` and `let` keywords?

[try in jsfiddle](https://jsfiddle.net/levblanc/vcug34hj/40/)

1. intro time

- `var` is there in JS since the beginning. 
- `let` is introduced in ES6/ES2015. 

2. function-scoped vs. block-scoped

- `var` is function-scoped, which means the variable dies at the end of the function. 
- `let` is block-scoped, which means the variable will be garbage collected at the end of the block. 

```js
function testOne() {
  var a = 3
  let b = 3

  if (true) {
    var a = 4 // same variable as outside (function-scoped)
    let b = 4 // different variable (block-scoped)
    console.log('var a in block:', a) // 4
    console.log('let b in block:', b) // 4
  }

  console.log('var a out of block:', a) // 4 (a's value changed)
  console.log('let b out of block:', b) // 3 (b maintains value)
}

testOne()
```

3. hoisting

- `var` gets hoisted at the top of the function. (`undefined` means variable initiated, just has no value defined)
- `let` is not hoisted.

```js
function testTwo() {
  if (true) {
    console.log('i before init:', i) // undefined
    console.log('j before init:', j) // ReferenceError: can't access lexical declaration `j' before initialization

    var i = 1
    let j = 2
  }

  // scoping difference
  // console.log('i:', i) // i: 1
  // console.log('j:', j) // ReferenceError: j is not defined
}

testTwo()
```

## Difference between `const` and `let` keywords?

- `const` keyword defines constants, which means after the first assignment of value, you cannot reassign the value.
- with `let` keyword, you can reassign as many times as you want, you can even change variable type.
- but if you use `const` to define an object, you can modify it (not reassign).

[try on jsfiddle](https://jsfiddle.net/levblanc/b3juzco6/12/)

```js
let a = '1'
a = 3
console.log('a', a) // 3

const b = 2
b = 4
console.log('b', b) // TypeError: invalid assignment to const `b'

const c = [1, 2]
c.push(3) // modify array
console.log('c', c) // [1, 2, 3]
```

## Difference between `==` and `===`?

- `==` compares value only
- `===` compares both value and type

[try on jsfiddle](https://jsfiddle.net/levblanc/mscwxg1n/2/)

```js
var a = '1'
var b = 1

console.log(a == b) // true
console.log(a === b) // false
```

## Difference between `undefined` and `null`?

They both represent empty value.

- if you init a variable but you don't give it a value, javascript automatically gives it a placeholder, that's `undefined`.

- but for `null` you will have to do it yourself. for example, you have a variable and you want to clear it up, you give it the value `null`.


```js
typeof(undefined) // undefined
typeof(null) // object
```

## What is the use of arrow functions

[try on jsfiddle](https://jsfiddle.net/levblanc/bkojLvd6/27/)

An arrow function does not have its own `this`. The `this` value of the enclosing lexical scope is used; arrow functions follow the normal variable lookup rules. So while searching for `this` which is not present in current scope they end up finding `this` from its enclosing scope.

```js
var AgeCount = function () {
  this.age = 1
  
  setTimeout(function() {
    this.age++
    
    console.log(this.age) // NaN
  }, 100)
}

var age = new AgeCount()
```

So it is good to use them when you want to retain the `this` context inside a function.

```js
var AgeCountWithFatArrow = function() {
  this.age = 1

  setTimeout(() => {
    this.age++

    console.log(this.age) // 2
  }, 100)
}

var age1 = new AgeCountWithFatArrow()
```

But not to use them as methods:

```js
var obj = {
  i: 10,
  arrowFunc: () => {
    console.log('arrowFunc this.i', this.i) // => undefined
    console.log('arrowFunc this', this)     // => Window {...} (or the global object)
  },
  func: function() {
    console.log('func this.i', this.i) // => 10
    console.log('func this', this)     // => Object {...}
  }
}

obj.arrowFunc(); 
obj.func(); 
```
---
References: 

[1] [MDN - var](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/var)

[2] [MDN - let](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/let)

[3] [MDN - Arrow functions](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Functions/Arrow_functions)