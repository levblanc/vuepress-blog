# 如何自定义 VS Code 编程用字体

<PublishDate date='2018-12-14'/>

Operator 这个字体刚一出来的时候，就见过国外的主播用。当时感觉并不大，就只觉得关键字用手写体还是挺酷的。再者，知道了这个字体的价格之后，作为屌丝的我就把这套字体完全抛诸脑后了。

![operator-mono-sample](@toolsImages/vscode/customize-fonts/operator-mono-sample.png)

（图片源：[Tomorrow and Tomorrow Night Operator Mono Theme](https://marketplace.visualstudio.com/items?itemName=chiragpat.tomorrow-and-tomorrow-night-operator-mono-theme)）

前两天突然间想换个不同颜色的主题，看到搜索结果里面有标明 Operator Mono 这样的，赶紧装了来试。结果发现，主题只是`font-family`里面把这个字体排在了首位（也就是优先使用这个字体），最重要的是你电脑上要安装有这个字体才行。

但是这些主题都标明支持这字体，要么就是很多人买了，要么…… 难道它现在免费了？赶紧去搜！

最后发现，这个世界对屌丝还是很残酷的。免费？不存在的！反倒是搜出了一堆国外程序员倒腾出来的替代方案。看了觉得效果还不赖，就自己在 VS Code 上跟着开始折腾了。

## 1. 下载并安装字体

整体的思路其实就是找到好看的正体和斜体，来替换 Operator Mono。

正体是 [Fira Code](https://github.com/tonsky/FiraCode)，斜体是 [Script12 BT](https://www.dafontfree.net/freefonts-script12-bt-f141942.htm)

两种字体下载好之后，安装到电脑上。

## 3. 修改编辑器的默认字体

`cmd + ,` 唤起 Settings 页，搜索 `font` 会列出所有相关的配置。

把 `font-family` 修改成 `Fira Code`。后续的字体大小也可以根据自己的喜好调整。

![editor-font-setting](@toolsImages/vscode/customize-fonts/editor-font-setting.png)

修改之后，编辑器的默认字体就是 `Fira Code` 了。

## 2. 安装 VS Code 魔改样式插件

[Custom CSS and JS Loader](https://marketplace.visualstudio.com/items?itemName=be5invis.vscode-custom-css) 这个插件可以让我们用自己的样式覆盖主题样式，但是似乎官方并不是太喜欢这种做法。所以第一次 Enable 之后，就会报错说'corrupted'。在 VS Code 的 title 栏，又会显示 '[Unsupported]' 这样…… 

第二个问题有人在 VS Code 的[这个 issue](https://github.com/Microsoft/vscode/issues/30556) 里面抱怨过，可是无果，官方直接锁掉了这个 issue，可见态度还是很坚决的。

我们的态度也是很坚定的，为了 sexy 的效果，必须用！

插件安装好之后，新建一个`css`文件，文件名、硬盘位置随意（自己最后记得就好），最后可以通过`file://`协议来引用。

我这里是希望在不同的主题下，斜体字都可以有效果。查看了 DOM 之后发现，每个主题都会在比较顶层的 warpper 会注入跟主题名称相关的 class。我常用的两个主题的 class 是：`monokai-theme-monokai-pro-vscode-themes-Monokai-Pro-json` 和 `Sujan-code-blue-themes-code-blue-theme-json`，所以以此来标记。大家请根据自己的情况来写。

```css
.monokai-theme-monokai-pro-vscode-themes-Monokai-Pro-json .mtk5,
.monokai-theme-monokai-pro-vscode-themes-Monokai-Pro-json .mtk12 {
  font-family: 'Script12 BT';
  font-size: 1.2em;
}

.Sujan-code-blue-themes-code-blue-theme-json .mtk11,
.Sujan-code-blue-themes-code-blue-theme-json .mtk23 {
  font-family: 'Script12 BT';
  font-size: 1.1em;
}
```

Tips: 可以通过 Help > Toggle Developer Tools 来查看 VS Code 的 DOM。

![open-dev-tools](@toolsImages/vscode/customize-fonts/open-dev-tools.png)


## 3. 在设置中引用魔改的 CSS

在`Settings` Tab 激活的状态写下，右手边会有这个花括号图标：

![open-settings-json](@toolsImages/vscode/customize-fonts/open-settings-json.png)

点击一下，可以打开 `settings.json`。在`USER SETTINGS`下，增加以下两条设置：

（注：`file://`的路径指向你刚刚创建的`css`文件路径。`file://`协议最后是两杠`//`，文件路径开头是根目录所以还有一个斜杠，一共三个斜杠，不要漏了。Win 用户请根据系统给出的路径来写。）

```json
"vscode_custom_css.imports": ["file:///Volumes/Transcend/documents/vscode/vscode_custome_style.css"],
"vscode_custom_css.policy": true
```

如图所示：

![customize-css-import](@toolsImages/vscode/customize-fonts/customize-css-import.png)

## 4. 启用自定义 CSS

`cmd + shift + p` 唤出 VS Code 的命令输入工具（command palette）。输入 **Enable Custom CSS and JS**，启用我们魔改的设置。

![enable-custom-css](@toolsImages/vscode/customize-fonts/enable-custom-css.png)

第一次启用后，VS Code 会报 'currupted' 的错，不用管，选择 **Don't show again**。

（注：插件开发者也提示，每次 VS Code 升级，都需要重新启用一下这个设置哦）

另外，如果你引用的文件出现任何权限相关的问题，可以参考插件开发者的[指引](https://marketplace.visualstudio.com/items?itemName=be5invis.vscode-custom-css)：

![permissions](@toolsImages/vscode/customize-fonts/permissions.png)

## 5. 最后效果

配置好了，来晒图吧。

截图所见是我自己魔改了一下 [Code Blue](https://github.com/sujan-s/code-blue) 这个主题的背景颜色之后的效果。弄完之后正在非常认真地考虑自己弄一个合心意的蓝色主题。ｂ（￣▽￣）ｄ

![final-1](@toolsImages/vscode/customize-fonts/final-1.png)

![final-2](@toolsImages/vscode/customize-fonts/final-2.png)

## 6. 思考延伸

走了一遍流程之后，你会发现，其实就是换了两个字体。那么，如果你对这两种字体不满意的时候，是不是也可以再替换成其它自己觉得好看的呢？

---

参考文章：

[1] [Multiple Fonts: Alternative to Operator Mono in VSCode](https://medium.com/@zamamohammed/multiple-fonts-alternative-to-operator-mono-in-vscode-7745b52120a0)

[2] [An alternative to Operator Mono font](https://medium.com/@docodemore/an-alternative-to-operator-mono-font-6e5d040e1c7e)

[3] [Free alternative to Operator Mono Italic Theme for VSCode ](https://github.com/open-source-ideas/open-source-ideas/issues/10)

<CopyRights />