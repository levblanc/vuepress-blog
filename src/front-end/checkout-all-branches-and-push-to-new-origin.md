---
title: "git: checkout all branches and push to new origin"
author: Levblanc
date: 2016-10-28 17:17:53
updated: 2018-12-18
categories: [coding]
tags: [git, branch]
cover: /images/math.jpg
---

# git: checkout all branches and push to new origin

Step by step on how to checkout all branches of a remote origin and push them all onto a new origin.

<!-- more -->

## 1. `cd` into your repo path

```bash
$ cd path/to/your/repo
```

## 2. create a script for tracking branches

save the following script under your repo's path:

```bash
#!/bin/zsh
for branch in `git branch -a | grep remotes/origin`; do
    # echo ${branch:15:${#branch}}
    # echo "origin/"${branch:15:${#branch}}
    git branch --track ${branch:15:${#branch}} "origin/"${branch:15:${#branch}}
done
```

**Be sure to comment out the echos to see the prints on terminal before executing the real track command!! All they did was to sub and concat strings return by the `git branch` command. You may have to change the starting and ending indexes, or modify the prefix on target origin branch.**

It will track all the remote branches to your machine in the following format:

origin branch name: `remotes/origin/develop`

local branch name: `develop`

So for each branch, the following command will be executed:

```bash
$ git branch --track develop origin/develop
```

## 3. fetch all branches

Under your repo path, run the following command to update all branches to the latest on your machine.

```bash
$ git fetch --all
```

If you have GUI apps like `SourceTree` on your machine, check and see whether all the branches are latest. 

**Note: If there are still badges telling you there are new commits to pull, pull them before you go on to the next step.**

## 4. create new origin

```bash
$ git remote add <new-origin-name> <new-origin-repo-url>
```

example:

```bash
$ git remote add my-awesome-new-origin http://new-origin-host.com/new-repo.git
```

## 5. push all local branches to new origin

```bash
$ git push --all my-awesome-new-origin
```

All done!

---
References:

[1] [git clone all remote branches locally](https://coderwall.com/p/0ypmka/git-clone-all-remote-branches-locally)

[2] [How to fetch all git branches](http://stackoverflow.com/questions/10312521/how-to-fetch-all-git-branches)

[3] [Push local Git repo to new remote including all branches and tags](http://stackoverflow.com/questions/6865302/push-local-git-repo-to-new-remote-including-all-branches-and-tags)


<CopyRights />