# ES6关键字在低版本浏览器报错原因及分析

<PublishDate date='2018-08-30' />

## `:// 表现`

这周，同事负责的客户端在低版本的iOS上报了个错：

```
Const declarations are not supported in strict mode
```

这让我非常疑惑，明明所有的 JS 文件都是经过 Babel 编译的，怎么就会报`const`的错呢？

同事说，她把那个页面引用过的插件一个个注释掉，手动排查出来，是某个插件的问题，只要不引用那个插件，就不会报这个错。

## `:// 解决`

嗯？插件的 JS 文件 Babel 不编译吗？

查了一下自己的 webpack 配置：

```javascript
{
  test: /\.js$/,
  loader: 'babel-loader',
  include: [
    resolve('src'), 
    resolve('test')
  ]
}
```

还确实是没有包含`node_modules`，所以编译完成后的 JS 文件可以找到`const`：

![exclude-pinch-zoom](@feImages/es6-keyword-error/exclude-pinch-zoom.png)

让同事改成下面这样就好了：

```javascript
{
  test: /\.js$/,
  loader: 'babel-loader',
  include: [
    resolve('src'), 
    resolve('test'),
    resolve('node_modules/pinch-zoom-js')
  ]
}
```

![include-pinch-zoom](@feImages/es6-keyword-error/include-pinch-zoom.png)

所以，如果遇到插件在低版本浏览器上报 ES6 关键字的错误，可以尝试排查出问题的源头，在 webpack 的`babel-loader`里面增加对这个源头的编译。


## `:// 好奇`

纯粹出于好奇，我就跑到这个插件的 GitHub repo 上去看了看，想证实一下引用的文件，是否真的包含有`const`。

结果一看人家的[`dist`文件](https://github.com/manuelstofer/pinchzoom/blob/master/dist/pinch-zoom.umd.js)，没有，清一色全是`var`。

这就更奇怪了，npm package 引用的不是`dist`里面的文件？那难道是引用了`src`里面的文件吗？

还真让我在`src`的 JS 文件里找到了`const`（而且仅存在于这个方法里头）：

![es6-keyword-error](@feImages/es6-keyword-error/const-keyword.png)

## `:// 查证`
可是为什么会引用`src`里面的文件呢？查了一圈，原来是在插件的`package.json`里面设置的（连在`package.json`的官方文档里面都没找到说明，最后是在某个 GitHub issue 里面找出来的……）：

![package-json-setting](@feImages/es6-keyword-error/package-json-setting.png)

**`module`设置的，是 es6 module 引入时使用的文件。**

**`main`设置的，是 commonJS module 引入时使用的文件。**

求证后发现，插件的`package.json`设置的`module`果真是指向了`src`里的 js：

![pinch-zoom-package-json](@feImages/es6-keyword-error/pinch-zoom-package-json.png)

## `:// issue 和 pull request`

所以这个问题应该直接在`src`里面改掉就好了呀。只要源头一改，大家都不需要在 webpack 配置里做修改了。于是马上[开了一个 issue](https://github.com/manuelstofer/pinchzoom/issues/68) 给作者。

其实作者稍微动下手就完事了，但其中一个 collaborator 竟然回我说，你要不要提个 pull request ?

![pinch-zoom-issue](@feImages/es6-keyword-error/pinch-zoom-issue.png)

看到这个回复我首先是受宠若惊，然后是瑟瑟发抖呢。因为我还从来没给人家的 repo 提过 pull request，完全不懂怎么弄…… 幸好 GitHub 的这个机制还是很智能的，跟着说明走就可以了。

于是就这样，跟来自世界各地的大佬们，一齐在[这个项目](https://github.com/manuelstofer/pinchzoom)的 contributor 里面有了一个位置。(*^▽^*)

---

参考文章

[1] [SyntaxError: Unexpected keyword 'const'. Const declarations are not supported in strict mode.](https://github.com/rails/webpacker/issues/974)

[2] [打包发布后，低版本IOS会报错 SyntaxError: Unexpected keyword 'const'. Const declarations are not supported in strict mode](https://github.com/Meituan-Dianping/mpvue/issues/624)

[3] [Main inside package.json is referencing to es6 modules](https://github.com/material-components/material-components-web/issues/929)

<CopyRights />