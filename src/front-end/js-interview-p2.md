---
title: JS Interview Questions (Part 2)
date: 2019-02-17 
updated: 2019-02-17
categories: [front-end]
tags: [js, interview]
---

# JS Interview Questions Part 2

## Prototype Inheritance

Every object in Javascript has a property call `prototype`, where you can add methods or properties to it.

When you use an object as a constructor, and create new instances from it. The child objects will automatically inherit `prototype` properties from its parent.

[try on jsfiddle](https://jsfiddle.net/levblanc/pqx419ge/11/)

```js
var Car = function(model, color) {
  this.model = model
  this.color = color

  this.getModel = function() {
    return this.model
  }
}

Car.prototype.getColor = function() {
  return this.color
}

var civic = new Car('civic', 'silver')

console.log('civic', civic)
console.log('civic color:', civic.getColor())
```

As shown in the following screenshot, new instance `civic` has no `getColor` method itself, but it inherits the method from its parent `Car`.

![prototype-inheritance](@feImages/js-interview/prototype-inheritance.png)

## Function Declearation vs. Function Expression

- declearation declears a function directly
- expression saves an anoynomous function to a variable

```js
console.log(funcDec()) // function declearation
console.log(funcExp()) // TypeError: funcExp is not a function

function funcDec() {
  return 'function declearation'
}

var funcExp = function () {
  return 'function expression'
}
```

## Promise: what & why

The Promise object is often used in ajax handling. To me it is more like a syntax sugar comparing to callback functions.

```js
// callback hell
$.ajax.get('getUrl', function(data) {
  $.ajax.post('postUrl', { data: data }, function(res) {
    console.log(res)
  })
})

// promise
var getData = new Promise(function(resolve, reject) {
  resolve($.ajax.get('getUrl'))
})

var postData = function(data) {
  return new Promise(function(resolve, reject) {
    resolve($.ajax.post(data))
  })
}

getData.then(function(data) {
  return postData(data)
}).then(function(res) {
  console.log(res)
})
```