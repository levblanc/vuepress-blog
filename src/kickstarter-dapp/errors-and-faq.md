# Kickstarter Dapp 常见报错及FAQ

<PublishDate date='2018-11-07' />

## Errors

### Returned error: Error: MetaMask Tx Signature: User denied transaction signature.

MetaMask 弹出转账确认窗口时，点 “Reject” 返回的错误信息。

### No "from" address specified in neither the given options, nor the default options.

可能性1：你的 MetaMask 账号还没登录。重新打开浏览器或重启电脑后，MetaMask 插件有可能是登出的状态，登录后再次尝试即可。

可能性2：你的浏览器上没有安装 MetaMask 插件。[如何安装？](./metamask-install-and-create-account.html)

### Nounce too low

切换到 Main Ethereum Network，然后切换会来 Rinkeby Test Network，一般可以解决这个报错。

### ALERT: Transaction Error. Exception thrown in contract code.

操作触发了合约中的条件限制逻辑。

例如，合约中限制了每个人（地址）只能支持项目**一次**，如果以相同的地址重复支持，进入了合约限制的情况，就会报错。

### Transaction has been reverted by the EVM.

case 1: 在 转账申请列表 中点了 同意 按钮，但你并不是该项目的贡献者，或你已经投票通过了这个申请，合约会报错，从而导致 web3 抛出这个异常。

case 2: 在 转账申请列表 中，非项目管理员点击了 确认转账 按钮，会抛这个异常。因为只有项目管理员，才能对最终的转账进行确认。

## FAQ

待补充