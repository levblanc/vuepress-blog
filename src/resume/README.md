# 个人简历

## 联系方式
- 微信ID: SHIVET（字母全大写，添加请求中请说明来意）
- Email: [levblanc@163.com](mailto:levblanc@163.com)
- Blog:  [https://levblanc.com](https://levblanc.com) 

## 编程技能

### 前端
- 熟练掌握 HTML、CSS，精确还原设计稿的页面布局，能够开发PC端、移动端的页面
- 熟练掌握 HTML 模板引擎 Pug、Handlebars 和 EJS，及CSS预处理器 Stylus、PostCSS
- 熟练掌握 JavaScript，能熟练使用 ES6 进行开发
- 有自己的前端开发框架，熟练使用 Vue、Vuex、Vue-Router、Axios 开发项目
- 懂得使用 Gulp 或 Webpack 对代码进行打包、压缩
- 有小程序开发经验，在小程序出现的初期就使用 Gulp 搭建小程序开发框架（Pug + PostCSS + ES6 + Promise），现小程序已上线
- 有使用微信 JS SDK 进行微信内页面开发的经验，熟悉获取授权、分享和支付的 API

### 后端
- 能使用 Node.js + Express.js 或 Golang 进行后端接口 API 的开发
- 理解 HTTP 及相关协议，了解 Web 安全基本知识
- 基本掌握 MySQL, MongoDB 数据库的增删改查
- 了解 Docker 的基本操作


### 代码管理

- 熟练使用 Git、SVN 来进行代码管理（主要使用图形界面工具，非命令行）

## 开源项目

- Golang RESTful API (demo)：[https://github.com/levblanc/golang-restful-api](https://github.com/levblanc/golang-restful-api)
- hexo 主题：[https://github.com/levblanc/hexo-theme-aero-dual](https://github.com/levblanc/hexo-theme-aero-dual)

## 项目贡献

- GitHub: [pinchzoom](https://github.com/manuelstofer/pinchzoom) 
- 贡献：[https://github.com/manuelstofer/pinchzoom/issues/68](https://github.com/manuelstofer/pinchzoom/issues/68)。发现源码中有 ES6 关键字，导致打包时报错。详请请参考解读文章 [https://levblanc.com/front-end/es6-keyword-error.html](https://levblanc.com/front-end/es6-keyword-error.html)


## 工作经历 

### 2018.05 - 2019.01 广州运宝科技有限公司
*（2017.09 - 2018.05 广州大唐网游信息科技有限公司，与运宝科技是同一老板）*

#### 购物分享平台数据后台

项目介绍：公司的后台数据管理系统，包括商品、订单、会员、供应商、财务和运营管理模块。

主要职责：
- Element UI + Vue.js + Vuex + Vue-Router，进行整个后台系统的所有前端页面开发
- 使用 Axios 对后端数据进行 AJAX 异步请求，然后渲染到前端界面
- 使用 Golang + gorilla/mux 和 MySQL 提供首页综合数据、用户登录、账号添加/编辑、单个商品的添加/编辑、会员等级添加/编辑相关的API接口

#### 小程序：运宝堂买家版、卖家版

主要职责：
- Gulp + Pug + PostCSS + ES6 + Promise 开发框架搭建
- 买家小程序中，除了常规的列表、详情等展示页面之外，还配合 Java 后端使用 MQTT 实现了文字聊天形式的拍卖专场，包括买家出价、卖家倒数结拍、文字信息发送等功能
- 卖家小程序中，按照产品原型实现表单形式的拍品管理功能（拍品信息填写、图片添加/删除、拍卖专场相关人员的管理等）
- 使用 Node.js + Express.js 进行后端 API 接口开发（最后这些接口都替换成以 Java 开发）
  
#### 拍卖卖家店铺展示 H5

项目介绍：拍卖卖家的店铺展示，主要用于在微信内分享

主要职责：
- 使用 Vue.js + Vuex + Vue-Router 进行前端页面开发（首页、店铺详情、店铺编辑）
- 使用微信的 JS SDK 完成用户授权登录、页面分享等功能
- 使用 Golang + gorilla/mux 和 MySQL 提供后端 RESTful API 接口

#### 公司内部其它项目支持

1. 大唐网游微信分享页开发、维护：微信授权登录、H5 唤起 App、微信分享
2. 大唐网游微信购买钻石页开发、维护：微信授权登录、微信支付、微信分享


### 2015.03 - 2017.09 广州枫车电子商务有限公司

- 门店管理系统第一版开发框架搭建，使用 React.js + Webpack
- 门店管理系统第一版的后端接口开发，使用 Node.js + Express.js
- 门店管理系统第二版开发框架搭建，使用 Electron + Vue.js 全家桶 + Webpack
- 枫车快手项目开发框架重构，在商城项目的基础上，转换为 Webpack 打包
- 枫车商城项目开发框架重构，增加 Require.js 以实现 JS 模块化加载，Backbone.js 历史路由记录实现
- 使用微信的JS SDK完成用户授权登录、页面分享、微信支付等功能


### 2013.11 - 2015.03 文思海辉技术有限公司

- 使用 Jade(HTML)、Stylus(CSS) 进行前端静态页面开发
- 使用 Node.js + Express.js 从后端获取数据并渲染到前端页面
- 使用 Node.js 把 MongoDB 中的数据整理好，生成前端展示图表时需要的接口
- 前端页面开发，获取接口数据，使用 Highcharts 框架对数据进行展示、通过日期或时间段进行数据筛选、搜索，以及数据排序功能开发
- 开发推广活动所需要的前端页面


### 2012.04 - 2013.10 讯汇科技（广州）有限公司

- 负责公司产品的英文文字撰写、审校、编辑、整理和优化 
- 网站产品页面的文字描述，服务介绍、宣传材料及网站条款文字的英文编写 
- 根据公司产品规划，协助产品的原型设计 
- 对英文文案在前端页面的布局和样式展示进行协助
- 配合后台工程师，对网站前端页面进行整理和维护 


### 2011.03 - 2012.04 广州启德教育服务有限公司 文案顾问

### 2010.03 - 2010.10 广州志盛文化传播有限公司 导演助理


## 教育经历

**2008-2009 澳大利亚 新南威尔士大学**  研究生文凭 / 数字媒体

**2007-2008 澳大利亚 西悉尼大学**  硕士 / 英语翻译和口译

**2003-2007 广东商学院**  本科 / 商务英语
