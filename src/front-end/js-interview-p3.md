---
title: JS Interview Questions (Part 3, tricky ones)
date: 2019-02-17 
updated: 2019-02-17
categories: [front-end]
tags: [js, interview]
---

# JS Interview Questions Part 3 (tricky ones)

[try all on jsfiddle](https://jsfiddle.net/levblanc/56o4mde8/94/)

## + vs -

`+` works on numbers and strings. If variables is not of these two types, it will type-cast them to strings.

So `[]` will be type-casted to empty strings, and return an empty string.

```js
console.log(2 + '2') // '22'

console.log([] + []) // => '' 
console.log({} + []) // => '[object object]' 

console.log([1, 2, 3] + [4, 5, 6]) // => '1, 2, 34, 5, 6'
```

Reference: [What is {} + {} in JavaScript?](http://2ality.com/2012/01/object-plus-object.html)

`-` works only on numbers. If the variable is not string, it will type-cast it to number.

```js
console.log(2 - '2') // 0
```

## 0.1 + 0.2

`0.1` and `0.2` is base 10, but the computer is base 2, so when doing floating point calculation, it has to do some conversion.

```js
console.log(0.1 + 0.2) // 0.30000000000000004
```

## > vs. <

```js
console.log(5 < 6 < 7) // true
// 5 < 6 => true
// true < 7 => 1 < 7 => true

console.log(7 > 6 > 5) // false
// 7 > 6 => true
// true > 5 => 1 > 5 => false
```

## a `hi`

when using a template string after a function name, it is consider to be the argument of the function.

```js
const a = function() {
	return 'hello'
}

const sentence = a `hi`
console.log(sentence) // hello
```

## remove dupicates from array

Reference: [Get all unique values in a JavaScript array (remove duplicates)](https://stackoverflow.com/questions/1960473/get-all-unique-values-in-a-javascript-array-remove-duplicates)

```js
// remove duplicates from array
var arr = [1, 1, 2, 2, 3]
var mem = {}

// uniq by using object keys
for(var ele of arr) {
  mem[ele] = true
}

var uniqWithObj = Object.keys(mem)

// uniq by using Array.filter
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/filter
// indexOf returns the first index of element in array, -1 if not found
// https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array/indexOf
var uniqWithFilter = arr.filter(function(val, index, self) {
  return self.indexOf(val) === index
})

// uniq by using ES6 Set
var uniqWithSet = [...new Set(arr)]

console.log(uniqWithFilter) 
console.log(uniqWithObj)
console.log(uniqWithSet)
```

## contenteditable

```html
<div contenteditable='true'> Hello </div>
```

```js
document.body.contentEditable = true
```

## argument length

`this` in function y refers to `arguments`

```js
function y() {
  console.log(this.length)
}

var obj = {
  length: 5,
  method: function(y) {
    arguments[0]()
  }
}

obj.method(y, 1) // 2
```

## string constructor

string has a prototype method called `constructor`, and it is `function String()`

```js
var x = 'constructor'
console.log(x[x](01)) // '1'
```

![string-constructor](@feImages/js-interview/string-constructor.png)


## `.__proto__.__proto__`

prototype chain

`hi` => `String` => `Object` => `null`

```js
console.log(('hi').__proto__.__proto__.__proto__) // null
```

## function arguments count

```js
// function returns total number of arguments, no loops allow

function argLen() {
  return arguments.length
}

console.log(argLen(1, 2, 3))
```

## method chaining

```js
var obj = {
  x: function() {
    console.log('x')
    return this
  },
  y: function() {
    console.log('y')
    return this
  },
  z: function() {
    console.log('z')
    return this
  },
}

obj.x().y().z() // x, y, z
```

## make `var` block-scoped

```js
// make v not available outside the block
function func() {
  {
    let l = 'let'
    var v = 'var'
  }

  console.log('v is =>', v)
  console.log('l is =>', l)
}

// solution
function func() {
  {
    (function() {
      let l = 'let'
      var v = 'var'
    })()
  }

  console.log('v is =>', v)
  console.log('l is =>', l)
}

func()
```

## return line break

```js
function lineBrk() {
  return // function ends here, returns nothing
  {
    message: 'hi'
  }
}

console.log(lineBrk()) // unreachable code after return statement
```

## Object.freeze vs Object.seal


The [Object.freeze()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/freeze) method **freezes** an object. A frozen object can no longer be changed; freezing an object 

- prevents new properties from being added to it, existing properties from being removed 
- prevents changing the enumerability, configurability, or writability of existing properties
- prevents the values of existing properties from being changed 
- prevents its prototype from being changed. 

freeze() returns the same object that was passed in.

The [Object.seal()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object/seal) method seals an object, preventing new properties from being added to it and marking all existing properties as non-configurable. Values of present properties can still be changed as long as they are writable.

```js
var person = {
  name: 'john'
}

// comment out either freeze, seal or defineProperty to see the difference
// Object.freeze(person)
// Object.seal(person)

person.name = 'smith' // property changing
person.age = 22 // property adding

// add a property but not allow future changes
Object.defineProperty(person, 'hair', {
  value: 'black',
  writable: false
})

// person.hair = 'brown'

console.log(person)
```

## -Infinity and Infinity

[Math.max()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/max) uses `-Infinity` to compare with what every value is provided

> If no arguments are given, the result is -Infinity.

Checkout [Math.min()](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/min) also
```js
console.log(Math.max()) // -Infinity
console.log(Math.min()) // Infinity
```

## bind this

[try the ones below in jsfiddle](https://jsfiddle.net/levblanc/h17meo5x/34/)

```js
var obj = {
  x: 1,
  getX: function() {
    var inner = function() {
      console.log(this.x)
    }

    inner.call(this)
  },
  get: function() {
    console.log(this.x)
  }
}

obj.getX()
obj.get.call(obj)
```

## find the count of max numbers
```js
var arr = [1, 2, 2, 4, 2, 4] // max is 4 and count should be 2

function getMaxCount(arr) {
  var max = Math.max(...arr)
  
  var maxNums = arr.filter(function(item) {
    if (item === max) return item
  })
  
  return maxNums.length
}

console.log('max count', getMaxCount(arr))
```

## number to hex

[numObj.toString([radix])](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number/toString)

> radix: Optional. An integer in the range 2 through 36 specifying the base to use for representing numeric values. 

```js
var num = 26
console.log(num.toString(16))
```