# Unix 常用命令速查

作者：留白shiye

本文档只列出本人在日常使用过程中常用的参数，要查询完整的参数列表，[请使用 manpage](https://linux.die.net/man/1/)，或点击每个命令下的 manpage 链接查看详情。

::: tip 版权声明：
自由转载-非商用-非衍生-保持署名（创意共享3.0许可证）
:::

## mkdir
[manpage](https://linux.die.net/man/1/mkdir)

#### [释义]

make directories. 新建目录。

#### [语法]

**mkdir** [OPTION]... DIRECTORY...

#### [参数]

**-p, --parents**

no error if existing, make parent directories as needed

如果指定的创建路径中，父级目录不存在，将会一并创建

```bash
# 不加 -p 参数
# 若 `some` 目录不存在，会报错
$ mkdir /home/some/dir

# output
mkdir:cannot create directory 'some/dir': No such file or directory

# 使用 -p 参数
# 如果 `some` 这层并不存在，则会一同新建
# 如果 `some` 目录已存在，则在其下创建 `dir`
$ mkdir -p /home/some/dir

# 父级目录可为多层
# 假设我想创建 `end` 文件夹，而 `home` 下的这些层都并不存在
# 可以直接这样创建
$ mkdir -p /home/some/long/long/dir/till/the/end
```

## rm
[manpage](https://linux.die.net/man/1/rm)

#### [释义]

remove files or directories. 删除文件、文件夹。

#### [语法]

**rm** [OPTION]... FILE...

#### [参数]

**-f, --force**

ignore nonexistent files, never prompt

**-r, -R, --recursive**

remove directories and their contents recursively

**-rf 组合使用**

强制删除指定文件夹及里面的所有内容

::: danger 危险警告
**-rf** 组合威力巨大，使用前请**重复确认**要删除的文件夹路径，并对重要文件**进行备份**。

曾经有朋友在电脑根目录下运行了 `rm -rf *`，然后他获得了一台新电脑……
:::

```bash
# 假设在 `home` 下有 `some` 目录，结构如下：
# /home/some/long/long/dir/till/the/end
# 想要删除 `some` 及里面的所有东西

$ rm -rf /home/some
```

## chown
[manpage](https://linux.die.net/man/1/chown)

#### [释义]

change file owner and group. 修改文件所在组别和所有者。

#### [语法]

**chown** [OPTION]... [OWNER][:[GROUP]] FILE...

**chown** [OPTION]... --reference=RFILE FILE...

#### [参数]

**-R, --recursive**

operate on files and directories recursively

同时对路径下的文件、文件夹进行同样的修改

```bash
$ sudo chown -R $USER:$USER /srv/sites
```

## chmod
[manpage](https://linux.die.net/man/1/chmod)

#### [释义]

change file mode bits. 修改文件权限设置。

#### [语法]

**chmod** [OPTION]... MODE[,MODE]... FILE...

**chmod** [OPTION]... OCTAL-MODE FILE...

**chmod** [OPTION]... --reference=RFILE FILE...

#### [参数]

**-R, --recursive**

change files and directories recursively

同时对路径下的文件、文件夹进行同样的修改

#### [OCTAL_MODE 说明]

文件的读、写、执行权限，使用不同数字来代表：

read (4), write (2), execute (1)

参数中的三个数位，分别表示，如下图：

User（文件所有者）、Group（文件所在组别的其它成员）、Other（上两个类别以外的用户）

![permissions](@devopsImages/permission-mode.png)

所以如果希望赋予 User `rwx`权限，4 + 2 + 1 = 7，参数的第一位就是 7 了。

Group 和 Other 仅赋予 `rx` 权限的话，4 + 1 = 5，参数的后两位就是 5 了。

```bash
$ sudo chmod -R 755 /srv/sites
```

图片源: [An Introduction to Linux Permissions](https://www.digitalocean.com/community/tutorials/an-introduction-to-linux-permissions)

## pbcopy

[manpage](https://ss64.com/osx/pbcopy.html)

#### [释义]

Copy data from STDIN to the clipboard. 复制 STDIN 中的内容到系统剪贴板。

#### [语法]

**pbcopy** [-pboard {general | ruler | find | font}]

#### [参数]

**-pboard**  

Specify a pasteboard to copy to. By default the general pasteboard.

指定剪贴板。默认为系统剪贴板。


```bash
# 复制某个文件夹的文档列表到系统剪贴板
$ ls ~ | pbcopy

# 复制本地文档的所有内容
$ pbcopy < cookies.txt

# 复制服务器上一个文件的所有内容到系统剪贴板
ssh user@your_domain.com "cat /path/of/file/on/server" | pbcopy

# 复制某个文档的部分内容
$ grep 'ip address' serverlist.txt | pbcopy
```

