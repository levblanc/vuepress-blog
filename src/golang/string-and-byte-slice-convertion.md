---
title: Golang 字符串与 byte slice 相互转换
date: 2019-01-24
updated: 2019-01-24
categories: [golang]
tags: [golang, go, "byte slice"]
---

# Golang 字符串与 byte slice 相互转换

<PublishDate date='2019-01-24'/>

字符串和 byte slice 之间可以直接使用 type casting 进行转换。

```go
package main

import "fmt"

func main() {
    s := "hi there"

    // convert a string to byte slice
    bs := []byte(s)
    fmt.Println("bs is: ", bs)

    // convert the byte slice back to string
    ss := string(bs)
    fmt.Println("ss is: ", ss)
}
```

```bash
$ go run main.go
bs is:  [104 105 32 116 104 101 114 101]
ss is:  hi there
```

<CopyRights />