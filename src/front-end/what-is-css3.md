---
title: CSS3 到底是什么？
date: 2019-02-14 
updated: 2019-02-14
categories: [front-end]
tags: [css, css3]
---

# CSS3 到底是什么？

- attribute selectors
- flex box layout
- grid layout
- rgba, opacity, gradient colors
- border-radius
- shadows
- 

## Gotchas

margin collapse: if the top and bottom are equal, they will some times collapse.

Note: the margins of floating and absolutely positioned elements never collapse.

--- 
参考文章：

[1] [Mastering margin collapsing](https://developer.mozilla.org/en-US/docs/Web/CSS/CSS_Box_Model/Mastering_margin_collapsing)
