# 使用 GitLab CI 部署静态网站

<PublishDate date='2018-08-11' />

::: tip NOTE
阅读本篇前，请先完成以下步骤：

1. 购买服务器空间
2. 完成[基本的服务器配置](https://levblanc.com/devops/ubuntu-server-setup.html)
3. 服务器上已经[安装好 Nginx](https://levblanc.com/devops/setup-nginx-on-ubuntu.html)
:::

像 Hexo、Jekyll 这类 build 完之后已经生成好静态文件目录的博客系统来说，在 MacOS 上只要 rsync 一下静态文件到服务器指定文件夹就好了。但是作为喜欢偷懒的我来说，更希望 

change -> build -> deploy 

这个过程是完全自动化的。不需要我每次都手动去重复一回。于是就有了这篇文档。

## 1. 服务器文件夹结构
看了很多教程，他们都喜欢把静态文件放到`/var/www`这个目录下，但是这个目录路径对我来说真是完全没办法理解。`var`对我来说是变量，`www`作为文件夹名称？嗯…… 90年代的互联网么？你可能会笑话我，说我完全不懂服务器文件目录，但是，下面这个就是我看完文档结构规范之后，最终决定的目录结构。本文也将继续使用这个目录结构来进行说明。

```bash
/srv               # short for `serve`，包含本服务器上会serve出去的文件
  /sites           # 本来没想要这层的，但转念一想，可能会出现需要 serve api 的情况呢？
    /levblanc.com  # 按域名放置需要 serve 出去的文件
      /dist        # 该网站所需的静态文sites-available件
```

文档结构规范：

[Filesystem Hierarchy Standard 3.0 HTML version](http://refspecs.linuxfoundation.org/FHS_3.0/fhs/index.html)

[Filesystem Hierarchy Standard 3.0 PDF version](http://refspecs.linuxfoundation.org/FHS_3.0/fhs-3.0.pdf)

## 2. 创建文件夹并修改权限

::: tip NOTE
本篇将会使用我自行创建的服务器用户来执行命令，所以会带`sudo`。

如果你直接使用 root 用户，可以去掉命令中的`sudo`。
:::

2.1 创建文件夹

```bash
$ sudo mkdir -p /srv/sites/levblanc.com
```

2.2 修改文件夹所有者和组别

```bash
$ sudo chown -R $USER:$USER /srv/sites/
```

2.3 修改文件夹权限

```bash
$ sudo chmod -R 755 /srv/sites/
```

完成后可使用`ls -ll`命令来确认当前文件夹的权限、用户及组别，命令打印的结果释义参考下图：

`Mode`中几个字母的释义为：d(Directory), r(Read), w(Write), x(Execute)

![ownership-permission](@devopsImages/ownership-permission.png)

## 3. Nginx 配置

3.1 创建 Nginx server block

```bash
$ sudo vi /etc/nginx/sites-available/levblanc.com
```

3.2 编辑文件内容

```bash
server {
  listen 80;

  # 放置静态文件的路径
  root /srv/sites/levblanc.com/dist;

  # 服务器接收到请求时首先会尝试 serve 的文件
  # 按照这份配置，请求 levblanc.com 时
  # 首先会 serve 的就是/srv/sites/levblanc.com/dist 下的
  # index.html
  index index.html;

  # 服务器 ip 或是 已绑定的域名
  server_name levblanc.com;

  # vue app 配置 
  location / {
    try_files $uri /index.html;
  }

  location = /index.html {
    root /srv/sites/levblanc.com/dist;
    add_header Cache-Control "no-cache, no-store";
  }

  # 自定义 404 页面
  error_page /404.html;
  location = /404.html {
    root /srv/sites/levblanc.com/dist;
    # 官方文档对 internal 的说明
    # http://nginx.org/en/docs/http/ngx_http_core_module.html#internal
    internal;
  }
}
```

3.3 symlink 配置文件

```bash
# 告诉 Nginx 使用 sites-available 中的配置
$ sudo ln -s /etc/nginx/sites-available/levblanc.com /etc/nginx/sites-enabled/levblanc.com
```

3.4 检查 Nginx 配置文件是否有语法错误

```bash
$ sudo nginx -t

# output
# pass 的话会有如下输出
nginx: the configuration file /etc/nginx/nginx.conf syntax is ok
nginx: configuration file /etc/nginx/nginx.conf test is successful

# 看起来检查的不是我们修改的文件
# 但其实出错的话还是会报错的
nginx: [emerg] directive "error_page" is not terminated by ";" in /etc/nginx/sites-enabled/levblanc.com:30
nginx: configuration file /etc/nginx/nginx.conf test failed
```

3.4 重启 Nginx，应用配置

```bash
$ sudo systemctl restart nginx

# 其实直接 reload Nginx，改变也会生效的
$ sudo nginx -s reload
```

## 4. `.gitlab-ci.yml`配置

这里我采取的策略是，向 gitlab push 修改后，马上进行 build 和 deploy 的操作。因为这样，我的代码得以保存的同时，网站也能实时更新到，真正达到了我想要的一条龙效果，所以选择了CI。

`$SSH_PRIVATE_KEY`、`$STATIC_BLOG_PATH`、`$SSH_BLOG_HOST`这几个变量在 gitlab 项目的后台可以进行配置。路径是：

Settings -> CI/CD -> Variables

配置的写法参考了好几篇文章（见底部参考文章链接），经过多次尝试之后确定下来的写法。Deploy 阶段：

`before_script`是对使用 SSH 连接自己的服务器进行配置。

执行`script`时，是把已经打包好的`dist`文件夹 rsync 到自己的服务器目标路径上，进行代码更新。

```yaml
stages:
  - build
  - deploy

build_vuepress:
  stage: build
  image: node:alpine

  # 这个设置一定不能漏掉
  # build 阶段完成之后，dist 文件夹会被拷贝出来
  # 这样 deploy 的阶段才能使用，否则会报错说找不到这个文件夹
  artifacts:
    paths:
      - dist

  script: 
    - echo '====== Installing Node Dependencies ======'
    - npm i
    - echo '====== Building Vuepress for Deploy ======'
    - npm run build

deploy_vuepress:
  stage: deploy
  image: alpine

  before_script:
    - apk update && apk upgrade
    - apk add openssh bash rsync
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - eval $(ssh-agent -s)
    - echo '====== Adding SSH Key ======'
    - echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null
    - '[[ -f /.dockerenv ]] && echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config'


  script: 
    - echo '====== Removing Old Files ======'
    - ssh -p22 $SSH_BLOG_HOST "rm -rf $STATIC_BLOG_PATH"
    - echo '====== Rsyncing New Files ======'
    - rsync -e 'ssh -p 22' -arpvz ./dist/ $SSH_BLOG_HOST:$STATIC_BLOG_PATH
    - echo '====== Deploy Finished! ======'

  only:
    - master
```

至此，只要向 gitlab 的 master 分支 push 修改，CI就会马上执行，对网站进行更新。


<br>

---
参考文章：

[1] [ubuntu manpages](http://manpages.ubuntu.com/manpages/xenial/man7/hier.7.html)

[2] [LinuxFilesystemTreeOverview](https://help.ubuntu.com/community/LinuxFilesystemTreeOverview)

[3] [Should I install Linux applications in /var or /opt?](https://serverfault.com/a/96420)

[4] [Why is the root directory on a web server put by default in “/var/www”?](https://unix.stackexchange.com/a/47461)

[5] [An Introduction to Linux Permissions](https://www.digitalocean.com/community/tutorials/an-introduction-to-linux-permissions)

[6] [How to Create a Blog with Hexo On Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-create-a-blog-with-hexo-on-ubuntu-14-04)

[7] [Deploying React Applications with Webhooks and Slack on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/deploying-react-applications-with-webhooks-and-slack-on-ubuntu-16-04)

[8] [CI rsync Deployment](https://tonyblyler.com/post/ci-rsync-deployment/)

[9] [Using SSH keys with GitLab CI/CD](https://docs.gitlab.com/ee/ci/ssh_keys/README.html)

[10] [Running Composer and NPM scripts with deployment via SCP in GitLab CI/CD](https://docs.gitlab.com/ce/ci/examples/deployment/composer-npm-deploy.html)

[11] [Deploy react app with gitlab.com and RSYNC](https://gist.github.com/pjhl/81767ed6c67138eaba681d7739a9db61)

[12] [Deploying a Hugo Static Site Using GitLab, CI/CD, & SSH](https://grh.am/2018/deploying-a-hugo-static-site-using-gitlab-ci-cd-and-ssh/)

[13] [How To Configure Nginx to Use Custom Error Pages on Ubuntu 14.04](https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-to-use-custom-error-pages-on-ubuntu-14-04)

<CopyRights />