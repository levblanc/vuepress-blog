# 如何自定义 FireFox Quantum DevTools 样式

<PublishDate date='2018-12-13' />

:::tip Note
文章写成时，FireFox Quantum 版本号为 64.0
:::

在 Chrome 还没出生的时候，FireFox 曾经是我最喜欢的浏览器，我对它可谓是各种折腾。后来有了 Chrome，觉得实在好用，就把 FireFox 弃了。曾经有几次尝试过想要吃回头草，可是由于 dev tools 跟 Chrome 的实在是有差距，都在挖坑初期就放弃了。

可是这回 Chrome 实在是吃内存吃得太过于丧心病狂，几次把我的电脑 8G 内存吃到卡死，唯有狠下心来吃一次回头草了。可是……这原生界面也太挫了吧，Network 每行这么窄，字体还这么小，怎么看啊……

所幸是，能找到可以自定义的方案。那么，就开搞吧！

## 1. 打开 FireFox 支持页

在 url 栏输入'**about:support**'

![open-support](@toolsImages/firefox-customize/open-support.png)

或，点击右上角三杠 > Help > Troubleshooting Information

![menu-option-1](@toolsImages/firefox-customize/menu-option-1.png)

![menu-option-2](@toolsImages/firefox-customize/menu-option-2.png)

## 2. 打开 Profile 文件夹

点击 **Show In Finder** 打开 Profile 文件夹。Windows 上按钮的名字为 'Open Folder'。

![open-profile-folder](@toolsImages/firefox-customize/open-profile-folder.png)

## 3. 创建 chrome 文件夹和 userChrome.css

> chrome 这个名称，指的是浏览器中的用户界面，也正是为什么 Google Chrome 会起这个名字的原因。

如果你的 Profile 文件夹下，没有 chrome 文件夹，则直接创建一个，并在里面创建`userChrome.css`文件。

![create-user-chrome](@toolsImages/firefox-customize/browser-ui/create-user-chrome.png)

## 4. 设置自定义的样式

在`userChrome.css`中添加自己想要的样式，保存，然后**重启浏览器**。

下面是测试过可行的样式，主要是设置字体及其大小。

`font-family`和`font-size`请根据自己的情况进行配置。

```css
@-moz-document url-prefix(chrome://devtools/content/) {
 body, .CodeMirror-line {
  font-family: "Input" !important;
  font-size: 14px !important;
  font-weight: normal;
 }
} 

 div, [platform="linux"]:root .devtools-monospace{
   font-size: 14px !important;
 }
```

## 5. 打开 dev tool 设置页

`Cmd + Opt + I`(Mac) 或 `Ctrl + Shift + I`(Windows) 打开开发者工具（dev tools）。

点击右上角的三个点按钮，选择`setting`项，打开 dev tools 的设置页面。

![open-devtool-setting](@toolsImages/firefox-customize/browser-ui/open-devtool-setting.png)


## 6. 允许自定义 CSS 配置

**Advanced Settings** 下，勾选 **Enable browser chrome and add-on debugging toolboxes**

![enable-browser-chrome](@toolsImages/firefox-customize/browser-ui/enable-browser-chrome.png)

## Bonus 1: Dark Theme

如果你也像我，喜欢使用深色界面的 dev tools，在 **Themes** 下，选择 **Dark** 即可。

![dark-theme](@toolsImages/firefox-customize/browser-ui/dark-theme.png)

## Bonus 2: 直接调试 userChrome.css

1. **Advanced Settings** 下，勾选 **Enable remote debugging**

![enable-remote-debug](@toolsImages/firefox-customize/browser-ui/enable-remote-debug.png)

2. `Cmd + Opt + Shift + I`(Mac) 或 `Ctrl + Alt + Shift + I`(Windows) 打开**浏览器界面的调试工具**（Browser Toolbox）。（注：我们平时调试页面的是**网页调试工具**。）

a. 弹出确认对话框，点击 ok 进行确认

![incoming-connection](@toolsImages/firefox-customize/browser-ui/incoming-connection.png)


b. 长得虽然跟平时用的 dev tools 很像，但这个工具是针对浏览器界面的。

![inspect-browser](@toolsImages/firefox-customize/browser-ui/inspect-browser-ui.png)

3. 在 **Style Editor** Tab 的侧边栏找到 `userChrome.css`。
  
由于没有搜索功能，有点难找……。找了多次之后发现在我的电脑上，它永远是在一个名称比较长而且有encode的文件后面，以后就看准这个长文件名来找了。

![browser-toolbox](@toolsImages/firefox-customize/browser-ui/browser-toolbox.png)

演示：修改字体大小为`18px`，`cmd + s`保存，效果马上可见。

（注意：这个保存是会直接修改硬盘上的`userChrome.css`的，所以如果你只是想调试，可以自己先做一个备份，避免保存后覆盖掉原来的样式了。）

![live-css-modify](@toolsImages/firefox-customize/browser-ui/live-css-modify.png)

---

参考文章： 

[1] [How to Customize Firefox’s User Interface With userChrome.css](https://www.howtogeek.com/334716/how-to-customize-firefoxs-user-interface-with-userchrome.css/)

[2] [Browser and GUI Chrome](https://www.nngroup.com/articles/browser-and-gui-chrome/)

[3] [Tutorial: How to create and live-debug userChrome.css](https://www.reddit.com/r/FirefoxCSS/comments/73dvty/tutorial_how_to_create_and_livedebug_userchromecss/)


<CopyRights />