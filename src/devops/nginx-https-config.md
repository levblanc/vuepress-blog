---
title: 使用 Certbot 进行 Nginx 上的 HTTPS 配置
date: 2018-08-13
updated: 2019-01-28
categories: [devops]
tags: [certbot, nginx, https]
---

# 使用 Certbot 进行 Nginx 上的 HTTPS 配置

<PublishDate date='2018-08-13' />

::: tip NOTE
阅读本篇前，请先完成以下步骤：

1. 购买服务器空间
2. 完成[基本的服务器配置](https://levblanc.com/devops/ubuntu-server-setup.html)
3. 服务器上已经[安装好 Nginx](https://levblanc.com/devops/setup-nginx-on-ubuntu.html)
4. [配置 Nginx](https://levblanc.com/devops/deploy-static-site-with-gitlab-ci.html#_3-nginx-配置)
:::

[Chrome v68 实行了新规则](https://levblanc.com/weekly/issue0.html#tech)，非 HTTPS 的网站都会在 URL 行被打上`Not Secure`标签。按照之前的 Nginx 配置，输入`http://levblanc.com`或`https://levblanc.com`都可以到达我的博客。如果不纠结这个`Not Secure`标签，其实配置已经算是完工了。

可是我就是这么个纠结的人，因为我见过有的网站，输入`http://`开头的地址，马上就会跳转到`https://`下，怎么配置才能有这种效果呢？

## 1. 安装 Certbot

首先往`apt-get`的list中增加`certbot`的repo

```bash
$ sudo add-apt-repository ppa:certbot/certbot

# output
# 按回车继续即可
This is the PPA for packages prepared by Debian Let's Encrypt Team and backported for Ubuntu(s).
 More info: https://launchpad.net/~certbot/+archive/ubuntu/certbot
Press [ENTER] to continue or ctrl-c to cancel adding it
```

Update `apt-get`

```bash
$ sudo apt-get update
```

安装`certbot`

```bash
$ sudo apt-get install python-certbot-nginx
```

## 2. 防火墙允许 HTTPS 连接

如果你没有配置过防火墙，可以参考[这篇文章](https://levblanc.com/devops/ubuntu-server-setup.html)

如果你已经启用防火墙，可以跟着下面的步骤检查一下 Nginx 相关的配置。

```bash
$ sudo ufw status

# output
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere
Nginx Full                 ALLOW       Anywhere
OpenSSH (v6)               ALLOW       Anywhere (v6)
Nginx Full (v6)            ALLOW       Anywhere (v6)
```

如果你的 output 中包含`Nginx HTTP`，delete 掉就可以了。

```bash
$ sudo ufw delete allow 'Nginx HTTP'
```

## 3. 获取 SSL 证书

```bash
$ sudo certbot --nginx -d levblanc.com -d www.levblanc.com
```

该命令让`certbot`运行时使用`--nginx`插件。通过`-d`参数来指明 SSL 证书可使用的域名。其中`levblanc.com`和`www.levblanc.com`替换成你自己的域名。

如果是第一次运行这个命令，会让你输入邮箱和同意服务条款。

```bash
# 输入自己的邮箱
Saving debug log to /var/log/letsencrypt/letsencrypt.log
Plugins selected: Authenticator nginx, Installer nginx
Enter email address (used for urgent renewal and security notices) (Enter 'c' to
cancel):
```

```bash
# 同意服务条款
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Please read the Terms of Service at
https://letsencrypt.org/documents/LE-SA-v1.2-November-15-2017.pdf. You must
agree in order to register with the ACME server at
https://acme-v02.api.letsencrypt.org/directory
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(A)gree/(C)ancel: A

# 日后是否接收通知、新闻邮件，这里我选择了否。
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Would you be willing to share your email address with the Electronic Frontier
Foundation, a founding partner of the Let's Encrypt project and the non-profit
organization that develops Certbot? We'd like to send you email about our work
encrypting the web, EFF news, campaigns, and ways to support digital freedom.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
(Y)es/(N)o: N
```

这些问题都回答完之后，certbot 会连接 Let's Encrypt 的服务器，验证一下你是否真的拥有指定的域名。然后就被报了错：

```bash
Fetching http://levblanc.com/.well-known/acme-challenge/bJbgPS2HT5bdv1n-kvKJNykWpRoq6KuAfjaEXjdMtCg: Connection refused
```

当然的啊，我的目录里面根本都没有`.well-known`这个文件夹，更别说下面的那堆东西了。这到底算什么鬼 challenge……

查了一圈，在 Nginx 的配置里面增加下面这段，就能通过了：

```bash
server {
  # ... 你的其它配置
  location ~ /.well-known {
    allow all;
    root /var/www/html;
    try_files $uri =404;
  }
}
```

成功之后会让你选择是否把所有 HTTP 连接都 redirect 到 HTTPS 下，这里我选择了2

```bash
# output
Please choose whether or not to redirect HTTP traffic to HTTPS, removing HTTP access.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
1: No redirect - Make no further changes to the webserver configuration.
2: Redirect - Make all requests redirect to secure HTTPS access. Choose this for
new sites, or if you're confident your site works on HTTPS. You can undo this
change by editing your web server's configuration.
- - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - - -
Select the appropriate number [1-2] then [enter] (press 'c' to cancel): 2
```

:::tip
选择了2之后，certbot 会在你的 Nginx 配置文件里面添加一些配置，记得要自己整理一下。
:::

所有动作都完成后的成功提示：

```bash
IMPORTANT NOTES:
 - Congratulations! Your certificate and chain have been saved at:
   /etc/letsencrypt/live/levblanc.com/fullchain.pem
   Your key file has been saved at:
   /etc/letsencrypt/live/levblanc.com/privkey.pem
   Your cert will expire on 2018-11-11. To obtain a new or tweaked
   version of this certificate in the future, simply run certbot again
   with the "certonly" option. To non-interactively renew *all* of
   your certificates, run "certbot renew"
 - If you like Certbot, please consider supporting our work by:

   Donating to ISRG / Let's Encrypt:   https://letsencrypt.org/donate
   Donating to EFF:                    https://eff.org/donate-le
```

## 4. 自动更新 SSL 证书

Let's Encrypt 的 SSL 证书有效期仅为90日，而我们上面又设置了所有 HTTP 访问都跳转到 HTTPS，所以自动更新是非常有必要的。我们安装的`certbot`会通过`systemd`的 timer ，每天自动运行两次`certbot renew`命令。下面这个命令可以测试自动更新：

```bash
$ sudo certbot renew --dry-run
```

至此，Nginx 上的 HTTPS 配置已经完成。

## 为子域名添加 SSL 证书

如果网站增加了子域名的使用，又希望可以使用 SSL，可以使用下面这个命令，扩展证书：

```bash
$ sudo certbot --nginx -d levblanc.com -d www.levblanc.com -d <subdomain>.levblanc.com --expand
```

其实就是获取证书的命令后面加上`expand` flag。

## 更新 Certbot 版本

Update@2019-01-28

今天收到了 Certbot 团队发过来的email，说 TLS-SNI-01 validation 将要在三月份寿终正寝了。所以需要升级一下 Certbot client 的版本到`0.28`或以上，以便使用别的验证方式。

> TLS-SNI-01 validation is reaching end-of-life. It will stop working
temporarily on February 13th, 2019, and permanently on March 13th, 2019.
Any certificates issued before then will continue to work for 90 days
after their issuance date.
>
> You need to update your ACME client to use an alternative validation
method (HTTP-01, DNS-01 or TLS-ALPN-01) before this date or your
certificate renewals will break and existing certificates will start to
expire.

邮件里面的[社区文章链接](https://community.letsencrypt.org/t/how-to-stop-using-tls-sni-01-with-certbot/83210)并没有给出具体的升级方法，只是指向了一个安装说明。重复了几遍安装步骤之后检查版本，还是跟最开始一样：

```bash
$ certbot --version
certbot 0.26.1
```

感觉这个真的超级奇怪，安装命令完成之后，已经打印出来说`0.28.1`是安装好了的，为毛`--version`命令却没有变化呢？

最后发现直接`sudo apt-get upgrade certbot`，完成后查版本，会真的更新到：

```bash
$ certbot --version
certbot 0.28.0
```

后续的步骤跟随社区文章指引就ok了，从打印信息可以看到，验证方式改成`http-01`了：

```bash
...

Performing the following challenges:
http-01 challenge for kickstarter.levblanc.com
http-01 challenge for levblanc.com
http-01 challenge for www.levblanc.com

...
```

---

参考文章：

[1] [How To Secure Nginx with Let's Encrypt on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-secure-nginx-with-let-s-encrypt-on-ubuntu-16-04)

[2] [Renewing certificate “Connection refused” using: NGINX, Django](https://community.letsencrypt.org/t/renewing-certificate-connection-refused-using-nginx-django/45051/7)

[3] [[enhancement] Add a subdomain to an existing certificate](https://github.com/certbot/certbot/issues/2230)

[4] [Re-creating and Updating Existing Certificates](https://certbot.eff.org/docs/using.html?highlight=certname#re-creating-and-updating-existing-certificates)

[5] [A Beginners Guide to using apt-get commands in Linux(Ubuntu)](https://codeburst.io/a-beginners-guide-to-using-apt-get-commands-in-linux-ubuntu-d5f102a56fc4)

<CopyRights />