---
title: JS Closure
date: 2019-02-15 
updated: 2019-02-17
categories: [front-end]
tags: [js, closure]
---

# JS Closure

<PublishDate date='2019-02-15' />

<!-- Closure is function with preserved data. -->

Closures are a fundamental part of many programming languages, not just JavaScript. A closure refers to a function maintaining access to the free variables defined where the function was called. 

A closure is the combination of a function bundled together (enclosed) with references to its surrounding state (the lexical environment). 

**In other words, a closure gives you access to an outer function’s scope from an inner function. The inner function will have access to the variables in the outer function scope, even after the outer function has returned.**

Some use cases for closures are event handlers, timeouts, intervals, callbacks and keeping variables private within functions. One drawback to using closures is that they can lead to over-consumption of memory, and possibly memory leaks if not handled properly.

In functional programming, closures are frequently used for partial application & currying.


## inner function

So the simplest example of closure is as follows:

[try on jsfiddle](https://jsfiddle.net/levblanc/gt8z30o2/29/)

```js
var greetings = function(name) {
  return function() {
    return 'Hello, ' + name
  }
}

// open console in Chrome's dev tools to see the log results
console.log(greetings('john doe')()) // Hello, john doe

// expand the log and check Closure under Scopes
console.dir(greetings('john doe'))
```

The inner function preserved value of `name` param and it is a closure.

## data privacy

```js
const getSecret = (secret) => {
  return {
    get: () => secret
  };
};
```

## setTimeout within a loop

[try on jsfiddle](https://jsfiddle.net/levblanc/fLzcyaxx/26/)

```js
// timeout loop
for (var i = 0; i < 3; i++) {
	setTimeout(function() {
		console.log('timeout loop', i) // 3, 3 times
	}, 100)
}
```

Reason for the unexpected result is: 

Each `setTimeout` is a new closure, but every one of them shares the same lexical env, which has a local variable (`i`) with chaning value.

The value of `i` is determined when `setTimeout` executes, but because the looping has already finished by that time, `i` is left pointing to the last entry, 3.

It is 3 rather than 2 because when `i` increments to 2, it satisfies the condition of less then 3, and then it increments.


Solutions are:

- try to create new function scope by using callback or IIFE, or

(Callback function or IIFE creates a new lexical environment for each timeout, so the `i` refers to the corresponding element of each loop.)

- use `let` keyword

### fix #1: use IIFE(Immediately Invoked Function Expression)

```js
// timeout loop fix #1: using IIFE
for (var i = 0; i < 3; i++) {
	(function (i) {
		setTimeout(function() {
			console.log('timeout fix #1', i)
		}, 100)
	})(i)
}
```

### fix #2: use ES6 `let` keyword

The `let` keyowrd creates a block-scoped variable, 

so in each loop, the function has its own local variable.

```js
// timeout loop fix #2: using ES6 let keyword
for (let i = 0; i < 3; i++) {
	setTimeout(function() {
		console.log('timeout fix #2', i)
	}, 100)
}
```

---
参考文章：

[1] [MDN - Closures](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Closures)

[2] [Hackernoon - Javascript Closure](https://hackernoon.com/picklejs-closures-in-javascript-1cd35576f060)

[3] [Javascript Closure tutorial ( Closures Explained ) - YouTube Video](https://www.youtube.com/watch?v=71AtaJpJHw0)

[4] [Master the JavaScript Interview: What is a Closure?](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-a-closure-b2f0d2152b36)

<CopyRights />