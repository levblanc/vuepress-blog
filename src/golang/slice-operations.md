---
title: golang slice 数组常用操作
date: 2019-01-24
updated: 2019-01-24
categories: [golang]
tags: [golang, go, slice]
---

# golang slice 数组常用操作

<PublishDate date='2019-01-24'/>

## delete an element in slice

```go
a = append(a[:i], a[i+1:]...)

a = append(a[:0], a[1:]...)
// is equal to 
a = append(a[:0], a[1], a[2])
```

Reference: [Delete element in a slice](https://stackoverflow.com/questions/25025409/delete-element-in-a-slice)


## clear a slice

```go
// define
letters := []string{"a", "b", "c", "d"}
// clear
letters = nil
```

Reference:  [How do you clear a slice in Go?](https://stackoverflow.com/questions/16971741/how-do-you-clear-a-slice-in-go)

<CopyRights />