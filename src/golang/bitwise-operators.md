---
title: 位操作符
date: 2019-01-16
updated: 2019-01-16
categories: [CS]
tags: ['computer science']
---

# 位操作运算符

<PublishDate date='2018-01-16'/>

在一份示例的代码里面看到`<<`的操作：

```go
r.ParseMultipartForm(32 << 20)
```

在另一份里面看到了`|`的操作：
```go
const (
    // 1 << 0 ==> 1
    January Month = 1 << iota

    February   // 1 << 1 ==> 2
    March      // 1 << 2 ==> 4
    April      // 1 << 3 ==> 8
    May        // 1 << 4 ==> 16
    June       // ...
    July
    August
    September
    October
    November
    December

    // Break the iota chain here.

    // AllMonths will have only
    // the assigned month values, 
    // not the iota's.

    AllMonths = January | February |
        March | April | May | June |
        July | August | September |
        October | November |
        December
)

fmt.Println("All months:", AllMonths) // => 4096
```

于是不禁对这些操作符号产生了好奇。

查了一圈，发现他们叫 Bitwise Operators , 位操作运算符。

## Bitwise AND (&)

## Bitwise OR (|)

## Bitwise NOT (~)

## Bitwise left shift (<<) and right shift (>>)

对二进制移位操作，最直白的解释来自于 stackoverflow 的一个回答： 

> The super (possibly over) simplified definition is just that << is used for "times 2" and >> is for "divided by 2" - and the number after it is how many times.
>
> **So n << x is "n times 2, x times". And y >> z is "y divided by 2, z times"**.
>
> For example, 1 << 5 is "1 times 2, 5 times" or 32. And 32 >> 5 is "32 divided by 2, 5 times" or 1.

---

参考文章：

[1] [Go << and >> operators](https://stackoverflow.com/questions/5801008/go-and-operators)

[2] [移位运算符](https://baike.baidu.com/item/%E7%A7%BB%E4%BD%8D%E8%BF%90%E7%AE%97%E7%AC%A6)

[3] [Wiki - Bitwise operation](https://www.wikiwand.com/en/Bitwise_operation)

[4] [Wiki - Bitwise operations in C](https://www.wikiwand.com/en/Bitwise_operations_in_C)

[5] [Bitwise Operators in C Programming](https://www.programiz.com/c-programming/bitwise-operators)

[6] [Understanding Bitwise Operators](https://code.tutsplus.com/articles/understanding-bitwise-operators--active-11301)

[7] [Number Systems: An Introduction to Binary, Hexadecimal, and More](https://code.tutsplus.com/articles/number-systems-an-introduction-to-binary-hexadecimal-and-more--active-10848)

[8] [Wiki - Two's complement](https://www.wikiwand.com/en/Two%27s_complement)

[9] [Cornell - Two's Complement](https://www.cs.cornell.edu/~tomf/notes/cps104/twoscomp.html)