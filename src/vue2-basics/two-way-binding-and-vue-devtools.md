# PART 2 双向绑定和vue-devtools

<PublishDate date='2017-01-07' />

## 双向数据绑定
这将是全宇宙最简单的双向数据绑定示例。

上一期我们已经成功地通过Vue给html绑定了数据，也在console里面看到了数据是可以实时进行更改的。想要实现在网页上根据用户的输入呈现出实时的更新，我们需要用到Vue的一个指令：`v-model`。这是一个专门针对表单的指令。

我们可以简单地把Vue指令理解为一些Vue封装好的方法，方便我们更快地在html里面绑定数据，以及操作与数据相关的html部分。它们全部都会以`v-`开头。

所以我们的js代码不需要变，html稍微改一下，在页面就可以马上看到效果。

```html
<div id="app">
    <input type="text" v-model="info">
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            info: 'hello hacker cafe'
        }
    });
</script>
```

但是即使这样，我们修改input里面的内容，什么事情都没有发生啊？

我们在html里面多加一行，就ok了：

```html
<div id="app">
    <input class="text-input" type="text" v-model="info">
    <div class="alert alert-info">{{ info }}</div>
</div>
```

效果如图：

![双向数据绑定1](@vue2basicsImgs/p2/2-way-data-binding-1.png)

![双向数据绑定2](@vue2basicsImgs/p2/2-way-data-binding-2.png)

## vue-devtools
初次安装好vue-devtools以后，需要关闭chrome devtool再开，才能看见vue的标签（通常在最后）。如果你正在使用我提供的html，或者同样也是在浏览器访问自己本机写的html，需要在vue-devtools的设置里面勾选“允许访问文件URL”（如图）。

![vue-devtools设置](@vue2basicsImgs/p2/vue-devtools-setting.png)

打开vue-devtools以后，点击`<Root> == $vm0`这一行，会看到新开的右侧栏，并且已经读取到我们往vue里面绑定的数据（如图）。

![vue-root](@vue2basicsImgs/p2/vue-root.png)

在input里面进行一些修改，可以看到三处同时更新：

![single-source-of-truth](@vue2basicsImgs/p2/single-source-of-truth.png)

回想一下jq的年代，需要很繁复的步骤：在dom里面find一下目标元素，拿到它的text值，然后进行修改。如果你说这还是能接受的，那么痛点是：你在其它地方再需要修改这个值，你就要每次都把这些步骤重复一回，而且，这些改动都只能修改到自身。

这里vue的做法，涉及到一个很重要的概念：Single Source of Truth（我的翻译：数据源唯一）。

也就是说，上面谈到的这几处地方，它们指向的数据源是同一个。所以，当其中一处对`info`（数据源）进行了修改，其它地方也会马上得到体现。


本期就到这里，敬请期待下一期：常用指令合集


## 写在最后
源码地址：[https://github.com/levblanc/vue-2-basics](https://github.com/levblanc/vue-2-basics)

视频攻略：自制 [本章视频攻略](http://www.bilibili.com/video/av7517416/) 在此。

<CopyRights />
