---
title: Go modules 
date: 2019-01-24
updated: 2019-01-24
categories: [golang]
tags: [golang, go, "go modules"]
---

# Go modules 

<PublishDate date='2019-01-24'/>

Go v1.11 增加了 go modules，官方的依赖管理工具。用 dep 用得不是太爽的我，决定一试。

首先要说明的是，我个人不喜欢把代码都放在`GOPATH`下，所以**说明的部分都是在`GOPATH`以外的目录进行**。

如果想在`GOPATH`内使用 go modules 相关命令，可以先对`GO111MODULE`进行如下的设置：

```bash
$ export GO111MODULE=on                         # manually active module mode
$ cd $GOPATH/src/<project path>                 # e.g., cd $GOPATH/src/you/hello
```


Ok，我们赶紧新建一个项目尝试吧。

## 新建项目

```bash
$ mkdir go-mod && cd go-mod

$ touch main.go
```

`main.go`文件里面就简单引用一下`gorilla/mux`好了：

```go
package main

import (
	"fmt"

	"github.com/gorilla/mux"
)

func main() {
	r := mux.NewRouter()
	fmt.Println(r)
}
```

## go mod init

这个时候如果你跑`go mod init`，你会得到：

```bash
$ go mod init
go: cannot determine module path for source directory /Users/levblanc/projects/go-mod (outside GOPATH, no import comments)
```

What the heck? 明明我就是有 import 啊……

查了一圈发现，在新项目的状态下，`go mod init`希望你的 module 是 init 在一个 git repo 下：

```bash
$ git init
Initialized empty Git repository in /path/of/repo/.git/

$ git remote add origin https://github.com/your-id/your-repo

$ go mod init
go: creating new go.mod: module github.com/your-id/your-repo
```

呃…… 那所以如果我暂时不想新建一个git repo，还能用 go mod 么？

答案是 **YES！**

:::danger 高危预警
但是！这么做会有一个**弊端**。那就是，你最后真的把项目推到 github 或其他代码仓库时，很可能已经忘了自己当初 init 的路径！

所以虽然下面这种做法可行，但在需要提到**生产环境的项目，请不要这么做**！
:::

我尝试了去欺骗一下这个命令：

```bash
$ go mod init gitlab/levblanc/go-mod
go: creating new go.mod: module gitlab/levblanc/go-mod
```

注意：我连路径都写错了，应该是`gitlab.com`(而且我是故意不写 github 的)。但是这从一个侧面反映出，其实命令并没有进行检测。


然后，你的项目目录下，就会多出一个 go.mod 文件：

```bash
$ ls 
go.mod main.go

$ cat go.mod
module gitlab/levblanc/go-mod

require github.com/gorilla/mux v1.6.2
```

然后呢？这东西下载了没？答：还没。

## 依赖包下载

根据官方的指引，执行`go build`后，就会开始下载相关的依赖包。

然而，我在开发的时候，不想跑`go build`啊。

其实`go run`一样可以的，因为它就相当于：

1. `go build`
2. 执行文件

> Command Go: 
>
> Run compiles and runs the named main Go package. 

所以，我们可以直接：

```bash
$ go run main.go
go: finding github.com/gorilla/mux v1.6.2
go: downloading github.com/gorilla/mux v1.6.2
```

下一次再执行`go run`，就不需要再下载了。同时项目下再增加了一个文件：

```
$ ls
go.mod  go.sum  main.go

$ cat go.sum
github.com/gorilla/mux v1.6.2 h1:Pgr17XVTNXAk3q/r4CpKzC5xBM/qW1uVLV+IhRZpIIk=
github.com/gorilla/mux v1.6.2/go.mod h1:1lud6UwP+6orDFRuTfBEV8e9/aOM/c4fVVCaMa2zaAs=
```

这说明这个依赖包，肯定是下载了在某个特定的目录。找了一下，在这里：

```bash
/Users/levblanc/go/pkg/mod/github.com/gorilla/mux v1.6.2
```

所以其实是把依赖包统一放在了`GOPATH`之外的一个位置。

估计其他项目引用同一个依赖、版本相同的时候，也不会重新下载，就直接引用的同一个。

这个设计比 npm 要好一些。每个项目下的`node_modules`都不知道有多少重复的依赖。

## 旧项目迁移

我的做法是，把项目从`GOPATH`整个移出来。然后在项目目录下执行：

```bash
$ go mod init github.com/levblanc/golang-jwt
go: creating new go.mod: module github.com/levblanc/golang-jwt
go: copying requirements from Gopkg.lock
```

`go mod`就会根据原有的`Gopkg.lock`来新建`go.mod`文件

```bash
$ cat go.mod
module github.com/levblanc/golang-jwt

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.6.2
	github.com/lib/pq v1.0.0
	github.com/subosito/gotenv v1.1.1
)
```

问题来了，这个项目里面，我有 import `golang.org/x/crypto`。

当时是直接从 GitHub 整个项目克隆下来放在项目下，然后编辑`Gopkg.toml`的 constraint 的。

所以在`Gopkg.lock`里面并没有 crypto 这个包的记录…… 怎么整…… 

在 github 找到一个 [issue](https://github.com/golang/go/issues/28652#issuecomment-443745942) 在讲这个问题，于是照着里面提供的方案做了一下：

```bash
$ go mod edit -require=golang.org/x/crypto@v0.0.0
$ go mod edit -replace=golang.org/x/crypto@v0.0.0=github.com/golang/crypto@latest

$ cat go.mod
module github.com/levblanc/golang-jwt

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gorilla/context v1.1.1
	github.com/gorilla/mux v1.6.2
	github.com/lib/pq v1.0.0
	github.com/subosito/gotenv v1.1.1
	golang.org/x/crypto v0.0.0
)

replace golang.org/x/crypto v0.0.0 => github.com/golang/crypto latest
```

然后跑一下`go run`：

```bash
$ go run main.go
go: finding github.com/lib/pq v1.0.0
go: finding github.com/gorilla/context v1.1.1
go: finding github.com/subosito/gotenv v1.1.1
go: finding github.com/dgrijalva/jwt-go v3.2.0+incompatible
go: finding github.com/golang/crypto latest
go: downloading github.com/subosito/gotenv v1.1.1
go: downloading github.com/lib/pq v1.0.0
go: downloading github.com/dgrijalva/jwt-go v3.2.0+incompatible

$ cat go.sum
github.com/dgrijalva/jwt-go v3.2.0+incompatible h1:7qlOGliEKZXTDg6OTjfoBKDXWrumCAMpl/TFQ4/5kLM=
github.com/dgrijalva/jwt-go v3.2.0+incompatible/go.mod h1:E3ru+11k8xSBh+hMPgOLZmtrrCbhqsmaPHjLKYnJCaQ=
github.com/golang/crypto v0.0.0-20190123085648-057139ce5d2b h1:9dkUhGlF9C+jJBMDKqq91ycBLyQMvFjTdBhOqchi7lU=
github.com/golang/crypto v0.0.0-20190123085648-057139ce5d2b/go.mod h1:uZvAcrsnNaCxlh1HorK5dUQHGmEKPh2H/Rl1kehswPo=
github.com/gorilla/context v1.1.1/go.mod h1:kBGZzfjB9CEq2AlWe17Uuf7NDRt0dE0s8S51q0aT7Yg=
github.com/gorilla/mux v1.6.2 h1:Pgr17XVTNXAk3q/r4CpKzC5xBM/qW1uVLV+IhRZpIIk=
github.com/gorilla/mux v1.6.2/go.mod h1:1lud6UwP+6orDFRuTfBEV8e9/aOM/c4fVVCaMa2zaAs=
github.com/lib/pq v1.0.0 h1:X5PMW56eZitiTeO7tKzZxFCSpbFZJtkMMooicw2us9A=
github.com/lib/pq v1.0.0/go.mod h1:5WUZQaWbwv1U+lTReE5YruASi9Al49XbQIvNi/34Woo=
github.com/subosito/gotenv v1.1.1 h1:TWxckSF6WVKWbo2M3tMqCtWa9NFUgqM1SSynxmYONOI=
github.com/subosito/gotenv v1.1.1/go.mod h1:N0PQaV/YGNqwC0u51sEeR/aUtSLEXKX9iv69rRypqCw=
```

但是呢，要等比较久的时间才能真正跑起来。用手机上的秒表测了一下，是34.33秒…… 这不科学啊。

尝试使用 issue 里面的`-mod=vendor` flag：

```bash
$ go mod vendor # 会在项目根目录新建一个vendor文件夹
$ go run -mod=vendor main.go
```

这下可是快了很多。但是这样做其实是把所有的依赖都下载到了项目下的`vendor`文件夹了，又回到 npm 的模式了……

所以我马上整了个[示例repo](https://github.com/levblanc/x-crypto-issue)，并给官方[提了个 issue](https://github.com/golang/go/issues/29920)


---
参考文章：

[1] [Modules - golang wiki](https://github.com/golang/go/wiki/Modules)

[2] [cmd/go: go mod init fails to determine module path in subdirectory #27951](https://github.com/golang/go/issues/27951)

[3] [Command go - Compile and run Go program](https://golang.org/cmd/go/#hdr-Compile_and_run_Go_program)

[4] [cmd/go: using golang.org/x/* from mainland China is awkward with modules](https://github.com/golang/go/issues/28652)

<CopyRights />