---
title: Golang Enum 和 Iota
date: 2019-01-15
updated: 2019-01-15
categories: [golang]
tags: [golang, go]
---

# Golang Enum 和 Iota

<PublishDate date='2018-01-15'/>

偶然在一份示例代码里面看到下面这一段：

```go
type MoodState int

const (
	MoodStateNeutral MoodState = iota
	MoodStateHappy
	MoodStateSad
	MoodStateAngry
	MoodStateHopeful
	MoodStateThrilled
	MoodStateBored
	MoodStateShy
	MoodStateComical
	MoodStateOnCloudNine
)
```

不禁好奇`iota`到底是什么用法，马上就有三个问题出现在脑海里：

1. `iota`只能在`const`定义常量的时候使用吗？
2. 赋值为`iota`的常量，值到底是多少？
3. 为什么后面的常量都不用赋值了呢？

## Enum

这就要从`Enum`的概念说起了。在 C#、Java 中都有`enum`

```csharp
enum Season {
	Winter, // 0
	Spring, // 1
	Summer, // 2
	Fall,   // 3
}
```

```java
enum Color 
{ 
	RED,   // 0
	GREEN, // 1
	BLUE;  // 2
} 
```

在上面两个例子中可以看出来，`enum`用来定义成组的、相关的常量，并且会从0开始赋值，依次递增。

但是这种定义方式有什么好处呢？

在上面两个例子中，只有三四个常量，试想如果你需要定义的常量组里，有十个或以上常量呢（就像最开始的代码例子）？

在 golang 里面，没有`enum`关键字，但是我们可以使用`const`来定义：

```go
type Weekday int

const (
	Sunday    Weekday = 0
	Monday    Weekday = 1
	Tuesday   Weekday = 2
	Wednesday Weekday = 3
	Thursday  Weekday = 4
	Friday    Weekday = 5
	Saturday  Weekday = 6
)
```

但是 C# 和 Java 好像都不用这样一个个地赋值诶。

不怕，我们有`iota`。


## iota

在 golang 里面，`iota`是一个全局的计数器，从0开始。

有一点必须记住的是：**`iota`只能在`const`定义常量时使用**

```go
const (
  Sunday = iota   // 0
  Monday          // 1 
  Tuesday         // 2
  Wednesday       // 3
  Thursday        // 4
  Friday          // 5
  Satruday        // 6
)
```

## iota reset

当有新常量中使用`iota`时，它的值会 reset 为 0。


```go
const (
	Sunday = iota  // Sunday = 0
	Monday         // Monday = 1
)

// iota reset
const (
	Tuesday = iota  // Tuesday = 0
)

// iota reset
const Wednesday = iota // Wednesday = 0
```

## iota trick 1 - 跳过常量

```go
const (
	a = iota // 0
	b        // 1
	_
	d        // 3
)
```

## iota trick 2 - 由大到小的常量

```go
const max = 10

const (
	a = (max - iota) // 10
	b                // 9
	c                // 8
)
```

## iota trick 3 - 生成字母

```go
const (
	a = string(iota + 'a') // a
	b                      // b
	c                      // c
	d                      // d
	e                      // e
)
```

## 二进制位移

<!-- 关于二进制操作符的使用，可以看[这篇文章](https://levblanc.com/golang/bitwise-operators.html)。 -->

```go
type Month int

const (
	// 1 << 0 ==> 1
	January Month = 1 << iota

	February   // 1 << 1 ==> 2
	March      // 1 << 2 ==> 4
	April      // 1 << 3 ==> 8
	May        // 1 << 4 ==> 16
	June       // ...
	July
	August
	September
	October
	November
	December
)
```

---

参考文章：

[1] [What Is an Enum?](https://www.thoughtco.com/what-is-an-enum-958326)

[2] [enum in Java](https://www.geeksforgeeks.org/enum-in-java/)

[3] [Enums in Go](http://www.nick-glenn.com/posts/enums-in-go/)

[4] [Golang Spec - Iota](https://golang.org/ref/spec#Iota)

[5] [Golang Wiki - Iota](https://github.com/golang/go/wiki/Iota)

[6] [Ultimate Visual Guide to Go Enums](https://blog.learngoprogramming.com/golang-const-type-enums-iota-bc4befd096d3)

[7] [Go: iota](https://programming.guide/go/iota.html)

<CopyRights />
