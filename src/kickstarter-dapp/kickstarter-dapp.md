# 简易版 Kickstarter Dapp

<PublishDate date='2018-11-07' />

::: tip NOTE
1. 这个 dapp 是跟着教程做的，并非原创。
2. 在教程原版的基础上，我对合约的逻辑和界面布局做了一定的优化，并将其汉化。
3. 这个 dapp 使用的是 Rinkeby Test Network 的数据。
:::


## 前置要求

1. 在电脑上使用 Chrome 浏览器
2. Chrome 上安装好 MetaMask 插件（[如何安装？](https://levblanc.com/kickstarter-dapp/metamask-install-and-create-account.html)）
3. MetaMask 账号中有一定数量的测试网络 Ether（[如何获得？](https://levblanc.com/kickstarter-dapp/how-to-get-ether-for-rinkeby-test-network.html)）


## 项目简介

有想法的人，没有钱，于是就有了众筹网站。但是众筹网站会有一个问题：

项目的发起人，有可能拿了众筹的钱之后，啥都不干。那么这个项目的支持者的钱就等于打水漂了。

这个简易项目，就是尝试使用区块链技术，来解决这个问题。

游玩地址：[https://kickstarter.levblanc.com](https://kickstarter.levblanc.com)

下面会介绍操作步骤和逻辑。

当然，你也可以直接去玩，报错了查一下[常见报错及FAQ](https://levblanc.com/kickstarter-dapp/errors-and-faq.html)


## Step 1 创建项目

设置项目名称、填写项目描述，并指定最低的参与金额

![create-campaign](@dappImages/kickstarter/create-campaign.png)

项目创建成功，会有提示弹窗，告知你刚创建好的项目地址：

![create-campaign-success](@dappImages/kickstarter/create-campaign-success.png)

点击“复制好了”，会回到项目列表，最新创建的项目会排列在最前面：

![campaign-list](@dappImages/kickstarter/campaign-list.png)

创建好项目后，项目的创建者，会**成为这个项目的管理者**。

也就是说，谁创建了项目，谁就有责任管理这个项目，所以他/她会拥有一些支持者不具备的权限。


## Step 2 查看项目详情

点击项目卡片中的“查看详情”，进入项目详情页，查看项目具体描述，每一项都会有文字说明： 

![campaign-summary](@dappImages/kickstarter/campaign-summary.png)


## Step 3 支持项目

任何人都可以对项目进行资金支持。输入金额并在 MetaMask 弹窗中确认转账：

![contribute](@dappImages/kickstarter/contribute.png)

![confirm-tx](@dappImages/kickstarter/confirm-tx.png)

转账成功后，详情会刷新，看到最新的“已筹金额”和“支持者”人数：

![campaign-summary-update](@dappImages/kickstarter/campaign-summary-update.png)


## Step 4 创建项目资金转账申请

要完成一个项目，总会有各种不同的支出。所以，项目的管理者，可以创建不同的转账申请。

目前的逻辑中，没有对单次转账的资金额度进行限制。只要申请的金额小于当前的已筹金额，就ok。

![request-list-empty](@dappImages/kickstarter/request-list-empty.png)

（注：只有管理者可以看到“新增申请”按钮，这是管理者独有的权限之一）

新建转账请求：

![request-create](@dappImages/kickstarter/request-create.png)


## Step 5 查看资金转账申请列表

点击项目详情页中的“申请列表”按钮，可以查看目前该项目的资金转账申请。

a. 管理者看到的申请列表（操作一项中，有“确认转账”按钮，这是管理者权限之二）：

![request-list-manager](@dappImages/kickstarter/request-list-manager.png)

b. 支持者所看到的申请列表如下：

![request-list-contributor](@dappImages/kickstarter/request-list-contributor.png)


## Step 6 投票同意转账申请

每个转账申请，都必须得到**过半数**的支持者**同意**，才能进行最终的转账确认。

这个步骤就是打破众筹骗局的关键。因为得不到过半数支持者同意，钱就没办法转走。

每个支持者，对每个转账申请，都有一次的投票权。
点击“同意”按钮后，在 MetaMask 弹出的确认框中确认这个操作，完成后列表数据会刷新。

![request-approved](@dappImages/kickstarter/request-approved.png)

（注：因为暂时无法判断当前的账户是否已经投过票，所以“同意”按钮还是可用状态。已经同意过的申请，再次点击“同意”会报错。）


## Step 7 管理者确认转账

过半数支持者都同意以后，管理者的界面上，“确认转账”的按钮将变成可用状态：

![request-finalize](@dappImages/kickstarter/request-finalize.png)

管理者确认后，这条申请就会标记为“完成”了：

![request-completed](@dappImages/kickstarter/request-completed.png)

账户里面也会马上看到资金转入了：

![before-request-complete](@dappImages/kickstarter/before-request-complete.png)

![after-request-complete](@dappImages/kickstarter/after-request-complete.png)

到了这里，整个 Dapp 的操作逻辑已经讲解完成。

<CopyRights />