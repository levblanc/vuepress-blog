---
title: React JSX Basics
date: 2019-04-30
updated: 2019-04-30
categories: [front-end]
tags: [js, react]
---

# React JSX Basics

## Babel helps the browser to understand JSX

JSX is for development, browsers don't understand it.

JSX is converted to normal js code with the help of Babel.

Try the following code in [https://babeljs.io/repl](https://babeljs.io/repl)

```jsx
const App = () => {
  return <div>hi there</div>;
};
```

In Babel's compilation result, it will be the `React.createElement()` function
call:

```js
const App = React.createElement(...)
```

## Multi Lines of JSX

```jsx
const App = () => {
  return (
    // put parenthese around multi lines of JSX
    <ul>
      <li>hi there</li>
      <li>bye there</li>
    </ul>
  );
};
```

### JSX styling

```html
<div style="background-color: red;"></div>
```

In JSX, first curly braces indicates a JS variable, the second curly braces
indicates an JS object.

```jsx
<div style={{ backgroundColor: 'red' }} />
```

one more example:

```html
<div style="border: 1px solid red"></div>
```

```jsx
<div style={{ border: '1px solid red' }} />
```

## Class vs ClassName

In JSX, we use `className` instead of `class` so as to avoid the colition of the
JS `class` keyword.

In the future this may change and we can just use `class` in JSX.

## Showing text in JSX with Object Variable

For **text display**, we can't just put an object inside the curly braces block,
because React don't know how to interpret an object as a child element.

```jsx
const App = () => {
  const buttonText = { text: 'Click Me' };
  // causes an error
  return <button>{buttonText}</button>;
};
```

```jsx
const App = () => {
  const buttonText = { text: 'Click Me' };
  // renders correctly
  return <button>{buttonText.text}</button>;
};
```
