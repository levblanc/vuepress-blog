# Firefox Proxy Settings

Settings work as of 2018-12-11, Firefox Release 64.0. 

Use with SS.

## 1. Go To 'Preferences' Page

Type 'about:preferences' in url bar.

![setting-1](@yanImages/firefox-proxy-setting/setting-1.png)

## 2. Search 'proxy'

Result will be filtered out and highlighted.

Click 'Settings...' to open proxy settings.

![setting-2](@yanImages/firefox-proxy-setting/setting-2.png)

## 3. SOCKS setting

- Choose '**Mannual proxy configuration**'

- **SOCKS Host** set to '**127.0.0.1**', your localhost

- **Port** set to your SS's **Socks5 listen port**

(Settings of step 2 and 3 can be found in your SS software settings/preferences)

- Select '**SOCKS v5**'

![setting-3](@yanImages/firefox-proxy-setting/setting-3.png)

