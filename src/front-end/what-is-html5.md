---
title: HTML5 到底是什么？
date: 2019-02-14 
updated: 2019-02-14
categories: [front-end]
tags: [html, html5]
---

# HTML5 到底是什么？

For one, HTML5 allows browsers to play videos and audios without extra plugins like Flash or Sliverlight.

For another, websites can accept drag and drop files.

The Geolocation API allows website to detect where users are and deliver contents tailored to their locations.

HTML5 also have sematic tags like: `<header>, <footer>, <video>, <audio>, <nav>, <main>, <aside>`

<!DOCTYPE html>
Exclamation point

## History APIs

The best thing about HTML5 history APIs is that, they don't reload the page.

In tha past, the only way to change the URL was by using the `window.location` API, and it reloads the page. Except that you change your URL with a hashbang (e.g.: `<a href='#target'>target link</a>`)

And this is how early frameworks like Backbone.js did it.

With the history APIs, morden web apps are now able to change URLs without reloading the page. And of course this will need support from server configurations. E.g., in Nginx, you will need to config the server to always serve `index.html` no matter what URL is requested.

---
参考文章：

[1] [Using the HTML5 History API](https://css-tricks.com/using-the-html5-history-api/)

[2] [Manipulating the browser history](https://developer.mozilla.org/en-US/docs/Web/API/History_API)