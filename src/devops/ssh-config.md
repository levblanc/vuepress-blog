# SSH Config 配置多个不同的 SSH Key

<PublishDate date='2018-08-10' />

`ssh-keygen`生成的密钥对是使用默认名称的，但实际情况通常是，我在 GitHub、GitLab 等好几个网站都有账户，也可能正在尝试不同的服务器提供商，它们都允许配置 SSH Key。所以我更希望每个账户对应不同的 SSH Key，同时可以自行记录、管理这些密钥对。

曾经尝试过把默认的名称修改掉，结果服务器 SSH 登录被拒绝了。当时心如死灰，没办法，只能用默认名称了。直到今天找到了使用 config 文件的方法，SSH Key 管理从此易如反掌。

## 1. 创建自定义名称的 SSH Key

```bash
$ ssh-keygen -t rsa -C "your-email-address"

# output
# 文件路径要从根目录开始写绝对路径，不能使用 alias
# 这里以保存名为`id_rsa_CUSTOME_NAME`的 key 为例
Enter file in which to save the key (/path/to/.ssh/id_rsa): /path/to/.ssh/id_rsa_CUSTOME_NAME
```

输入完之后会问`passphrase`，可以直接回车跳过。如果选择输入，要自己记清楚文件和`passphrase`的对应关系。

上述步骤完成后在你的`.ssh`文件夹里面，应该可以看到下面两个文件

```bash
$ cd /path/to/.ssh
$ ls

# output
id_rsa_CUSTOME_NAME  id_rsa_CUSTOME_NAME.pub
```

## 2. 告知 SSH 新的 key

```bash
# add 的是私钥，不带 .pub 后缀的
$ ssh-add /path/to/.ssh/id_rsa_CUSTOME_NAME 

# output
Identity added: /path/to/.ssh/id_rsa_CUSTOME_NAME (/path/to/.ssh/id_rsa_CUSTOME_NAME)
```

## 3. 创建 SSH config 文件 

```bash
# 如果文件夹中原来没有 config 文件
# 这个命令将直接创建一个新文件并开始编辑
# Note: 如果最后不保存，文件不会存在

# 如果文件夹中已经有 config 文件
# 这个命令将打开该文件
$ vi ~/.ssh/config

# or

# 适用于新建 config 文件情况的另一种方式
# 先创建好空白文件
$ touch ~/.ssh/config
# 再进入文件进行编辑
$ vim config
```

## 4. 编辑 config 文件内容

```bash
# 假设你的 SSH key 会用在 aliyun.com
Host code.aliyun.com
  HostName code.aliyun.com 
  User git 
  IdentityFile ~/.ssh/id_rsa_CUSTOME_NAME # SSH private key (不带 .pub 后缀的那个)

# 假设你的 SSH key 会用在自己的服务器
Host levblanc.com
  HostName levblanc.com # 服务器域名，主域名即可
  User root # 或是你为服务器创建的 user 名称
  IdentityFile ~/.ssh/id_rsa_CUSTOME_NAME 
```

编辑好之后记得**保存文件**。

## 5. 告知服务器/服务商 SSH Key 的修改

5.1 复制公钥内容

```bash
# Mac
$ pbcopy < ~/.ssh/id_rsa_CUSTOME_NAME.pub

# Windows
clip < ~/.ssh/id_rsa_CUSTOME_NAME.pub

# GNU/Linux (requires xclip):
xclip -sel clip < ~/.ssh/id_rsa_CUSTOME_NAME.pub
```

5.2 **黏贴**到服务器 / 服务提供商后台或 admin dashboard 记录 SSH Key 的位置

::: tip

1. 如果是 GitHub 这类代码托管仓库，流程到这里就结束了，测试配置是否成功：

```bash
$ ssh -T git@code.aliyun.com

# output
Welcome to GIT, levblanc!
```

2. 如果是服务器，则需要修改上面记录过的 SSH 公钥内容，请继续下面的步骤。
:::

5.3 修改服务器`authorized_keys`文件

5.3.1 服务器`root` user的`authorized_keys`文件在根目录下，复制好公钥内容后，替换原来的内容

```bash
$ vi .ssh/authorized_keys
```

保存文件后登出，尝试重新登录。

5.3.2 如果你有创建`root`以外的用户，可以以该用户登录，或者登录`root`后切换用户进行修改：

```bash
# 从 root 切换到你创建的用户
$ su - username
```

`authorized_keys`文件也是在该用户的根目录下，所以直接使用上面的`vi`命令打开修改就可以了。

完成后输入下面的命令可以切换回`root`

```bash
$ exit
```

至此，使用 SSH config 文件配置自定义 SSH Key 的流程结束。

<br>

---

参考文章：

[1] [Quick Tip: How to Work with GitHub and Multiple Accounts](https://code.tutsplus.com/tutorials/quick-tip-how-to-work-with-github-and-multiple-accounts--net-22574)

[2] [user633183's answer on Multiple github accounts on the same computer?](https://stackoverflow.com/a/43009365)

[3] [jexchan's gist on Multiple SSH keys for different github accounts](https://gist.github.com/jexchan/2351996)

[4] [git生成ssh key及本地解决多个ssh key的问题](http://riny.net/2014/git-ssh-key/)

<CopyRights />