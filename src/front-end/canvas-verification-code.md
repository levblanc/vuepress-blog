# 利用 Canvas API 生成随机图形验证码

<PublishDate date='2018-08-19' />

:::tip 声明
方法是我综合 GitHub 上开源项目的思路，加上自己的一点想法综合而成，并非原创。
:::

## `:// TL,DR;`

直接看 [我的repo](https://github.com/levblanc/canvas-verification-code)

## `:// 搜寻`

在时间紧迫的情况下，自己也没啥想法，第一件事，肯定是问谷歌。Stackoverflow、GitHub 上的大佬们或许能提供一些思路，甚至是现成的开源 repo，站在巨人的肩膀上会省下不少时间。

我在 GitHub 上，找到了 4 个 repo（repo 地址会在文末列出），而其中三个长得非常相似。所以这里也提醒大家：

**Git 的每次 commit 都会记录时间，而时间是不会骗人的，谁先谁后明眼人一看就知道。如果你的代码是参考了别人的东西，一定要列明参考文章和 repo 。**

**不是说在代码里写了 `Created by xxx` 这东西就是你的，不是代码上了 npm 就很高大上。**

**要对得起自己的心啊。（有时候，那些你没看懂的、又是拿人家的代码，是会反噬你自己的）**

## `:// 思路`

接下来是我分析这 2 个 repo 的源码（由于 4 个里面有 3 个几乎是一样的，所以实际上是 2 个）后总结出来的思路：

**1. 我们需要一个 function，让它在指定的范围内，生成一个随机数**

**2. 用随机的颜色，填充指定宽高的矩形，作为验证码的背景**

**3. 用随机的颜色写出 4 个随机的字母，并让其在背景范围内以随机距离位移、随机角度旋转、随机缩放**

先让大家来看看我的最终成品效果图（我的验证码并不以让别人看不清楚为目的）：

![verification-codes](@feImages/verification-codes.jpg)

如果你对效果还满意，欢迎继续阅读我对代码的解读。

## `:// 解构`

:::tip

下面的说明中只解释逻辑，不提供完整代码。代码中出现的`randomInt`和`randomColor`方法，会在 [我的repo](https://github.com/levblanc/canvas-verification-code) 中列明，可供参考。
:::

**1. 在指定的范围内，生成一个随机数的 function**

```javascript
const randomArbitrary = (min, max) =>
  (Math.random() * (max - min)) + min;
```

我们通过举例来说明这个方法：

假设我们想要的是 5 到 10 之间的一个随机数。而`Math.random()`生成随机数的范围是`[0, 1)`，套到我们的需求中就是`[5, 10)`，也就是

```javascript
Math.random() * 10  // => [0, 10)

Math.random() * 5  // => [0, 5) 

// 我们希望得到的是大于 5 小于 10 的值，所以相减
Math.random() * 10 - Math.random() * 5 // => [0 - 0, 10 - 5)
// 提取公因式
Math.random() * (10 - 5)  // => [0 - 0, 10 - 5)
```

到这一步其实我们已经算对了步长（5 到 10 的步长为 5），可是最小值一直是 0，怎么才能以我们制定的最小值为起始呢？其实很简单，往整个结果上加上最小值就可以了：

```javascript
(Math.random() * (10 - 5)) + 5 
// => [0 - 0 + 5, 10 - 5 + 5)
// => [5,         10)
```

**2. 使用Canvas API画出矩形背景**

```javascript
// 创建 canvas
const canvas = document.createElement('canvas');
// Canvas context
const ctx = canvas.getContext('2d');
// 生成包含所有大小写字母的数组
const letterArr = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'.split('');
// 记录最终生成的4个字母的字符串变量
let codeText  = '';

// 设定画布的宽高
canvas.width = 120;
canvas.height = 40;

// 设定背景的填充颜色
// RGB 中 180 - 255 的颜色相对较浅
// 作为背景色不易与字母冲突
ctx.fillStyle = randomColor(180, 255);

// 设置背景色的透明度
ctx.globalAlpha = 0.7;

// 对背景矩形进行绘制
ctx.fillRect(0, 0, 120, 40);

// 由于上面使用了这个 api 设置全局的透明度
// 在这里恢复为 1，让字母不透明
ctx.globalAlpha = 1;
```

**3. 随机字母的生成**

```javascript
for (let i = 0; i < 4; i++) {
  // 在 letterArr 的长度范围内随机获取一个正整数，作为 index
  const textIndex  = randomInt(0, letterArr.length - 1);
  // 找到这个 index 所指向的字母
  const targetChar = letterArr[textIndex];

  // ... 对字母余下的操作
}
```

**4. 字母的随机颜色**

```javascript
// 设置字体样式
ctx.font = 'bold 38px serif';

// 设置字体垂直方向的对齐方式
ctx.textBaseline = 'middle';

// 设置字体颜色，RGB 中 1 - 100 颜色较深
ctx.fillStyle = randomColor(1, 100);
```

**5. 字母的位移、缩放和旋转**

```javascript
// 让每个字母都在 X 轴上按顺序位移到自己的位置
const transX = (120 / 4) * (i + 0.2);
// 所有字母都在 Y 轴上位移背景高度的一半，保证在可视范围内
const transY = 40 / 2;

// 80% - 100% 之间随机缩放字母
const scaleX = randomArbitrary(0.8, 1);
const scaleY = randomArbitrary(0.8, 1);

// 在 -60 度 到 60 度角之间的随机角度进行旋转
const deg    = Math.PI / 180;
const rotate = randomArbitrary(-60, 60);

// 一定要先进行位移，再旋转，否则字母有可能会在可视区域之外
ctx.translate(transX, transY);
ctx.scale(scaleX, scaleY);
ctx.rotate(deg * rotate);
```

**6. 绘制并为下一个字母重置设定**

```javascript
// 绘制字母
ctx.fillText(targetChar, 0, 0);
// 为下一个字母重置所有设定
ctx.setTransform(1, 0, 0, 1, 0, 0);
// 记录该字母
codeText += targetChar;
```

**7. 输出结果**

```javascript
// 获取全小写字符串
const code = codeText.toLowerCase();
// 获取 base64 data
const data = canvas.toDataURL();
```

至此，随机生成图形验证码的要点就基本解释完了。从这里出发，你还可以增强的是：

1. 通过参数来设定背景矩形的宽、高，并让字母的位移根据宽高而定
2. 通过参数来设定图形验证码中的是纯字母、纯数字、还是数字与字母的结合
3. 通过参数来设定图形验证码的长度
4. 是用一个`function`完成数据及结果字符串的输出，还是创建一个类，下面挂生成、和验证两个方法？
5. 纯色的背景太简单了，有没有办法加个背景？

## `:// Bonus`

如果你坚持读到了这里，我有一个小 Bonus，解答上面第 5 点，为图形验证码加背景。

其实方法很简单，到网上找一个你认为合适的图片，给这个图片做一个绝对定位，叠在验证码下方即可（或是反过来，绝对定位验证码也可以，只要最终二者是重叠的就行）。上面的方法中，验证码的背景本身做了透明度设置，所以下面的图片是可以透出来的。示例图片在 [我的repo](https://github.com/levblanc/canvas-verification-code) 中有，你也可以直接用。

非常感谢你，这么长的文，阅读到最后！

---

参考文章 和 repo：

[1] [Math.random() @ MDN](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Math/random)

[2] [Generating random whole numbers in JavaScript in a specific range?](https://stackoverflow.com/questions/1527803/generating-random-whole-numbers-in-javascript-in-a-specific-range)

[3] [Canvas tutorial @ MDN](https://developer.mozilla.org/en-US/docs/Web/API/Canvas_API/Tutorial)

[4] [html5 Canvas画图教程26:用transform来实现位移,缩放,旋转等](http://jo2.org/html5-canvas-transform/)

[5] [shoestrong/validate-code-canvas](https://github.com/shoestrong/validate-code-canvas)

[6] [KIDx/verify-code](https://github.com/KIDx/verify-code)

[7] [jovey-zheng/verify-code](https://github.com/jovey-zheng/verify-code)

[8] [F-happy/verification-code](https://github.com/F-happy/verification-code)

<CopyRights />
