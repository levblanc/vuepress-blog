# 欢迎阅读有意思周报

*Since 2018-07-26*

每周四更新，记录一周新奇有趣的事情。


## Get Started

您可以通过以下途径订阅本博客内容：

1. 微信公众号 - 链物志

  ![wechatQrCode](@images/wechatQrcode.jpg)

2. 收藏本博客链接：[https://levblanc.com/weekly/](https://levblanc.com/weekly/)

<!-- 2. follow [语雀上的 repo](https://www.yuque.com/levblanc/weekly)（更新稍晚于上述其他方式） -->

3. 关注知乎专栏 - [链物志](https://zhuanlan.zhihu.com/connet-the-dots) （延时更新）

## 所有周报归档

[查看周报 Archive](./archive.md)
