# 如何获取测试用 Ether

<PublishDate date='2018-12-07' />

::: tip NOTE
1. 此流程需要梯子
2. 获取的是 Rinkeby Test Network 的测试用 Ether
:::

## [https://faucet.rinkeby.io/](https://faucet.rinkeby.io/)

这个网站就是我们获取测试 Ether 的地方。进入后，可以看到输入框下面有使用说明。

我们将采用第二种方法，在 Google Plus 上操作。

![how-to-get](@dappImages/test-ether/how-to-get.png)


## Google Plus 发帖

### 1. 打开 MetaMask 插件，复制自己的账号地址

![copy-address](@dappImages/test-ether/copy-address.png)

### 2. 打开 [Google Plus](https://plus.google.com)，新建一个贴，贴文内容黏贴自己的账号地址就好了。

![new-post](@dappImages/test-ether/new-post.png)

### 3. 新帖发出来之后，点击右上角按钮，在新 tab 中打开这个帖子

![open-new-post](@dappImages/test-ether/open-new-post.png)

### 4. 复制新帖的url

![copy-post-url](@dappImages/test-ether/copy-post-url.png)

### 5. 把 url 黏贴到 [https://faucet.rinkeby.io/](https://faucet.rinkeby.io/)的输入框，选择想要获取的 Ether 量

![get-ether](@dappImages/test-ether/get-ether.png)

### 6. 根据提示，进行图片安全验证
  
### 7. 等待片刻，测试用 Ether 到账

![ether-in-account](@dappImages/test-ether/ether-in-account.png)

<CopyRights />