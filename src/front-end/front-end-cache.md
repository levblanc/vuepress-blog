---
title: 前端缓存
date: 2019-02-14 
updated: 2019-02-14
categories: [front-end]
tags: [cache]
---

# 前端缓存 Cache

## localstorage

I don't think this is a good approach. Because localstorage stays for ever, you will have to make sure how and when to clear your storage, also, what to clear. Another thing is that, localstorage has a max size of 4M, it is really hard to make sure your user doesn't max out his/her storage.

## redux / vuex

I actually use this as a runtime memory. By this I mean, if the user comes in and request for a product detail, then he goes onto another page, and come back again to the product detail, at this second time visit, he won't be requesting the api again, because the data is already stored inside our state object.

And if the user closes our app, we won't memorize anything.

But this also depends on specific situation. Like the auction app I built in my last company, the product detail page has a realtime price, so we decided to make request everytime.

## http cache

I think this is an efficient way for data caching. But it has nothing to do with front end.

Set `ETag`, `Last-Modified`, `Expires` and `Cache-Control: max-age` headers in response header. If the resource hasn't changed, the server doesn't need to send a full response.

## Nginx config

For front end resources caching, like html, css, js, and images, we can configure in our Nginx server. With the help of Webpack, every resource has a hashed version in its file name. So once Nginx is properly configured, and the file name hasn't change, these resources will be cached for the time length you set.


---
参考文章：

[1] [MDN - ETag](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/ETag)

[2] [Fantastic front-end performance](https://hacks.mozilla.org/2012/12/fantastic-front-end-performance-part-1-concatenate-compress-cache-a-node-js-holiday-season-part-4/)
