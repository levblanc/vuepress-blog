---
title: 30分钟上手Browserify
date: 2015-08-19 15:01:15
update: 2018-12-18
categories: [front-end]
tags: [browserify, bundling]
---

# 30分钟上手Browserify

Browserify 打包尝试。

<!-- more -->

## 本机环境 

|        | version |
|--------|---------|
| OS     | Yosemite 10.10.2 |
| Git    | 1.9.5 |
| Nodejs | 0.10.32 |

- 使用[cnpm](https://npm.taobao.org/)代替npm 

## 初始文件夹结构

初始文件夹结构如下：

```bash
.
├── app.js
└── index.html
```

`app.js`暂时为空，`index.html`为最基本html结构，引入`app.js`。

```html
<html>
  <head>
      <meta charset="UTF-8">
      <title>browserify-test</title>
  </head>
  <body>
    ...
  </body>
  <script src="app.js" charset="utf-8"></script>
</html>
```

在`app.js`中增加简单的代码`alert('hello browserify!')`，在浏览器中打开页面，可见弹出提示。

## 下载并保存Browserify到项目

首先，使用`npm init`命令生成`package.json`文件。

然后，使用以下命令下载Browserify并保存为开发依赖包。

```bash
$ cnpm install browserify --save-dev
```

完成后，`package.json`中应该会增加下面的部分：

```json
  "devDependencies": {
    "browserify": "^11.0.1"
  }
```

## Browserify 和 NPM

使用npm安装的依赖包，Browserify可以自动识别。以jQuery为例：

```bash
$ cnpm install jquery --save
```

下载完成后，在`index.html`增加：

```html
  ...
  <body>
      <div class="main"></div>
  </body>
  ...
```

然后，在`app.js`中对`main`这个`div`作简单处理：

```js
/* 引用jQuery就是这么简单！ */
var $ = require('jquery');
$('.main').text('hello browserify!');
```

如果这时在浏览器中打开`index.html`是会报错的，因为浏览器本身不懂得处理文件之间的依赖关系，需要使用Browserify打包文件后才可以看到效果。

## 使用Browserify打包文件

首先，把`index.html`引入的js文件从`app.js`改为`bundle.js`。

```html
<script src="bundle.js" charset="utf-8"></script>
```

`bundle.js`从哪里来呢？

在`package.json`中增加以下部分：

```json
...
  "scripts": {
    "build": "browserify app.js -o bundle.js"
  }
...
```

然后在命令行运行以下命令：

```bash
$ npm run build
```

完成以后可以发现，文件夹下新增了`bundle.js`，这时候打开`index.html`应该就可以正常看到文字了。

```bash
$ browserify app.js -o bundle.js
```

这个命令所做的，就是把`app.js`所依赖的所有文件都打包到`bundle.js`里面去。打开`bundle.js`可以看到`app.js`和jQuery的内容。

## 使用Watchify实现实时文件打包

使用`npm run build`命令，只会在运行之后打包一次文件。在开发的时候每做一次改动，就跑一次这个命令，显然不现实。所以在项目中安装Watchify这个工具来实现实时文件打包。

```bash
$ cnpm install watchify --save-dev
```

```json
  "devDependencies": {
    "browserify": "^11.0.1",
    "watchify": "^3.3.1"
  }
```

然后在`package.json`文件中增加`watch`指令：

```json
...
  "scripts": {
    "build": "browserify app.js -o bundle.js",
    "watch": "watchify app.js -o bundle.js"
  }
...
```

现在，在命令行运行`npm run watch`，并对文件做一些改动，就可以实时见到效果了。

## 尝试增加自己的模块

Browserify可以使用通过`npm`下载的包，非常方便。但它的用处绝不仅于此。在开发的时候，把自己的代码模块化，然后`export`出去，在Browserify中也可以使用`require`来获得。

现在，把问候语强化成一个小模块`salutation.js`，放在`utils`文件夹下。

```js
/* 创建自己的模块 */
var salutation = function (container) {
    var greetingsList = [
        'Nice to see you here!',
        '很高兴见到你！',
        'Alohaaaaaaa!',
        'Beep Boop!'
    ];

    var max = greetingsList.length;
    var randomIndex = Math.floor(Math.random() * max);

    container.text(greetingsList[randomIndex]);
};

/* 把模块export出去 */
module.exports = salutation;
```

在`app.js`中引入这个模块，并使用：

```js
var $ = require('jquery');
/* 引入自己的salutation模块 */
var salutation = require('./utils/salutation.js');

salutation($('.main'));
```

不断刷新页面可见效果。

## 主观使用感受

1. 直接`module.exports`输出模块，`require`使用模块，直观方便

2. 和backbone一起使用的时候，是否有办法实现css, js按需加载？

3. 如何把文件分开打包，而不是打包到一个大文件中去？

<CopyRights />