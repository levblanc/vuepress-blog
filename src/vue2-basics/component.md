# Part 7 组件

<PublishDate date='2017-01-24' />

界面写多了，大家应该都会想到一个问题：js的模块写好以后可以在多个地方重复使用，html 有没有办法做到呢？Vue给了我们这个能力，使用组件，就可以轻松做到。

## 最简单的组件
初始化Vue实例之前，使用`Vue.component`方法注册一个简单的template，在 HTML 中，就可以直接使用。因为这里会举一连串的例子，就直接用`one`、`two`、`three`来作为组件名称了。

```html
<body>
    <div id="app">
        <one></one>
    </div>
</body>
```

```javascript
Vue.component('one', {
    template: '<li>这是一个item</li>'
})

var app = new Vue({
    el: '#app'
})
```

![component-one](@vue2basicsImgs/p7/one.png)

组件名称定义的时候有一点需要注意的，就是要使用中划线分词。比方说，我想新建一个叫 list item 的组件，组件的名称就需要是`list-item`，在HTML中使用的时候也一样：

```html
<div id="app">
    <list-item></list-item>
</div>
```

```javascript
Vue.component('list-item', {
    template: '<li>这是一个item</li>'
})
```

## 组件的内容可以从数据获取吗？
可以。在组件的data方法里面返回数据就可以了。跟 Vue 实例不一样的是，组件的 data 对应一个 function，在组件中想要用到的数据，需要从这个方法里面返回（返回的数据类型是对象）。

```html
<div id="app">
    <two></two>
</div>
```

```javascript
Vue.component('two', {
    template: '<li>{{ listItem.name }}</li>',
    data: function () {
        return {
            // 在html中引入gamesDB.js
            listItem: window.games[0]
        }
    }
})
```

![component-two](@vue2basicsImgs/p7/two.png)

## 组件的内容可以在HTML里面定义吗？
可以。在组件中使用`<slot>`吧。在 HTML 的组件中间定义的内容，就会被插入到`<slot>` tag的位置中去。除了直接定义文字之外，当然也可以写 HTML。

```html
<div id="app">
    <three>item1</three>
    <three>item2</three>
    <three>item3</three>
</div>
```

```javascript
Vue.component('three', {
    template: '<li><slot></slot></li>'
})
```

![component-three](@vue2basicsImgs/p7/three.png)

## 在没有定义组件内容的时候，可以有默认的内容吗？
可以。在`<slot>`tag中间设置的内容，就是默认的内容。

```html
<div id="app">
    <four></four>
    <four>这是自定义的内容</four>
</div>
```

```javascript
Vue.component('three', {
    template: '<li><slot>默认内容</slot></li>'
})
```

![component-four](@vue2basicsImgs/p7/four.png)

## 如果我想在不同的位置插入不同的内容呢？
使用具名`<slot>`吧。在template里面设置好每个 slot 的名称，在 HTML 中通过`slot`属性指定内容要插入到哪个具名`<slot>`中。详情请看下面的代码片段和注释。

```html
<div id="app">
    <five>
        <!-- 指定要插入header这个slot中 -->
        <ul slot="header" class="nav nav-tabs">
          <li class="active"><a href="#">Home</a></li>
          <li><a href="#">Profile</a></li>
          <li><a href="#">Messages</a></li>
        </ul>

        <!-- 指定要插入content这个slot中 -->
        <div slot="content">this is my awesome website</div>
    </five>
</div>
```

```javascript
Vue.component('five', {
    template:
        '<div>' +
            '<div class="top-nav">' +
                // 设置slot的名称为header
                '<slot name="header"></slot>' +
            '</div>' +
            '<div class="main">' +
                // 设置slot的名称为content
                '<slot name="content"></slot>' +
            '</div>' +
        '</div>'
})
```

![component-five-1](@vue2basicsImgs/p7/five-1.png)

图片中选中的这一行，因为在 HTML 中指定slot的时候使用了`div` tag 所以文字被它包了起来，如果希望直接插入文字，可以使用`template`这个 tag：

```html
<div id="app">
    <five>
        <ul slot="header" class="nav nav-tabs">
            <!-- ... -->
        </ul>

        <!-- 改为使用template tag -->
        <template slot="content">this is my awesome website</template>
    </five>
</div>
```

![component-five-2](@vue2basicsImgs/p7/five-2.png)

## 既然组件相当于自定义了一个tag，那可以自定义tag的属性吗？
可以的。使用`component`的`props`来设置吧。这里有一点千万要记得，在`props`里面，是驼峰式分词，但是，在 HTML 里面使用这个属性的时候，需要用中划线分词，是中！划！线！我最开始使用的时候，两边都习惯性地使用驼峰，结果死活没有效果。最后发现官方文档有说明。。。

```html
<div id="app">
    <six user-name="john"></six>
</div>
```

```javascript
Vue.component('six', {
    props: ['userName'],
    template: '<li>{{ userName }}</li>'
})
```

![component-six-1](@vue2basicsImgs/p7/six-1.png)

## 从属性传入的数据，组件内可以进行处理吗？
可以。我们用计算属性做例子吧。把属性设定的文字转换为全大写。

```html
<div id="app">
    <six user-name="john"></six>
</div>
```

```javascript
Vue.component('six', {
    props: ['userName'],
    // 最后template中使用的是计算属性
    template: '<li>{{ uppercaseName }}</li>',
    computed: {
        uppercaseName: function() {
            return this.userName.trim().toUpperCase()
        }
    }
})
```

![component-six-2](@vue2basicsImgs/p7/six-2.png)

## 这些自定义的属性也可以用v-bind指令吗？
YES！直接用官方的一个双向数据绑定的例子吧：

```html
<div id="app">
    <input type="text" v-model="inputMsg" />
    </br>
    <six :user-name="inputMsg"></six>
</div>
```

```javascript
Vue.component('six', {
    props: ['userName'],
    template: '<li>{{ uppercaseName }}</li>',
    computed: {
        uppercaseName: function() {
            return this.userName.trim().toUpperCase()
        }
    }
})

var app = new Vue({
    el: '#app',
    data: {
        inputMsg: ''
    }
})
```

![component-six-3](@vue2basicsImgs/p7/six-3.png)

## 那我可以在组件里面直接使用另外一个组件吗？
当然可以。我们直接上例子吧：

```html
<div id="app">
    <game-list></game-list>
</div>
```

```javascript
Vue.component('game-list', {
    template:
        '<ul>' +
            // 直接使用第三个组件进行循环
            '<three v-for="game in games">{{ game.name }}</three>' +
        '</ul>',
    data: function () {
        return {
            games: window.games
        }
    }
})
```

![component-seven](@vue2basicsImgs/p7/seven.png)

这期的基本上把组件的基础都过了一遍，视频里面会附加套用 boostrap 的 css 做一个自己的组件的内容。

## 写在最后

源码地址：[https://github.com/levblanc/vue-2-basics](https://github.com/levblanc/vue-2-basics)

视频攻略：[本期视频攻略](http://www.bilibili.com/video/av8174666/) 在此。

<CopyRights />