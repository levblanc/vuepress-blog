# Intro 

![devops](@devopsImages/devops.jpg)

服务器运维相关的流程记录、备忘。

前端为什么需要懂这些？当你遇到一个跟你说，他不懂配置 Nginx 的运维时，你就会明白的。

图片源：[DevOps icon by Katia Z](https://dribbble.com/shots/4762048-DevOps-icon)
