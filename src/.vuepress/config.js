module.exports = {
  port: 9876,
  dest: 'dist',
  title: '链物志',
  description: 'Believing that the dots will connect down the road, will give you the confidence to follow your heart',
  head: [
    ['meta', {
      charset: 'utf8'
    }],
    ['meta', {
      name: 'author',
      content: 'levblanc, levblanc@gmail.com'
    }],
    ['meta', {
      name: 'X-UA-Compatible',
      content: 'IE=edge'
    }],
    ['meta', {
      name: 'copyright',
      content: '本站版权归 levblanc 所有'
    }],
    ['meta', {
      name: 'description',
      content: "Levblanc's Blog | Levblanc 的个人博客 | 互联网, 科技, 编程, 区块链, internet, tech, coding, blockchain"
    }],
    ['meta', {
      name: 'keywords',
      content: 'javascript, js, vue, vuejs, tech, coding, devop, golang, docker, unix, blockchain, 互联网, 编程, 前端, 前端开发, 前端工程师, web前端, 区块链, 服务器, 运维, 科技, 教程'
    }],
    ['link', {
      rel: 'icon',
      href: `/connectTheDots@144px.png`
    }],
    ['link', {
      rel: 'manifest',
      href: '/manifest.json'
    }],
    ['meta', {
      name: 'viewport',
      content: 'width=device-width,initial-scale=1'
    }],
    ['meta', {
      name: 'theme-color',
      content: '#fe4a49'
    }],
    ['meta', {
      name: 'apple-mobile-web-app-capable',
      content: 'yes'
    }],
    ['meta', {
      name: 'apple-mobile-web-app-title',
      content: 'levblanc.com'
    }],
    ['meta', {
      name: 'apple-mobile-web-app-status-bar-style',
      content: 'black'
    }],
    ['link', {
      rel: 'apple-touch-icon',
      href: `/connectTheDots@152px.png`
    }],
    ['meta', {
      name: 'msapplication-TileImage',
      content: '/connectTheDots@144px.png'
    }],
    ['meta', {
      name: 'msapplication-TileColor',
      content: '#000000'
    }]
  ],
  serviceWorker: true,
  ga: 'UA-83259976-2',
  themeConfig: {
    serviceWorker: {
      updatePopup: {
        message: '发现新内容！',
        buttonText: '马上刷新'
      }
    },
    nav: [{
        text: 'Home',
        link: '/'
      },
      {
        text: 'Web Front End',
        items: [{
            text: '传火之路',
            link: '/front-end/'
          },
          {
            text: 'Vue 2 基础教程',
            link: '/vue2-basics/'
          },
        ]
      },
      {
        text: 'DevOps',
        link: '/devops/'
      },
      {
        text: 'Blockchain',
        link: '/blockchain/'
      },
      {
        text: 'Go',
        link: '/golang/'
      },
      {
        text: 'Tooling',
        link: '/tooling/'
      },
      // {
      //   text: 'Resume',
      //   link: '/resume/'
      // },
      // {
      //   text: 'Weekly',
      //   link: '/weekly/'
      // },
      {
        text: 'Quotes',
        link: '/quotes/'
      },
      // {
      //   text: 'GitHub',
      //   link: 'https://github.com/levblanc'
      // },
    ],
    sidebar: {
      '/front-end/': [{
        title: 'Web 前端',
        collapsable: false,
        children: [
          '',
          '/vue2-basics/final-words.html',
          'advance-stylus-in-30-min',
          'get-started-with-rjs-in-30-min',
          'get-started-with-requirejs-in-30-min',
          'get-started-with-bower-in-30-min',
          'get-started-with-browserify-in-30-min',
          'get-started-with-electron-in-30-min',
          'building-github-pages-blog-with-hexo',
          'checkout-all-branches-and-push-to-new-origin',
          'canvas-verification-code',
          'es6-keyword-error',
          'launch-app-from-webpage',
          'js-closure',
          'js-classical-vs-prototypal-inheritance',
          // 'js-functional-programming',
          // 'js-promise',
          'how-does-array-prototype-slice-call-work',
          'this-context-of-anonymous-function-in-javascript',
          // 'front-end-cache',
          // 'front-end-security',
          // 'js-interview-p1',
          // 'js-interview-p2',
          // 'js-interview-p3',
          // 'what-is-html5',
          // 'what-is-css3',
          // 'regexp'
        ]
      }],
      '/vue2-basics/': [{
        title: '大家的 Vue.js 2.0',
        collapsable: false,
        children: [
          '',
          'two-way-binding-and-vue-devtools',
          'directive',
          'v-bind',
          'list-and-event-listener',
          'computed-properties',
          'component',
          'component-communication',
          'final-words',
          '/front-end/',
        ]
      }],
      '/kickstarter-dapp/': [{
        title: '',
        collapsable: false,
        children: [
          '',
          'metamask-install-and-create-account',
          'how-to-get-ether-for-rinkeby-test-network',
          'how-dapp-solves-kickstarter-problem',
          'kickstarter-dapp',
          'errors-and-faq',
        ]
      }],
      '/blockchain/': [{
        title: 'Blockchain',
        collapsable: false,
        children: [
          '',
          'short-history-of-bitcoin',
          'short-history-of-ethereum',
          '/kickstarter-dapp/',
        ]
      }],
      '/golang/': [{
        title: '',
        collapsable: false,
        children: [
          '',
          'give-dep-a-try',
          'enum-and-iota',
          // 'bitwise-operators'
          'slice-operations',
          'string-and-byte-slice-convertion',
          'go-modules'
        ]
      }],
      '/devops/': [{
        title: 'DevOps',
        collapsable: false,
        children: [
          '',
          'ubuntu-server-setup',
          'setup-nginx-on-ubuntu',
          'ssh-config',
          'deploy-static-site-with-gitlab-ci',
          'nginx-https-config',
          'unix-commands',
        ]
      }],
      '/tooling/': [{
        title: 'Tooling',
        collapsable: false,
        children: [
          '',
          'firefox-customize-browser-ui',
          'firefox-customize-page-style',
          'vscode-fonts-customization'
        ]
      }],
      // '/resume/': [
      //   {
      //     title: 'Resume',
      //     collapsable: false,
      //     children: [
      //       '',
      //       'self-intro'
      //     ]
      //   }
      // ],
      // '/weekly/': [
      //   {
      //     title: '最新10期周报',
      //     collapsable: false,
      //     children: [
      //       'issue2',
      //       'issue1',
      //       'issue0',
      //       'archive'
      //     ]
      //   }
      // ],
    },
  },
  configureWebpack: {
    resolve: {
      alias: {
        '@images': '../images',
        '@weeklyImages': '../images/weekly',
        '@devopsImages': '../images/devops',
        '@feImages': '../images/front-end',
        '@vue2basicsImgs': '../images/vue2-basics',
        '@blockchainImages': '../images/blockchain',
        '@dappImages': '../images/kickstarter-dapp',
        '@dockerImages': '../images/docker',
        '@yanImages': '../images/yan',
        '@toolsImages': '../images/tools',
        '@golangImages': '../images/golang',
      }
    }
  }
}