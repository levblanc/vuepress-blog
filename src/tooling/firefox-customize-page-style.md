# 如何自定义 FireFox Quantum 页面样式

<PublishDate date='2018-12-14' />

:::tip Note
文章写成时，FireFox Quantum 版本号为 64.0
:::

FireFox 现在自带了一个 Reader View 模式：

![reader-view-button](@toolsImages/firefox-customize/page-style/reader-view-button.png)

可以设置的东西还算齐全：

![reader-view-settings](@toolsImages/firefox-customize/page-style/reader-view-settings.png)

还有语音朗读功能（虽然读得比较机械）：

![narrate-mode](@toolsImages/firefox-customize/page-style/narrate-mode.png)

可是在默认状态下，文字是左对齐的，我觉得看起来非常不舒服：

![text-align-left](@toolsImages/firefox-customize/page-style/text-align-left.png)

强烈地想要弄成两端对齐的！最后又找到了资料，所以 FireFox 这个可定制的开放态度还是不错的。

## 1. 打开 FireFox 支持页

在 url 栏输入'**about:support**'

![open-support](@toolsImages/firefox-customize/open-support.png)

或，点击右上角三杠 > Help > Troubleshooting Information

![menu-option-1](@toolsImages/firefox-customize/menu-option-1.png)

![menu-option-2](@toolsImages/firefox-customize/menu-option-2.png)

## 2. 打开 Profile 文件夹

点击 **Show In Finder** 打开 Profile 文件夹。Windows 上按钮的名字为 'Open Folder'。

![open-profile-folder](@toolsImages/firefox-customize/open-profile-folder.png)

## 3. 创建 chrome 文件夹和 userContent.css

如果你的 Profile 文件夹下，没有 chrome 文件夹，则直接创建一个，并在里面创建`userContent.css`文件。

![create-user-Content](@toolsImages/firefox-customize/page-style/create-user-content.png)

## 4. 设置自定义的样式

在`userContent.css`中添加自己想要的样式，保存，然后**重启浏览器**。

下面是测试过可行的样式，强制 Reader View 下文字两端对齐。


```css
@namespace url(http://www.w3.org/1999/xhtml);

@-moz-document url-prefix("about:reader") {
  .moz-reader-content {
    text-align: justify;
  }
}
```

重启浏览器后可以看到效果：

![text-jusify](@toolsImages/firefox-customize/page-style/text-justify.png)

---

参考文章： 

[1] [Justified text in Firefox's "reader" mode](https://gist.github.com/Cimbali/2d371d11fac17fb3140cdca4b41ee7cf)

[2] [userChrome vs. userContent files](https://www.reddit.com/r/FirefoxCSS/comments/7dtd91/userchrome_vs_usercontent_files/)


<CopyRights />