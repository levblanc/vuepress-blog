---
title: dep 初体验
date: 2018-12-24
updated: 2018-12-27
categories: [golang]
tags: [golang, go]
---

# dep 初体验

<PublishDate date='2018-12-24'/>

## 安装

mac 上由于墙的问题，使用`homebrew`安装的时候，到了需要使用`https://go.googlesource.com`上的库时，安装失败……

```bash
$ brew install dep                                                                                                       
==> Installing dependencies for dep: go
==> Installing dep dependency: go
==> Downloading https://dl.google.com/go/go1.11.4.src.tar.gz
Already downloaded: /Users/levblanc/Library/Caches/Homebrew/downloads/3b21f3bc9313c639c24f1c953c2ecb263fbf12ec520b95d848d8fa28e848aeeb--go1.11.4.src.tar.gz
==> Downloading https://storage.googleapis.com/golang/go1.7.darwin-amd64.tar.gz
Already downloaded: /Users/levblanc/Library/Caches/Homebrew/downloads/ad0901a23a51bac69b65f20bbc8e3fe998bc87a3be91d0859ef27bd1fe537709--go1.7.darwin-amd64.tar.gz
==> ./make.bash --no-clean
==> /usr/local/Cellar/go/1.11.4/bin/go install -race std
==> Cloning https://go.googlesource.com/tools.git
Updating /Users/levblanc/Library/Caches/Homebrew/go--gotools--git
fatal: unable to access 'https://go.googlesource.com/tools.git/': Failed to connect to go.googlesource.com port 443: Operation timed out
Error: An exception occurred within a child process:
  DownloadError: Failed to download resource "go--gotools"
Failure while executing; `git fetch origin` exited with 128. Here's the output:
fatal: unable to access 'https://go.googlesource.com/tools.git/': Failed to connect to go.googlesource.com port 443: Operation timed out
```

曾经尝试在 github 的 repo 把库下载到了指定的文件夹，希望安装程序检测到已经下载好了，可以跳过这个步骤，没想到它发现下载好了，还要进行 update，当场气晕过去……

转而使用官方的`sh`命令安装：

```bash
$ curl https://raw.githubusercontent.com/golang/dep/master/install.sh | sh
```

下载的过程也很诡异，`tools.git`明明只有9.1M（v0.5.0），直接在 github 上面下载也是一眨眼的事，但是`sh`的 fetch 却等了非常久，以至于我两次以为是 timeout 了，忍不住`ctrl + c`停止掉进程。幸好最后还是成功了。

```bash
ARCH = amd64
OS = darwin
Will install into /Users/levblanc/go/bin
Fetching https://github.com/golang/dep/releases/latest..
Release Tag = v0.5.0
Fetching https://github.com/golang/dep/releases/tag/v0.5.0..
Fetching https://github.com/golang/dep/releases/download/v0.5.0/dep-darwin-amd64..
Setting executable permissions.
Moving executable to /Users/levblanc/go/bin/dep
```

## 项目路径

项目强制要放在 `GOPATH/src` 下，感觉这个设置有点坑啊（据说 golang 2.0 会好起来，不需要强制路径）。

```bash
$ dep init
init failed: unable to detect the containing GOPATH: /Users/levblanc/projects/golang-channels is not within a known GOPATH/src
```

## 安装依赖包

另外一个坑点，`dep init`后，一定要首先在项目下建一个`.go`文件，否则直接`dep ensure -add`会报错……

```bash
$ dep ensure -add github.com/gorilla/mux
no dirs contained any Go code
```

如果只是新建了一个空的`main.go`文件，还是会报错：

```bash
all dirs contained build errors
```

…… 

对这些设定表示非常无语。只能在`main.go`里面写一个空的`main` function，然后就可以了……

直接这样添加依赖包，会报错说你的项目中并没有`import`。所以看来`dep`的机制跟`npm`有点不一样。

首先，一定要有使用，才可以添加。

其次，即使你在前面成功添加了没有`import`过的库，后面再次使用`dep ensure -add`命令继续添加其它依赖，它还是会识别出来，然后帮你删除掉……


```bash
$ dep ensure -add github.com/gorilla/mux
Fetching sources...

"github.com/gorilla/mux" is not imported by your project, and has been temporarily added to Gopkg.lock and vendor/.
If you run "dep ensure" again before actually importing it, it will disappear from Gopkg.lock and vendor/.
```

这个操作可以说是有两面性吧。成功地避免了没有使用过的依赖包（装了然后不记得删除）的冗余堆积（项目作者自己不注意、不 review 的时候，npm的`package.json`里面就经常出现这种情况），但是在开发过程中，不安装又没法`import`，预先安装了也在单个文件里面引用了，结果仅仅是因为删除了文件里面的一行相关代码，你就帮我把整个包都给删了，却是有点让人抓狂呢，谁说我在其它代码里面就不用呢？

## crypto 依赖包被墙

尝试安装一系列依赖包的时候，某个包依赖了`crypto`这个库，结果`golang.org`这个域名被墙了。

```bash
Solving failure:
Solving failure: unable to deduce repository and source type for "golang.org/x/crypto": unable to read metadata: unable to fetch raw metadata: failed HTTP request to URL "http://golang.org/x/crypto?go-get=1": Get http://golang.org/x/crypto?go-get=1: dial tcp 216.239.37.1:80: i/o timeout
```

只能转而使用增加`http_proxy`环境变量来绕，变量设置一定要加双引号，否则设置无效

（当前 proxy 设置是基于SS的）

```bash
$ http_proxy="http://127.0.0.1:123456" dep ensure -add github.com/gorilla/mux

Fetching sources...

The following packages are not imported by your project, and have been temporarily added to Gopkg.lock and vendor/:
	github.com/go-pg/pg
	github.com/gorilla/mux
	github.com/subosito/gotenv
If you run "dep ensure" again before actually importing them, they will disappear from Gopkg.lock and vendor/.
```

终于成功把包都弄了下来，这时候本地的`vendor`文件夹下可见：

```bash
vendor
└── github.com
    ├── go-pg
    ├── gorilla
    ├── jinzhu
    └── subosito
```

---
参考文章：

[1] [Dep Guides - Creating a New Project](https://golang.github.io/dep/docs/new-project.html)


<CopyRights />