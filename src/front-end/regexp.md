# 常用正则速查

## 只包含字母、数字、下划线的字符串

```javascript
const isValidPassword = (password) => {
  const validChar = new RegExp(/^\w+$/g);
  return validChar.test(password);
};
```

source: [Javascript regex only alphabet, number and underscore](https://stackoverflow.com/questions/21540624/javascript-regex-only-alphabet-number-and-underscore)

## 截取字符串开头的数字

字符串中间、结尾有数字也不影响

```javascript
var paragraph    = '300天0门槛免费券666';
var regex        = /^\d+/g;
var number       = paragraph.match(regex);
var restOfString = paragraph.replace(regex, '')

console.log(number); // => ["300"]
console.log(restOfString) // => "天0门槛免费券666"
```

reference: [正则表达式：提取以数字开头，以点结尾的字符串](http://www.voidcn.com/article/p-kjwurcyh-sx.html)