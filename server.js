const express = require('express');
const util = require('util');
const fs = require('fs');
const path = require('path');
const readFile = util.promisify(fs.readFile);
const app = express();
const port = process.env.PORT || 8892;

const renderPage = async (req, res) => {
  const htmlRegex = new RegExp(/^.*\.html$/g)
  const imageRegex = new RegExp(/^.*\.(png|jpg|jpeg)$/g)
  let file = null
  let filePath = ''

  if (htmlRegex.test(req.url) ||  imageRegex.test(req.url)) {
    filePath = path.resolve(__dirname, `dist${req.url}`)
  } else {
    filePath = path.resolve(__dirname, `dist${req.url}index.html`)
  }

  const html = await readFile(filePath, 'utf8');
  res.send(html);
};

const staticDirPath = path.resolve(__dirname, `./dist/assets`);

app.use('/assets/', express.static(staticDirPath, {
  setHeaders: (res) => {
    res.set('Cache-Control', 'public, max-age=31536000, s-maxage=3600');
  },
}));

app.get('*', renderPage)

app.listen(port);

console.info(`> Listening at http://127.0.0.1:${port}/`);
