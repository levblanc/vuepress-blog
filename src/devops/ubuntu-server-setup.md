# Ubuntu 16.04 服务器初始 Setup

<PublishDate date='2018-08-06' />

在公司，每次部署项目到服务器，运维都会给我不同的密码。即使有的项目是在相同的机器上，但时间一长，手上摊的项目多了，自然是各种账号密码满天飞，只能默默地开一个文档记起来，每次部署被问到密码，都要手动复制黏贴一次。

最近自己开始捣鼓博客的事，所以在DO（Digital Ocean）开了账号开始折腾。DO上面的各种教程文章写得挺不错，有问题直接搜一下基本都有详细的解决方案（当然也有遇到中间步骤走不通，然后卡住我几小时到处搜答案的）。

跟着这些教程走一遍之后，基本的设置都走通了，各种操作也可以说都明白了，但是基本上没法记住，所以写这篇文章给自己留个记录。

## 1. 生成SSH密钥对(SSH Key Pair)

#### SSH简单介绍

SSH是一种加密的网络传输协议，通过在网络中建立安全隧道（secure channel）来实现SSH客户端与服务器之间的连接，最常见的用途是远程登录系统。

服务器可以通过各种方法来验证用户身份，使用密码是最容易，却不一定是最安全的做法。虽然密码是在安全的环境下进行传输，但是通常这些密码强度和长度都不够，以现在计算机的算力加上自动化的脚本对密码进行暴力破解，是完全可能的。

SSH密钥对通过公私钥的形式，来对信息进行加密。使用公钥（Public Key）进行加密的信息，只有其配对的私钥（Private Key）才能解开。一般的做法是，在本地的计算机上生成密钥对，然后把公钥上传到需要使用的地方（授权网站、或服务器之类的）。

当用户上传了自己的公钥到服务器上之后，服务器会将其记录在`~/.ssh/authorized_keys`这个文档里面。用户请求登录时，服务器检测当前的用户是否持有对应的私钥，从而判断是否允许其登录。

#### 1.1 使用`ssh-keygen`命令（该命令默认生成 2048 bit 的 RSA 密钥对）

```bash
$ ssh-keygen

# output:
Generating public/private rsa key pair.
Enter file in which to save the key (/home/username/.ssh/id_rsa):
```

上述路径中`username`就是你当前使用的用户的名字，直接回车使用默认的路径即可。因为登录服务器时，自动检测私钥的路径就是这个，为了避免折腾，直接用默认的这个路径就好了。

如果你想要配置自定义名称的密钥对，请参考 [SSH Config 配置多个不同的 SSH Key](https://levblanc.com/devops/ssh-config.html)

#### 1.2 确认密钥对已经生成

```bash
$ cd ~/.ssh
$ ls

# output:
id_rsa    id_rsa.pub
```

#### 1.3 作为`root`登录服务器

在 DO 的 dashboard 上，从侧边栏选择 Security tab，点击 Add SSH Key，把公钥黏贴到弹出的 input 里面，就完成了。

新建 Droplet 的时候，它就会自动把你的公钥放在里头，也不会给你发送初始密码邮件，这样，你以`root`身份登录，就不再需要密码了。

所以，建立新 Droplet 的步骤，应该是：

1. 添加 SSH Key
2. 新建 Droplet

当然你建立了 Droplet 之后再添加 SSH Key 给它也是没问题的，只是又要走其他繁琐的步骤，上面这个流程应该是最简单的了。

## 2. 新建用户

作为`root`登录服务器是最简单的，一般运营商也都是给你 root 权限。但这也可能是毁灭性的，如果你作为 root ，不小心跑错了一个命令，你的服务器可能就残废了。

所以，建一个新用户，给它日常使用的操作权限，就安全得多。下面以新建用户`john`为例进行介绍。

**Note: 下面步骤中的命令，除非具体说明，否则都以`root`用户来执行。**

```bash
$ adduser john
```

它会要求你给这个用户设置密码，设置一个强度和长度都足够的密码吧。

接下来还有会一些问题，什么用户全名啦，公司名称啦这些，直接按回车 skip 掉就行了。

## 3. 设置用户权限

我们新建好了一个叫 john 的新用户，但目前他只有普通权限。可是我们希望这个用户能做管理员的工作呀，所以我们要把他设置成为“超级用户”，能以`sudo`开头运行管理员权限的命令。

在 Ubuntu 16.04 下，属于 sudo 这个 Group 的用户，可以执行`sudo`命令。执行下面这个命令，赋予 john sudo 权限：

```bash
$ usermod -aG sudo john
```

## 4. 让新用户通过SSH密钥对登录服务器

我们的`root`用户在最开始建立 Droplet 的时候，已经设置好了密钥对，但是新用户 john 还没有。所以我们要告诉服务器，john 这个用户登录的时候，也直接使用 SSH 密钥对就好了。

#### 4.1 在服务器上，执行下面这个命令，暂时切换为 john：

```bash
# 这命令应该是 switch user 的简写吧？
$ su - john
```

#### 4.2 在新用户的 home 目录下新建`.ssh`文件夹，限制权限：

```bash
$ mkdir ~/.ssh
$ chmod 700 ~/.ssh
```

#### 4.3 在`.ssh`文件夹里面新建名为`authorized_keys`的文档：

```bash
$ vi ~/.ssh/authorized_keys
```

接下来我们要把本机的公钥复制到这个文档里面来。

#### 4.4 手动复制公钥内容：

方法A：全手动型，打印出公钥的内容，并手动从命令行里面复制

```bash
$ cat ~/.ssh/id_rsa.pub

# 命令行会输出你的 id_rsa.pub 内容：
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDBGTO0tsVejssuaYR5R3Y/i73SppJAhme1dH7W2c47d4gOqB4izP0+fRLfvbz/tnXFz4iOP/H6eCV05hqUhF+KYRxt9Y8tVMrpDZR2l75o6+xSbUOMu6xN+uVF0T9XzKcxmzTmnV7Na5up3QM3DoSRYX/EP3utr2+zAqpJIfKPLdA74w7g56oYWI9blpnpzxkEd3edVJOivUkpZ4JoenWManvIaSdMTJXMy3MtlQhva+j9CgguyVbUkdzK9KKEuah+pFZvaugtebsU+bllPTB0nlXGIJk98Ie9ZtxuY3nCKneB+KjKiXrAvXUPCI9mWkYS/1rggpFmu3HbXBnWSUdf localuser@machine.local
```

方法B：使用 pbcopy (MacOS自带)

```bash
# 执行后公钥的内容已经复制好在你的系统剪贴板里面了
$ pbcopy < ~/.ssh/id_rsa.pub
```

接着把复制好的内容，黏贴到刚刚新建的`authorized_keys`里头就行了

#### 4.5 限制权限：

```bash
$ chmod 600 ~/.ssh/authorized_keys
```

#### 4.6 切换回`root`：

```bash
$ exit
```

#### 4.7 确认新用户可以使用 SSH 密钥对登录

新开一个命令行窗口，尝试让 john 登录

```bash
$ ssh john@your_server_ip
```

如果上面的步骤都执行正确，应该是可以成功登录了。如果不行就给 ssh 命令加 `-vvv` flag debug 一下看是什么问题吧。

## 5. 设置基本的防火墙

***Note: 这个步骤里面的命令，均以新用户 john 来执行***

Ubuntu 16.04 可以使用 UFW 防火墙来确保仅有我们允许的程序才能连接服务器。

各程序在安装时就会向 UFW 进行注册自己的 profile，我们可以查看当前有哪些程序已经向 UFW 注册过：

```bash
$ sudo ufw app list

# Output
Available applications:
  OpenSSH
```

设置允许 SSH 连接：

```bash
$ sudo ufw allow OpenSSH

# output
Rules updated
Rules updated (v6)
```

开启防火墙：

```bash
$ sudo ufw enable

# 输入 'y' 然后回车 
Command may disrupt existing ssh connections. Proceed with operation (y|n)? y
Firewall is active and enabled on system startup
```

查看目前防火墙状态：

```bash
$ sudo ufw status

# Output

Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere
OpenSSH (v6)               ALLOW       Anywhere (v6)
```

到这里，Ubuntu 服务器的配置就基本完成了。

<br>

---

参考文章：

[1] [Secure Shell](https://www.wikiwand.com/zh-hans/Secure_Shell)

[2] [How To Configure SSH Key-Based Authentication on a Linux Server](https://www.digitalocean.com/community/tutorials/how-to-configure-ssh-key-based-authentication-on-a-linux-server)

[3] [Initial Server Setup with Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-16-04)

<CopyRights />