# 有意思周报 issue:0 

<PublishDate date='2018-07-26' />

## `:// World` 
[俄罗斯的新型鱼雷](https://www.businessinsider.com/russia-shows-off-a-nuclear-doomsday-torpedo-that-the-us-cant-stop-2018-7)

![russia](~@weeklyImages/issue0/russia.jpg)
RIA Novosti/Reuters

俄罗斯国防部公布了一个视频，里面展示了一款高速核动力鱼雷（并非上图）。报道称，这款鱼雷是专门以躲避美国的防御为目的而制造的，但同时又表示，这可能只是俄罗斯虚张声势而已。

感觉米国人还真是对普京大大又恨又怕呢，普京大大总是一副信心满满的样子也是很帅。

## `:// Entrepreneur`
1. [谷歌公布其翻译App使用量](https://www.businessinsider.com/sundar-pichai-google-translate-143-billion-words-daily-2018-7)

![google](~@weeklyImages/issue0/googleTranslate.jpg)
Google

谷歌CEO Pichai 公布了一项数据，让人对谷歌翻译产品的变现能力充满想象。数据显示，Google Translate 每天翻译143十亿个单词（143 billion words），使用量在世界杯期间还激增。

虽然 Google Translate 目前是免费使用，而且没有任何广告，同时也并没有任何变现计划，但是如此惊人的使用量是无论如何都难以忽视的。当用户在旅游的时候推一下餐馆的广告啦，其它场景使用的时候放一下语言学习的广告啦……

<br>

2. [Peter Thiel 将在斯坦福大学任教一门德语课](https://www.theverge.com/2018/7/25/17614764/peter-thiel-stanford-teach-german-course-globalization)

![peterThiel](~@weeklyImages/issue0/peterThiel.png)
Photo by Chip Somodevilla/Getty Images

根据一名斯坦福学生的推文，PayPal 联合创始人 Peter Thiel 将会作为讲师之一，教授一门名为  “German 270: Sovereignty and the Limits of Globalization and Technology.” 的课程。

看到这个新闻的第一时间是在想，创业成功的人都是神吧，还能用另外一种语言讲课？查了一下他的资料，才知道他出生于西德，拥有美国国籍，所以能说一口流利的德语。

## `:// Code`
[GoogleChromeLabs 开源 node 调试工具 ndb]()

![peterThiel](~@weeklyImages/issue0/ndb.gif)
Medium@Indrek Lasn

既然是谷歌出品的，怎么也得试一下吧。

项目GitHub地址： https://github.com/GoogleChromeLabs/ndb

## `:// Tech`
1. [Chrome 68 将所有 HTTP 链接标记为 Not Secure]()

![chrome](~@weeklyImages/issue0/chrome.png)
Google

由2017年的 Chrome 56开始，要求用户输入密码或包含填写付款信息的网站，在 URL 栏会显示 Not Secure 警告。在今年7月的升级中，Chrome 终于决定在所有不加密链接前面显示“不安全”文字标记。

我的 Chrome 在今天早上已经升级到了68，公司项目公测的地址就赫然被标记为 Not Secure 了。

<br>

2. [Star Trek 的粉丝们，现在可以买到梦寐以求的无线耳机了]()

![starTrek](~@weeklyImages/issue0/starTrek.png)
ThinkGeek

一图胜千言。

## `:// Science` 
[21世纪持续时间最长的月蚀即将到来]()

![bloodMood](~@weeklyImages/issue0/bloodMoon.jpg)
REUTERS/Mike Blake

这次月蚀将会持续1小时43分钟，可能是百年来持续时间最长的一次月蚀。
在广州，这次 Blood Moon（由于其微红的颜色，月全蚀也被称作“血月”）的持续时间，将会从本周六（2018-07-28）早上的3:30 一直到5:13，至于什么方位和角度能够看到嘛，表示并没有看懂，估计也不会半夜爬起来看了。当天的新闻估计就会有快进动图之类的吧（笑）。

其它地区的月蚀时间和方位可以查询这个网站，搜索自己所在地区的拼音名称即可。

## `:// Music`
下面这两位是这两周我听得最多的音乐人，以创作动漫、电视剧配乐为主。

**泽野弘之**

作  品：
动画《机动战士高达 UC》、《罪恶王冠》、《进击的巨人》、《甲铁城的卡巴内瑞》配乐

Top 3（个人主观评价，排名不分先后）：
- Mad-Nug
- 祓魔师强奏曲 第二楽章: X
- tragedy

<br>

**林友树**

作  品：
电视剧Legal High系列、BOSS女王系列配乐，动画《我的英雄学院》配乐

Top 3（他本人最喜欢的三首）：
- 三角迷踪的“guardian dance”
- 教我爱的一切的“Prelude”
- 草莓之夜的“Sleeping Beauty”

<CopyRights />