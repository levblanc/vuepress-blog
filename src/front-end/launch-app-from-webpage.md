# Deep Linking：H5 页面调起 App 

<PublishDate date='2018-09-11' />

这种需求是前端工程师在夹缝之中求生存的完美体现。

经验文章在网上可以搜到不少，但读起来感觉比较杂乱，可能因为大家都结合了各种需求情景，附带了大段的判断代码吧。在搜集资料的过程中，也发现国内外都有团队提供专门做这件事的 SDK，所以这件事的复杂程度也可见一斑了。

这里按照自己的思路写一篇，力求清晰明了，仅总结 H5 调起 App 的各种方法，作为文档型的存在，方便查询阅读。


## `:// 不可能的任务`

奇葩的需求总是层出不穷，如果你也有遇到以下的需求，请不要浪费时间：

<br>

1. 检测用户是否已经安装 App，有安装则直接唤起，没安装则跳转下载页

说这个任务“不可能”，并不是没有办法完成。而是**无论如何体验都不会好。**

首先，浏览器没有办法“检测”到其它APP的安装情况。即使有，它也没有提供任何 API 给前端工程师来查询。

其次，由于没有办法得知是否成功打开了 App，所以使用`setTimeout`定时跳转的话，在用户没有安装 App 的时候，就会出现既没有打开 App 又跳转了页面的情况，即使用户手动回退，也还是会不断被定时器把页面跳转掉。

<br>

建议：

分开 “下载 App” 和 “打开 App” 两个按钮，各自完成自己的功能。

<br>

2. 微信中，H5 里点击分享按钮后，复制页面所有图片，打开微信朋友圈编辑界面，并黏贴图片
  
<br>

建议：

做一个 App 放到应用宝，再用 H5 唤起 App，让 App 完成黏贴图片的操作。（注：强扭的瓜不会甜）


## `:// 工作原理`

web 端和 App 端共同商定一个 URI 协议（scheme-based uri）：

```bash
yourapp://path/to/page
```

App 里面会注册一个命令，只要接收到这个命令，它就会直接打开。

系统中已经安装好 App 之后，当 H5 尝试以该协议打开页面时，系统会把这个 Url 转换成命令，从而打开 App，并且传递数据。


## `:// 两种唤起方式`

经过各种折腾后定下来的方案是：

iOS 系统，使用 `window.location.href`

```javascript
  window.location.href = 'yourapp://path/to/page'
```

Android 系统，使用`iframe`

```javascript
  const iframe = document.createElement('iframe');

  iframe.style.display = 'none';
  iframe.src = 'yourapp://path/to/page'

  document.body.appendChild(iframe);
```


## `:// Android Intent`

大于 version 25 的 手机 Chrome 浏览器，完全不支持 URI Scheme （你猜我是怎么知道的？苦笑），要使用`intent`。跟 App 端的约定还是一样的，只是前端的写法需要根据`intent`语法修改而已，并且增加`package`名称。

根据 Chrome [官方的说明](https://developer.chrome.com/multidevice/android/intents)，语法如下：

```
intent:
  HOST/URI-path // Optional host 
  #Intent; 
    package=[string]; 
    action=[string]; 
    category=[string]; 
    component=[string]; 
    scheme=[string]; 
  end; 
```

所以判断出浏览器类型和版本后

```javascript
// 在 js 中控制跳转
window.location.href = 'intent://path/to-page/#Intent;scheme=yourapp;package=com.yourapp.package.name;end'
```

```html
<!-- a 标签用户点击跳转 -->
<a href='intent://path/to-page/#Intent;scheme=yourapp;package=com.yourapp.package.name;end'>打开app</a>
```


## `:// iOS Universal Links`

从 iOS 9 开始，苹果就强推自家的 Univeral Links 方案，具体的说明可以在它家[开发者官网](https://developer.apple.com/ios/universal-links/)找到。

看了一圈，配置步骤相对繁复，需要 App 端、web 端、以及服务器上一齐做配置，甚至连负责 iOS App 开发的同事都不清楚这东西。

URI Scheme 虽然仍然可用，但是会弹出错误提示框，或询问用户是否访问。


## `:// Android App Links`

Android 6.0 (API level 23) 开始，加入了 [App Links](https://developer.android.com/training/app-links/)。感觉上跟 iOS 的 Universal Links 很类似。

以后，不会大家都采取这种方式了吧。（苦笑）


## `:// 结语`

目前所知的调起方式就上面总结的这几种，情景判断上，根据自身需求进行调节就可以了。

主要是系统版本、浏览器版本的不同，可能会导致各种判断交叉存在，使得代码可读性变差。但是这也没办法了，谁叫 web 是跨平台的呢。

<br>

---

参考文章

[1] [Universal Links and Deep Links: What's the difference?](https://www.adjust.com/blog/universal-links-vs-deep-links/)

[2] [Universal Links, URI Schemes, App Links, and Deep Links: What’s the Difference?](https://blog.branch.io/universal-links-uri-schemes-app-links-and-deep-links-whats-the-difference/)

[3] [Android Intents with Chrome](https://developer.chrome.com/multidevice/android/intents)

[4] [Deep Linking：从浏览器调起APP](https://harttle.land/2017/12/24/launch-app-from-browser.html)

[5] [iOS/Android 浏览器(h5)及微信中唤起本地APP](https://segmentfault.com/a/1190000006127431)

[6] [浏览器中唤起native app || 跳转到应用商城下载](https://segmentfault.com/a/1190000005848133)

[7] [h5唤起app](https://echozq.github.io/echo-blog/2015/11/13/callapp.html)

[8] [Handling Android App Links](https://developer.android.com/training/app-links/)

[9] [Android深度链接 Deep Links 和 App Links](https://www.jianshu.com/p/1632be1c2451)

<CopyRights />