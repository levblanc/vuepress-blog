## Quotes

***Progress over Perfection.***

---

***He who laughs at himself never runs out of things to laugh.***

*-- Epictetus*

---

***Fate leads the willing and drags along the reluctant.***

*-- Seneca*

---

***Everybody wants to go to heaven, but nobody wants to die.***

*-- US Marine Proverb*

---

***Finding an opportunity is a matter of believing it's there.***

*-- Barbara Corcoran*

---

***If one does not know to which port one is sailing, no wind is favorable.***

*-- Seneca*

---

***Begin at once to live, and count each separate day as a separate life.***

*-- Seneca*

---

***Change yourself and everything will change for you.***

*-- Jim Rohn*

---

***Thoughts become things, choose them wisely!***

***Be careful how you interpret the world: It is like that.***

***In the end, we are our choices. Build yourself a great story.***

*-- Jeff Bezos*

---

***You have to trust in something; your gut, destiny, life, karma, whatever, because believing that the dots will connect down the road, will give you the confidence to follow your heart, even when it leads you off the well-worn path. And that will make all the difference.*** 

*-- Steve Jobs*

---