---
title: What is Functional Programming?
date: 2019-03-05 
updated: 2019-03-05
categories: [front-end]
tags: [js, 'functional programming', fp]
---

# What is Function Programming?

Functional programming (often abbreviated FP) is the process of building software by composing pure functions, avoiding shared state, mutable data, and side-effects. Functional programming is declarative rather than imperative, and application state flows through pure functions. Contrast with object oriented programming, where application state is usually shared and colocated with methods in objects.

- Pure functions
- Function composition
- Avoid shared state
- Avoid mutating state
- Avoid side effects
- reusability

liked it, but it is really hard to explain to newbies

- map
- filter

- compose
- pipe

## Pure Function

A function is only pure if, given the same input, it will always produce the same output.

```js
// pure function
function double(num) {
  return num * 2
}

// not pure
Math.random()
```

Function composition is the process of combining two or more functions to produce a new function. Composing functions together is like snapping together a series of pipes for our data to flow through.


---
References:

[1] [Master the JavaScript Interview: What is Functional Programming?](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-functional-programming-7f218c68b3a0)

[2] [Master the JavaScript Interview: What is Function Composition?](https://medium.com/javascript-scene/master-the-javascript-interview-what-is-function-composition-20dfb109a1a0)