# 有意思周报 issue:2

<PublishDate date='2018-08-09' />

> ***本期是 Curated List 专刊。***

## `://TL;DR`

根据自己的兴趣或需要，建立并维护一个有索引的资源列表吧。这是一个记录人生逐渐丰富起来的过程呢。

## `://好奇`

发现 Curated List 这东西，完全是个意外。

例行阅读每日 RSS feed 的时候巧遇了一篇 Git 的基础教程文章，看起来是个新网站，所以就随意逛了一下。发现它家有个 [Resource Tab](https://alligator.io/resources/)，好奇就点进去看看到底是啥 resource 吧，结果如图：

![alligator](@weeklyImages/issue2/alligator-screenshot.png)

啊，果然是资源大合集呢，涵盖了每个框架的方方面面。但是，Curated List 这英文到底是什么意思？

## `://寻找`

找答案的时候，竟然直接有个联想是这样的：

![googleSearch](@weeklyImages/issue2/google-search.png)

没查出来中文翻译，就直接看看人家是怎么做的吧？点入上图的[第一个结果](https://github.com/sindresorhus/awesome)看一眼，这目录还挺长的：

![awesomeList](@weeklyImages/issue2/awesome-list.png)

如果也是开始的那个网站那种资源合集，这里涵盖的信息范围还是十分广阔的呢。看一眼 Star 数：90,260。嗯？作为一个文档型的项目，这么高的 Star 数，这里面肯定是有什么不得了的好东西。

如果你点击目录链接去看每一项，你会发现里面都藏着一个细分 list，列明该项下面有用的资源链接，由本项目作者或者其他人进行维护。比如，我走了下面这条路径：

Books -> Mind Expanding Books -> Startups and Business

得到的最终结果是这么一个表格：

![books](@weeklyImages/issue2/books.png)

先不论这些书的选择会不会太过主观，从 Good Reads 的评分来看，基本都是4分以上的评价，首先证明这些书都是值得一读的。

其次，这个 Books 列表下，又覆盖了各个不同的范畴，证明维护者的涉猎非常广泛。

第三，从更新时间来看，项目还是有在积极维护的（资源新鲜，且常更新）。

浏览一下各个条目，有些条目里的书的数非常多，而有的则只有一两本。我认为这从一个侧面反映出其实维护者本身是真的有读过，才放上来的。如果仅仅是一个资源合集，在网络这么发达的年代，随便一搜就一堆了，尽可以随便放，没必要只写一两本。

## `://思考`

这个时候回过头来想 Curated List 这件事。从 awesome 这个项目为入口往下发散，如果每个条目并不是这项资源的尽头，而是再嵌套着其它列表，这种网状发散的力量就很可怕了。在 GitHub 上，就真的有人收集出来[这么一个项目](https://github.com/jnv/lists)：

![books](@weeklyImages/issue2/lists-of-lists.png)

你可能会说，这不类似于思维导图嘛。可是你仔细想想，思维导图是个图而已，这些 Curated List 是实打实的资源链接！当然，把一个思维导图做成 Curated List 的话，潜力也是非常大的。

## `://扩散`

最后，由此扩散出来的一个思维就是，如果你想搜索某一方面的资源，不妨试试搜索 “资源名称 + curated list” ，如果有匹配的结果，你将可以从里面找到更多具有针对性的资源。

a. design curated list 搜索结果：
![designCurated](@weeklyImages/issue2/design-curated.png)

b. photography curated list 搜索结果：
![photoCurated](@weeklyImages/issue2/photo-curated.png)

我瞎试了几回，感觉还是跟互联网、科技相关的搜索会比较容易出结果。随便搜一种编程语言的名称，比方 “python curated list”，结果非常多，而如果搜 “gardening curated list” 的话，三个词都匹配到的结果非常少，而且质量也不高。

<br>

<CopyRights />