---
title: 30分钟上手RequireJS打包及优化
date: 2015-08-16 16:06:38
updated: 2018-12-18
categories: [front-end]
tags: [requirejs, rjs]
---

# 30分钟上手RequireJS打包及优化

本篇记录的是在不使用任何自动化工具（如 Grunt 或 Gulp ）的情况下，使用 r.js 对项目进行打包和优化。
使用自动化工具时配置的方法基本大同小异，届时根据该工具所用插件的文档进行适当调整即可。

<!-- more -->

## 本机环境 

|           | version  |
|-----------|----------|
| OS        | Yosemite 10.10.2 |
| Git       | 1.9.5 |
| Nodejs    | 0.10.32 |
| requirejs | 2.1.19 |

几个简单的优化目标：

- 把通用库打包到一个文件
- 自定义的模块按需进行合并
- js文件压缩

## 文件夹结构
首先，我从[官网链接](http://requirejs.org/docs/download.html#rjs)找到r.js，并下载到对应文件夹中。

当前文件夹结构如下（文件夹中的内容均与[30分钟上手RequireJS](https://levblanc.com/front-end/get-started-with-requirejs-in-30-min.html)的一致）：

```bash
dev
├── assets
│   └── js
│       ├── comm on.js  /* 此文件为空，是为了最后把通用依赖包都打包到一齐而准备的。*/
│       ├── global.js
│       ├── lib
│       │   ├── backbone-1.1.2.min.js
│       │   ├── jquery-1.11.2.min.js
│       │   ├── r.js
│       │   ├── requirejs-2.1.19.min.js
│       │   └── underscore-1.8.3.min.js
│       ├── requirejsConfig.js
│       └── tipText.js
├── index-modern.html
├── index-old-school.html
└── testRequire.js

```

## 创建build.js
在`dev`文件夹的同层新建`build.js`文件。它是使用r.js进行优化时需要用到的配置文件。

期望在进行优化后，文件夹结构如下：

```bash
.
├── dev
├── dist   /* 最后使用r.js创建的文件夹，不需要手动创建 */
└── build.js
```

接下来看看`build.js`文件常用的配置项：

```javascript
appDir: 'dev'
```

`appDir`顾名思义，指向的就是项目源文件的文件夹。

```javascript
baseUrl: 'assets/js'
```

`baseUrl`指的是模块文件的base路径。

```javascript
dir: 'dist'
```

`dir`指向的是优化后的文件的存放文件夹。

所以，`appDir` 是源，`dir`是目的地。

```javascript
mainConfigFile: 'dev/assets/js/requirejsConfig.js',
```

`mainConfigFile`指向的是RequireJS的配置文件，详情可见[30分钟上手RequireJS]()。

```javascript 
modules: [
	/* 首先定义通用的module */
	{
		/* 每个module的name是相对于baseUrl的路径 */
		name: 'common',
		include: [
			'jquery',
			'backbone',
			'underscore',
			'requirejs'
		]
	},
	/* 下面是每一个页面需要用到的模块 */
	/* include页面依赖的模块，这里只把tipText和global合并 */
	/* exclude已经打包好的common */
	{
		name: 'tipText',
		include: ['tipText', 'global'],
		exclude: ['common']
	}
]
```

`modules`就是进行打包合并的重点部分。首先把通用的依赖包打包到common.js里面。再处理每一个页面需要的模块。由于已经有common这个模块，各个页面的模块中就不需要把它包含在内了。

```javascript
paths: {
	backbone: 'empty:'
}
```

在`requirejsConfig.js`中设置了从CDN请求的文件，必须作如下设置，否则会报错。

```javascript
optimize: 'uglify2'
```

`optimize`项配置的是需要使用的JS压缩引擎。

```javascript
optimizeCss: 'standard'
```

`optimizeCss`顾名思义，压缩CSS的模式。配置`standard`就可以压缩CSS文件。
这时候，也许你会说，连CSS都可以帮忙压缩真是太强大了。而我个人就认为：未免管得太多了。

更多配置选项，可以参考官方的[example.build.js](https://github.com/jrburke/r.js/blob/master/build/example.build.js)。

## 开始打包和优化

进入项目文件夹下，运行以下命令，就可以开始打包和优化了。

```bash
$ node dev/assets/js/lib/r.js -o build.js
```

完成以后打开`build`文件夹中的`index-morden.html`, 跟原来的一样，没有变化。

```javascript
require(['tipText', 'global', '../../testRequire'], function (tipText, GLOBALVAR, test) {
	console.log(GLOBALVAR);
});
```

但其实这个时候已经不需要分别`require` `tipText`和`global`了，因为已经合并到`tipText`文件中，所以可以改成：

```javascript
require(['tipText', '../../testRequire'], function (tipText, test) {
	console.log(GLOBALVAR);
});
```

打开控制台，刷新页面，依然可以见到log出来的`GLOBALVAR`对象。

在开发者工具的`networks`查看`tipText.js`，可以见到其实源代码中的`tipText`和`global`的内容已经合并，并且压缩。

## 主观使用感受

1. 初学者极度不友好。无论是RequireJS本身，还是优化工具r.js，都需要各种繁琐的配置。

2. 没有简明清晰的说明文档。文档是很详尽，可惜我没有看明白。花了很多时间去搜索研究，极大地增加了学习成本。

3. 打包前后`require`的文件可能会变化。我把`tipText`和`global`合并了，还要到引用的地方改一下。（可能是还没有找到章法吧）

4. 研究到了最后，并没有预期中的效果那么好。（预期：各种配置？好吧，好好配置，不然机器怎么知道哪是哪呢？结果：怎么我都配好了，还是有浓重的不智能感？）

<CopyRights />