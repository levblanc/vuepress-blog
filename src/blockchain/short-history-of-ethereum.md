# Ethereum 小史

<PublishDate date='2018-11-07' />

## Ethereum (Vitalik Buterin, 2013)

Vitalik Buterin，因为创造了 Ethereum（以太坊），人称V神。

V神是 Bitcoin 的支持者，但认为 Bitcoin 这个项目本身太过于简单了。他觉得区块链这种创新的技术，不应该只用来处理转账这件事，而应该用来创造更多更复杂的程序，并用于其它类型的交易。

要开发程序，就需要有可编程入口。Ethereum 的核心，就是这个入口：

**Smart Contract（智能合约）**，一份存在于以太坊区块链上的代码。

程序员可以通过这份代码在区块链上完成一些事情，或者向另外一份智能合约（另一份代码）、或用户传递信息。

在 Bitcoin 的世界里，只能完成很简单的操作，例如：

李雷转100块钱给韩梅梅。

但是，如果李雷想预约**每个月都自动转账**100块给韩梅梅，就没办法做到了。

而 Ethereum 为这件事提供了解决一个方案。智能合约为什么智能？因为它的本质就是一份代码。可以编程，就等于可以对各种条件进行判断，每月100块钱的预约转账就成为了可能。

这种可编程的特点，让 Ethereum 成为了一个基于区块链的开放平台，程序员可以在上面开发去中心化的程序，这也激发了世界对区块链的各种可能性的想象。

（注：这个项目于2015年7月上线。）

## 群雄并起

在这之后，逐渐出现了各种区块链网络 Ripple, Stellar, Hyperledger 等等等等。他们都标榜自己专注于解决某一类问题。

但无论是哪一种方案，其实都是基于区块链技术的一种协议（protocol），或者说是一个让 Dapp 开发这件事变得更容易的手段而已。

所以，V神开了一扇门之后，可以说是各路都看到了光。

## 资源

项目官网：[https://www.ethereum.org/](https://www.ethereum.org/)

官方项目白皮书：[https://github.com/ethereum/wiki/wiki/White-Paper](https://github.com/ethereum/wiki/wiki/White-Paper)

给普通人的白皮书：[Ethereum White Paper Made Simple](https://blockchainreview.io/wp-content/uploads/2018/03/02.01._final_Ethereum-White-Paper-Made-Simple.pdf)

<CopyRights />