---
title: 如何使用Hexo搭建Github Pages博客
date: 2015-07-13 21:04:00
categories: [front-end]
tags: [hexo, 'github pages', blog]
---

# 如何使用Hexo搭建Github Pages博客

使用hexo搭建一个博客，并托管在github pages上的简易教程。

<!-- more -->

## 写在前面

本机环境：
- OS:     Yosemite 10.10.2
- Git:    1.9.5
- Nodejs: 0.10.32
- Hexo:   3.1.1

需要使用npm安装的命令，可以使用cnpm代替，有助提升速度和成功率。
cnpmjs = 淘宝NPM镜像。有[英文版说明](https://cnpmjs.org/) 和 [中文版说明](https://npm.taobao.org/)

在本机已安装好Node.js、Git、已有github账号并且配置好SSH key的状态下，
跟随正文步骤安装并部署到github pages总过程不超过30分钟。

如果是小白，需要从零开始的话，各种安装所经历的折腾都是必须的。

## 安装Node.js及Git

具体方法参见 [Node.js](https://nodejs.org/) 及 [Git](http://git-scm.com/) 官网教程。

关于SSH key的生成和配置，github有详细的[帮助文档](https://help.github.com/articles/generating-ssh-keys/)可以参考。

## 创建博客用的git仓库

仓库名称**必须**为 **username**.github.io

注：username为你在github的用户名

假设github用户名为john，那你的仓库就必须是`john.github.io`。如果你创建一个叫做`jason.github.io`的仓库，最后deploy完以后打开页面是会报错的。

{% asset_img "create-a-new-repository.png" "Figure 1. 创建博客用git仓库" %}

（图中标红报错是因为仓库已经创建）

仓库创建成功后，github会给你该仓库的https和ssh地址，复制ssh地址备用。
{% asset_img "new-repo-created.png" "Figure 2. 创建成功" %}

## 克隆git仓库到本地

使用GUI工具或命令行，把刚刚创建好的git仓库克隆到本地文件夹中。

## 安装Hexo

```bash
$ cnpm install -g hexo-cli
```

## 使用命令行，创建Hexo博客项目  

```bash
<!-- 进入刚才创建的博客文件夹 -->
$ cd <your-blog-folder>
    
<!-- 初始化博客 -->
$ hexo init
    
<!-- 安装博客项目所需的packages -->
$ cnpm install
```

## 安装hexo-server

从Hexo 3开始，hexo-server从hexo项目中分离了出来。需要另外安装，才能在本地把blog跑起来。在命令行运行下面这条的命令：

```bash
$ cnpm install hexo-server --save
```

server安装成功后，启动server便可以把博客跑起来：

```bash
$ hexo server
```

默认本地运行地址是 http://localhost:4000
{% asset_img "hexo-blog.png" "Figure 3. 本地运行" %}


## 在Hexo中配置项目部署信息

在博客文件夹根目录下，找到文件`_config.yml`。在deploy部分填写配置信息，把repository值设置为刚刚复制好的ssh地址。

```yml
deploy:
  type: git
  repository: <your-github-repo-ssh-address>
  branch: master
```

## 部署到Github Pages

原生项目中已有一篇默认博客hello world，可以马上尝试部署到github pages。

删除database和public文件夹
（第一次生成网页可以跳过这一步，日后在本地写文章的时候，可能会有更改md文件名等操作，清空这两个地方可以在`generate`的时候生成一份最新的项目结构）

```bash
$ hexo clean
```

运行下面的命令生成部署所需的文件：

```bash
$ hexo generate
```

在日后写新post的时候如果想实时看到博客展示的变化，可以运行

```bash
$ hexo generate --watch
```

完成后就可以进行部署：

```bash
$ hexo deploy
```

如果上面这个步骤报错：`ERROR Deployer not found : github`，尝试安装`hexo-deployer-git`:

```bash
cnpm install hexo-deployer-git --save
```

完成后再次执行`hexo deploy`命令，应该可以解决问题。

如果在部署过程中报 `fatal: Not a git repository (or any of the parent directories): .git` 这个错，尝试在博客所在文件夹下运行`git init`命令，然后再次进行部署。

部署成功后，打开[https://your-github-username.github.io](https://your-github-username.github.io)，就能看见默认博客。

至此，hexo的博客生成和部署已经全部成功。

## 创建新 post

在自己的blog目录下，运行下面的命令便可以快速创建一个新post

```bash
$ hexo new [layout] "your-post-title"
```


注：

1. `post`是默认的`layout`，`title`是新post的md文件名和url对应地址。

2. `title`不要带空格，如果有空格，创建文件的时候文件名只会是最后一个空格后面的文字。它将会用在该post的url中，而且如果`_config.yml `中的`post_asset_folder ` `true`（是否生成与每篇文章对应的资源文件夹，默认值为`false`），资源文件夹也会直接使用`title`值，所以建议最好使用英文来创建。真正的文章题目可以在md文件中修改。

3. post创建成功后，在 /source/_posts 文件夹下，会生成一个新的markdown文件，文件名就是你刚刚的`title`。


<CopyRights />