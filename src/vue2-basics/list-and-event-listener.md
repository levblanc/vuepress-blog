# Part 5 列表渲染和事件监听

<PublishDate date='2017-01-22' />

之前在 Vue 里面绑定数据，都只是单个地绑定。这期我们来看一下怎样渲染列表，然后通过事件监听方法往列表里面增加 item。

## 列表渲染

废话不多说，直接上代码：

```html
<div id="app">
    <ul>
        <li v-for="item in list">{{ item }}</li>
    </ul>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            list: [
                '星际牛仔',
                '攻壳机动队',
                'Blassreiter',
                '鸦KARAS',
                '黑之契约者'
            ]
        }
    });
</script>
```

直接看效果，可见`v-for`循环了一遍`list`，把其中的每一项都绑定到`li`中去：

![v-for](@vue2basicsImgs/p5/v-for.png)

唯一需要注意的是，给`v-for`的值是`<item> in <list>`的形式。很容易就直接写`v-for="list"`了。

HTML的部分，也可以用`v-text`来代替大胡子语法：

```html
<div id="app">
    <ul>
        <!-- <li v-for="item in list">{{ item }}</li> -->
        <li v-for="item in list" v-text="item"></li>
    </ul>
</div>
```

其实也可以直接循环数字，虽然好像实际中会这样用的机会不大：

```html
<div id="app">
    <ul>
        <li v-for="item in count" v-text="item"></li>
    </ul>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            count: 10
        }
    });
</script>
```

## 事件监听

先做一个简单的例子：点击按钮之后 alert。

Vue的事件监听，使用的是`v-on`指令，后面跟的就是需要监听的事件。

```html
<div id="app">
    <button class="btn btn-success" v-on:click="clickMe">点击我吧，主人！</button>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            count: 10
        },
        methods: {
            clickMe: function () {
                alert('感受到了主人的点击！')
            }
        }
    });
</script>
```

![v-on-click](@vue2basicsImgs/p5/v-on-click.png)


在页面监听用户交互事件是非常常用的，但是如果每次都要写`v-on`，就会很繁琐了，所以vue也为我们准备好了简写方法：

```html
<button class="btn btn-success" @click="clickMe">点击我吧，主人！</button>
```

要记得`v-bind`的简写是`:class`，用的冒号，但是`v-on`用的是`@`，像`@click`这样。

最后我们把这两个新知识合并到一起，做一个可以让用户通过输入文字来新增列表项的列子吧。

```html
<div id="app">
    <ul>
        <li v-for="item in list">{{ item }}</li>
    </ul>

    <div class="input-group">
        <input type="text" class="form-control" v-model="userInput">
        <span class="input-group-btn">
            <button class="btn btn-default" type="button" @click="addItem">增加</button>
        </span>
    </div>        
</div>

<!-- ... ... -->

<script type="text/javascript">
    var userInput = ''
    var app = new Vue({
        el: '#app',
        data: {
            list: [
                '星际牛仔',
                '攻壳机动队',
                'Blassreiter',
                '鸦KARAS',
                '黑之契约者'
            ],
            userInput: userInput
        },
        methods: {
            addItem: function () {
                this.list.push(this.userInput)
                this.userInput = ''
            }
        }
    });
</script>
```

这里需要特别讲的，也许就只有`this`了。`this`指的其实是我们创建的这个 Vue 实例，也就是`app`。在 vue-devtools 里面可以看到（在 console 里面 log 一下 app 也可以找到，由于图片会太长，这里就不展示了）：

![root-this](@vue2basicsImgs/p5/root-this.png)

最后是完成后的效果：

![final-example](@vue2basicsImgs/p5/final-example.png)

![final-example-update](@vue2basicsImgs/p5/final-example-update.png)


## 写在最后

源码地址：[https://github.com/levblanc/vue-2-basics](https://github.com/levblanc/vue-2-basics)

视频攻略：自制 [本期视频攻略](http://www.bilibili.com/video/av7699340/) 在此。

<CopyRights />
