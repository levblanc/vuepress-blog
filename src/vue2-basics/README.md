# PART 1 跟世界打个招呼吧

<PublishDate date='2016-12-25' />

:::tip NOTE
这是一个纯新手向的攻略系列，它：

√ 使用最简单的文字进行解释

√ 每期分享一个点，长度适中，适合碎片时间阅读

√ 图片均压缩在50k以下，把流量消耗降到最低（其中一期因为截屏了比较大的面积用来展示效果，所以会稍微超出这个限制）
:::

## 启程

本系列是对VueJS 2.0（使用版本v2.1.3）最基础的东西的介绍，所以采用直接在html里面引入`vue.js`的方式。为了使界面稍微养眼一点，直接使用bootstrap的css。

Sa，我们马上来跟世界问好吧！

假设我们有这样一个`data`对象，希望把`data.info`的值绑定到html中：

```javascript
var data = {
    info: 'hello hacker cafe'
}
```

操作很简单（`div#app`和`script`之间还有vue的引入，为了节省空间使用省略号代替）：

```html
<body>
    <!-- vue对象绑定的元素 -->
    <div id="app">
        <!-- 直接放入数据对应的key -->
        <h1>{{ info }}</h1>
    </div>

    <!-- ... ... -->

    <script type="text/javascript">
        var data = {
            info: 'hello hacker cafe'
        }

        var app = new Vue({
            // 绑定html元素
            el: '#app',
            // 绑定数据
            data: data
        })
    </script>
</body>
```

当然js的部分还可以简化为：

```html
<script type="text/javascript">
    var app = new Vue({
        // 绑定html元素
        el: '#app',
        // 绑定数据
        data: {
            info: 'hello hacker cafe'
        }
    });
</script>
```

## 大胡子（Mustache）
简单地说一下双重花括号这种语法。在Vue里面它只接受JS表达式。所以下面这几种写法都是ok的：

```html
<div id="app">
     <!-- info值续上，为了节省空间省略其它代码 -->
     <h2>{{ info.concat('!!!') }}</h2>

     <h2>{{ info.length }}</h2>

     <h2>{{ info ? 'has info' : 'no info'}}</h2>
 </div>
```

但是不要以为这就是在它里面可以写任何代码的意思，下面这些例子就会报错（例子来源于官网）：

```html
<div id="app">
    <!-- 这是赋值 -->
    <h2>{{ var ok = 1 }}</h2>

    <!-- 条件控制使用二元表达式，见上面正确的写法 -->
    <h2>{{ if (ok) { return info } }}</h2>
</div>
```

顺带说一下为什么直接引入未压缩的vue.js。在你跑上面这两行错误代码的时候，未压缩的版本会在console里面给出提示，而压缩了的版本是不会有提示的。可以自行尝试一下。

## 提前偷窥一下双向绑定

- 在console中打印出原来的info值

![在console中打印出原来的info值](@vue2basicsImgs/p1/app-info.png "在console中打印出原来的info值")

- 修改info值

![修改info值](@vue2basicsImgs/p1/app-info-update.png "修改info值")


## 写在最后：
源码地址：[https://github.com/levblanc/vue-2-basics](https://github.com/levblanc/vue-2-basics)

视频攻略：自制 [本期视频攻略](http://www.bilibili.com/video/av7473867/) 在此。

第一次弄视频，弄好以后才发现这件事看起来简单，但是挺耗时间和精力的……

这一期唠叨了，而且声音是从耳机上的麦克风直接录的，声音有点小。后续的找了个免费音频录制软件，能稍微加大点音量。

做的不好的地方大家多提意见和建议。

<CopyRights />
