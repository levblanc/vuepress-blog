---
title: 30分钟上手Bower
date: 2015-08-10 22:39:56
updated: 2018-12-18
categories: [front-end]
tags: [bower, 'package management']
---

# 30分钟上手Bower

Bower是一个前端的包管理工具，用于管理项目中的需要用到的依赖包十分方便。

<!-- more -->

## 本机环境

|        | version |
|--------|---------|
| OS     | Yosemite 10.10.2 |
| Git    | 1.9.5 |
| Nodejs | 0.10.32 |

- 使用[cnpm](https://npm.taobao.org/)代替npm 


## 安装Bower

1. 全局安装Bower

```bash
$ cnpm install -g bower
```

2. 查看版本信息

```bash
$ bower -v
1.3.12
```

## 使用Bower

安装完成后，可以在命令行直接使用`bower`命令。

下面来尝试新建一个文件夹，并在文件夹路径下执行：

```bash
$ bower install jquery
```

Bower会在该目录下生成`bower_components`文件夹，并把下载好的包放在这个文件夹里面。此时目录结构如下：

```bash
.
└── bower_components
    └── jquery
        ├── MIT-LICENSE.txt
        ├── bower.json
        ├── dist
        │   ├── jquery.js
        │   ├── jquery.min.js
        │   └── jquery.min.map
        └── src
        ... 
```

在执行刚才的`bower install`命令时，可以看到terminal里面的Bower进度提示如下：

```bash
$ bower install jquery
bower jquery#*                  cached git://github.com/jquery/jquery.git#2.1.4
bower jquery#*                validate 2.1.4 against git://github.com/jquery/jquery.git#*
bower jquery#~2.1.4            install jquery#2.1.4
```


从`bower jquery#~2.1.4`可以发现Bower是通过`#`号来确定需要下载的版本，我们没有指定版本，Bower自动帮我们下载最新的。所以，如果需要下载特定版本的包，可以在安装命令中使用`#`号来声明。

```bash
$ bower install jquery#1.9.1
bower jquery#1.9.1          not-cached git://github.com/jquery/jquery.git#1.9.1
bower jquery#1.9.1             resolve git://github.com/jquery/jquery.git#1.9.1
...
```

`bower install`命令还有更多其他[options](http://bower.io/docs/api/#install)可以使用。这里暂时不作介绍。


## 查看当前Bower下载过的包

`bower list`命令可以查看当前下载过的包

```bash
$ bower list

bower check-new     Checking for new versions of the project dependencies..
bower-test /Users/levblanc/projects/bower-test
└── jquery#1.9.0 extraneous (latest is 3.0.0-alpha1+compat)
```

`bower list --paths`命令可以查看当前下载过的所有包的在项目中的对应路径。
**这个命令最有用的时刻，就是在其它工具中需要声明/配置前端依赖包地址的时候。**

```bash
$ bower list --paths

jquery: 'bower_components/jquery/jquery.js'
```

## 使用bower.json文件管理依赖包

跟NodeJs的`package.json`文件一样，可以通过`bower.json`文件来管理项目需要下载的包。这样项目组里的其它成员就不需要逐一去`bower install`各种包了。

`bower.json`文件的生成方法与`package.json`也非常类似。可以通过`bower init`命令来生成，也可以手动直接在项目根目录下创建。

使用`bower init`命令的话，Bower会问一些问题，直接回答即可。

```bash
$ bower init
? name: bower-test
? version: 0.0.0
? description:
? main file:
? what types of modules does this package expose?: amd
? keywords: bower
? authors: test@testbower.com
? license: MIT
? homepage:
? set currently installed components as dependencies?: Yes
? add commonly ignored files to ignore list?: Yes
? would you like to mark this package as private which prevents it from being accidentally published to the registry?: (y/N) ? would you like to mark this package as private which prevents it from being accidentally published to the registry?: Yes

{
  name: 'bower-test',
  version: '0.0.0',
  authors: [
    'test@testbower.com'
  ],
  moduleType: [
    'amd'
  ],
  keywords: [
    'bower'
  ],
  license: 'MIT',
  private: true,
  ignore: [
    '**/.*',
    'node_modules',
    'bower_components',
    'test',
    'tests'
  ],
  dependencies: {
    backbone: '~1.2.1',
    jquery: '1.9.0'
  }
}

? Looks good?: Yes
```

完成所有问题以后，会在项目根目录生成`bower.json`文件。手动创建的话，跟随对应格式即可。此时文件夹结构如下：

```bash
.
├── bower.json
└── bower_components
    ...
```

当项目文件夹下已有`bower.json`文件时，直接执行`bower install`命令，便会自动下载所有依赖包。

## 下载并保存依赖包到 bower.json

使用安装命令的`-S`option（注意S必须是大写），可以在下载的同时，把这次下载的包的信息自动加入到`bower.json`文件中。

```bash
$ bower install -S backbone
```

下载完成后`bower.json`文件如下：

```bash
  ...
  "dependencies": {
    "jquery": "1.9.0",
    "backbone": "~1.2.1"
  }
  ...
```

## Bower自定义

如果想要把依赖包下载到自己希望的目录，怎么办呢？很简单，使用`.bowerrc`文件就可以了。

在项目根目录创建`.bowerrc`文件，使用`json`格式来写。

```bash
{
    "directory": "app/vender/"
}
```

保存好以后，执行`bower install`命令可以见到效果。

```bash
.
├── app
│   └── vendor
│       ├── backbone
│       │   ...
│       ├── jquery
│       │   ...
│
└── bower.json
```

注意：`.bowerrc`文件需要与`bower.json`结合使用。在`.bowerrc`中声明了下载目录之后之所以能够自动下载包，是因为在`bower.json`的`dependencies `中声明过。

[更多bower配置选项，可参考官网](http://bower.io/docs/config/)

<CopyRights />