---
title: 前端安全
date: 2019-02-14 
updated: 2019-02-15
categories: [front-end]
tags: [xss, csrf, cors]
---

# 前端安全

## CORS

CORS: Cross-Origin-Resource-Sharing. [MDN - CORS](https://developer.mozilla.org/en-US/docs/Web/HTTP/CORS)

A Cross-Origin resource fails if it’s:

  - to a different domain
  - to a different subdomain
  - to a different port
  - to a different protocol

1. Enable CORS from the backend.
2. Proxy from the front end if you don't have any contorl over backend.
   - [Using Node.js as a Proxy for Angular.js Ajax Requests](http://shawnsimondeveloper.com/nodeproxyangular/)
3. JSONP (an outdated method)
   - [Can anyone explain what JSONP is, in layman terms?](https://stackoverflow.com/questions/3839966/can-anyone-explain-what-jsonp-is-in-layman-terms)


my demo: [github.com/levblanc/cors-demo](https://github.com/levblanc/cors-demo)

## CSRF & XSS

XSS: Cross-Site Scripting

- user login
- cookie set on browser
- attacker send unescaped scripts to the server and stored into database
- user loads that malicious script
- cookie got stoken and send to attacker's database
- attacker can fake himself as user 

[XSS - Cross Site Scripting Explained](https://www.youtube.com/watch?v=cbmBDiR6WaY)

CSRF: Cross-Site Request Forgery

- user login 
- cookie set on browser
- user receive a link via email or chat message
- link goes to a attacker's page(on other domain) and init a request to your website by submitting a form or embed the request link as image src
- manipulate user action because the request was sent with cookie, and the backend doesn't know it was the attacker.
- the attacker cannot see the final response data, so it is used for state-changing, not reading data

possible solution:

- re-auth by captcha or re-enter password
- generate unique token on every POST, PUT, DELETE requests, include token in both cookie header and body


form enctype values:

- application/x-www-form-urlencoded 	
- multipart/form-data
- text/plain

---
参考文章： 

[1] [XSS and CSRF, why should you care?](https://medium.com/@nielsvanderveer/xss-and-csrf-why-should-you-care-343bb38cfbae)

[2] [Common CSRF Prevention Misconceptions](https://www.nccgroup.trust/us/about-us/newsroom-and-events/blog/2017/september/common-csrf-prevention-misconceptions/)

