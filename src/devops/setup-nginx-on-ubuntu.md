# Ubuntu 16.04 上 Nginx 的安装

<PublishDate date='2018-08-07' />

::: tip NOTE
如果你还没对自己的服务器进行过配置，推荐阅读：[基本的服务器配置](https://levblanc.com/devops/ubuntu-server-setup.html)
:::

## 1. 同步所有软件包信息
作为best practice，在 Ubuntu 服务器安装任何软件包之前，都应该从同步一下 apt 仓库中的软件包（package）列表，更新所有软件包及其依赖包（dependency）的版本号信息。

```bash
$ sudo apt-get update
```

## 2. 安装 Nginx

```bash
$ sudo apt-get install nginx

# 输入上述命令后，会告知安装 nginx 需要占用 9,307 kB 空间，询问是否继续
Reading package lists... Done
Building dependency tree
Reading state information... Done
The following package was automatically installed and is no longer required:
  grub-pc-bin
Use 'sudo apt autoremove' to remove it.
The following additional packages will be installed:
  fontconfig-config fonts-dejavu-core libfontconfig1 libgd3 libjbig0 libjpeg-turbo8 libjpeg8 libtiff5 libvpx3 libxpm4 nginx-common nginx-core
Suggested packages:
  libgd-tools fcgiwrap nginx-doc ssl-cert
The following NEW packages will be installed:
  fontconfig-config fonts-dejavu-core libfontconfig1 libgd3 libjbig0 libjpeg-turbo8 libjpeg8 libtiff5 libvpx3 libxpm4 nginx nginx-common
  nginx-core
0 upgraded, 13 newly installed, 0 to remove and 23 not upgraded.
Need to get 2,857 kB of archives.
After this operation, 9,307 kB of additional disk space will be used.

# 输入 'y' 后回车继续即可
Do you want to continue? [Y/n] y
```

## 3. 打开防火墙

```bash
$ sudo ufw app list

# output
Available applications:
  Nginx Full
  Nginx HTTP
  Nginx HTTPS
  OpenSSH
```

如果你没有设置过防火墙，可以参考 [Ubuntu 16.04 服务器初始 Setup](https://levblanc.com/ubuntu-server-setup.md#step-5-设置基本的防火墙) 中的介绍。

Nginx Full 包括 Nginx HTTP 和 Nginx HTTPS。如果你的服务器已经安装了SSL证书，可以像我这样，直接允许 Nginx FULL，否则就只允许 Nginx HTTP 吧。 

```bash
$ sudo ufw allow 'Nginx Full'

# output
Rule added
Rule added (v6)
```

## 4. 查看防火墙状态

```bash
$ sudo ufw status

# output
Status: active

To                         Action      From
--                         ------      ----
OpenSSH                    ALLOW       Anywhere
Nginx Full                 ALLOW       Anywhere
OpenSSH (v6)               ALLOW       Anywhere (v6)
Nginx Full (v6)            ALLOW       Anywhere (v6)
```

## 5. 检查 Nginx 安装情况

5.1 查看当前版本号

```bash
$ nginx -v

# output
nginx version: nginx/1.10.3 (Ubuntu)
```

5.2 config 文件所在目录

```bash
$ ls  -l /etc/nginx/

# output
total 56
drwxr-xr-x 2 root root 4096 Jul 12  2017 conf.d
-rw-r--r-- 1 root root 1077 Feb 11  2017 fastcgi.conf
-rw-r--r-- 1 root root 1007 Feb 11  2017 fastcgi_params
-rw-r--r-- 1 root root 2837 Feb 11  2017 koi-utf
-rw-r--r-- 1 root root 2223 Feb 11  2017 koi-win
-rw-r--r-- 1 root root 3957 Feb 11  2017 mime.types
-rw-r--r-- 1 root root 1462 Feb 11  2017 nginx.conf
-rw-r--r-- 1 root root  180 Feb 11  2017 proxy_params
-rw-r--r-- 1 root root  636 Feb 11  2017 scgi_params
drwxr-xr-x 2 root root 4096 Aug  7 07:10 sites-available
drwxr-xr-x 2 root root 4096 Aug  7 07:10 sites-enabled
drwxr-xr-x 2 root root 4096 Aug  7 07:10 snippets
-rw-r--r-- 1 root root  664 Feb 11  2017 uwsgi_params
-rw-r--r-- 1 root root 3071 Feb 11  2017 win-utf
```

5.3 检查 Nginx 是否已经启动

```bash
$ systemctl status nginx

# output
● nginx.service - A high performance web server and a reverse proxy server
   Loaded: loaded (/lib/systemd/system/nginx.service; enabled; vendor preset: enabled)
   Active: active (running) since Tue 2018-08-07 07:10:15 UTC; 6min ago
 Main PID: 17101 (nginx)
   CGroup: /system.slice/nginx.service
           ├─17101 nginx: master process /usr/sbin/nginx -g daemon on; master_process on
           └─17102 nginx: worker process
```

至此，如果你的 nginx 是如上述 output 显示 **active(running)**，

在浏览器中输入当前服务器的公共ip，或者服务器绑定的域名，可以看到 nginx 默认的欢迎页

<br>

![nginx-welcome](@devopsImages/nginx-welcome.png)

<br>

---
参考文章:

[1] [apt-get(8) - Linux man page](https://linux.die.net/man/8/apt-get)

[2] [What does “sudo apt-get update” do?](https://askubuntu.com/questions/222348/what-does-sudo-apt-get-update-do)

[3] [How To Install Nginx on Ubuntu 16.04](https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-16-04)

<CopyRights />