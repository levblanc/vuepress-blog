# 有意思周报 issue:1

<PublishDate date='2018-08-02' />

## `:// World`
[八州联合起诉特朗普政府以制止3D打印枪械](https://www.fastcompany.com/90211037/washington-state-sues-the-trump-administration-over-3d-printed-guns)

![3dGun](@weeklyImages/issue1/3dGun.jpeg)
AFP/Getty Images

2013年，枪支倡导组织 Defense Distributed 的负责人 Cody Wilson 在网上上传了3D枪支解构图，引来众多网民进行下载，美国国务院勒令停止。

但在今年六月，国务院却解除了这项禁令，并允许该公司公布其3D枪支打印图纸，从8月1日起可以合法下载。而就在8月1日前数小时，西雅图联邦法官 Robert Lasnik 发布了一项临时限制令，禁止3D枪图纸的销售。

据说米国大学院校和公众地方都有3D打印机，而打印又不需要身份证明，随时随地打印零件就可以造一支枪，确实很危险呢。我的问题是，这3D图纸里头，包括子弹么？不过人家既然是枪支合法化，子弹应该也很容易解决啦。


## `:// Blockchain`

1. [文克莱沃斯兄弟第二次提交比特币ETF申请被拒](https://www.coindesk.com/winklevoss-brothers-bitcoin-etf-rejected-by-sec-for-second-time/)

![winklevoss](@weeklyImages/issue1/winklevoss.png)
coindesk

一年多前，文克莱沃斯兄弟第一次向美国证券交易委员会(Securities and Exchange Commission，简称SEC)提交比特币ETF申请，被拒绝。最近，兄弟俩又再次提出申请，SEC的回答依然是：不。

根据wiki，Exchange Traded Funds（简称ETF），“是一种在证券交易所交易，提供投资人参与指数表现的指数基金。ETF将指数证券化，投资人不以传统方式直接进行一篮子证券之投资，而是透过持有表彰指数标的证券权益的受益凭证来间接投资。”

所以如果SEC同意了基金中包含比特币，相当于是对虚拟货币在金融领域的一种莫大肯定，市场也肯定会为之震动。而SEC对此还是看得比较清楚的，它认为虚拟货币市场上，诈骗和操控的情况依然存在，而市场本身又无法对此进行监管。但同时也指出，他们的决定并不是针对比特币，或是区块链作为一种技术本身，所具有的创造性价值和投资价值。并认为随着时间推移，比特币相关的虚拟货币市场也会发展。

无论是家庭背景还是人生经历，都属于人生赢家级别的文克莱沃斯兄弟，miss 掉社交网络的一波互联网风口之后，又投身区块链领域，又是建立自己的比特币价格指数，还建立了世界上第一家ether交易所，看来每一次都是走在时代的尖端呢。

<br>

2. [Blockchain For Students 101 — The Basics (Part 1)](https://hackernoon.com/blockchain-for-students-101-the-basics-part-1-f39b8201a7d5)

![blockchain](@weeklyImages/issue1/blockchain.png)
Medium

作者用轻松简单的语言讲解区块链相关的概念（目前只更新到part 1）。对比起其它的讲解，这篇是我看过最易懂的。同时，在文中作者还预告了后续的文章更新，所以不妨 follow 一下。


## `:// Tech`

1. [Mozilla 正重新设计 Firefox 的 logo](https://blog.mozilla.org/opendesign/evolving-the-firefox-brand/)

![firefox](@weeklyImages/issue1/firefox.jpg)
MOZILLA

Mozilla 近日发布了一篇博客，表示公司正在开发新类型的浏览器、一众的 app 和服务，所以希望设计一套新的 logo ，以更好地展现新产品，同时提高其辨识度。博客中还鼓励用户给他们留言和反馈，希望可以根据这些信息来继续完善新 logo 的设计。

如果你有意见，可以通过标题链接到他们的博客留言区进行留言。

<br>

2. [室内监控用的无人机，你准备好了吗？](https://gizmodo.com/get-ready-for-indoor-surveillance-drones-1827997780)

![indoorDrones](@weeklyImages/issue1/indoorDrones.png)
Skysense

两家科技公司表示，他们正合作开发一款全自动的，用于室内安保的无人机。它们将会巡逻预先设置好的路线，记录看见的所有情形，并标识任何系统中认为有问题的东西，在电量低的时候自己会飞到充电座上进行充电。

机器人不会累，可以全天候进行巡逻，确实有市场需求。但是如果反过来想，这些机器人一旦被黑了呢？控制它的人也可以让它飞到建筑物之内的任何地方呢……

<br>

3. [Google 正式把 Chrome 加入到 Daydream VR 平台](https://www.theverge.com/2018/7/30/17630440/google-chrome-vr-webvr-launch-official-daydream-view-mirage-solo)

![chromeVr](@weeklyImages/issue1/chromeVr.jpg)
The Verge

VR 版的 Chrome 和桌面版的在功能上大致相同，但VR版的增加了一个影院模式（cinema mode）以优化在VR下观看网络视频的体验，另外还支持 WebVR 标准，所以只要是网页上有 VR 元素，在 Daydream VR 上都能体验到。

虽然不一定有人选择在 VR 设备上浏览网页，但是必要时想搜索一下 VR 使用技巧什么的，有了 Chrome 也方便很多吧。



## `:// Code`

[web 开发者的福音：Page Lifecycle API](https://developers.google.com/web/updates/2018/07/page-lifecycle-api)


![pageLifecycleApi](@weeklyImages/issue1/pageLifecycleApi.png)
Google

在移动端，安卓或 iOS 作为操作系统，可以控制 app 的生命周期以更好地分配资源。而浏览器页面长久以来，都并没有生命周期这个概念，页面一直都是活的。

虽然现代浏览器也已经开始采取一些手段来优化资源配置，但 web 开发者并没有办法得知浏览器在什么时候对页面进行了哪些干预。因此，浏览器也只能采取一些保守的行动，避免导致页面崩溃。

有了 Page Lifecycle API，开发者就有办法监听到浏览器针对页面的行为，并对其进行相应的处理，同时浏览器也可以更加大刀阔斧地优化资源了。

Google 开源了 PageLifecycle.js ，让我们可以轻松地跨浏览器使用这个 API。在电脑和手机上都试了一下官方 demo，效果还是不错的。



## `:// Science`

[NASA 公布阿波罗11号的历史录音记录](https://www.seriouseats.com/2018/07/japanese-knife-village.html)

![apollo11](@weeklyImages/issue1/apollo11.jpeg)
NASA

2018.07.31，NASA 公布了阿婆罗11号在登月任务中的录音，总时长超过19000小时。其中包括阿姆斯特朗那句“这是一个人的一小步，却是人类的一大步”。

不过，有一点让我很好奇，这个任务的时长是约9天，即使一直在说话，也说不出来19000小时的录音呀？


## `:// Picture`

![supernova](@weeklyImages/issue1/supernova.jpg)

钱德拉X射线影像，银河中最年轻的超新星遗迹 Cassiopeia A (via NASA/CXC/MIT/University of Massachusetts Amherst/M.D.Stage et al.)



## `:// Game`

Hotel Dusk: Room 215 （黄昏旅馆215号房）

![hotelDusk](@weeklyImages/issue1/hotelDusk.jpg)

Wikipeadia

2007年发行在DS上的一款文字冒险游戏。过气警探 Kyle Hyde 为了追寻自己的兄弟兼无间道 Bradely 三年前背叛自己的真相，而来到黄昏旅馆。住下之后却发现旅馆里面的每一个人，包括旅馆东家，都与 Bradely 当年的案件有所关联。故事就此展开。

本来我对文字游戏的印象，就有且仅有《秋之回忆》，跟漂亮女孩聊天，养成，然后跟自己恋爱啥的，不感兴趣。毕竟我是手残志坚的自虐党啊，要玩当然是黑魂、血源、仁王什么的！

六月份的时候突然间有一天看视频，被安利了一个叫《流言侦探》的国产手游，结果觉得这种形式的游戏还蛮轻松的。等车等人等上菜的时候，可以掏出手机来消磨下时间。不用双手操作，又可以随时停下来做正事，不占大脑带宽，不错。

继而疯狂搜索这类型的游戏，这一款是我看封面和游戏标题觉得蛮顺眼，于是开始玩的。结果无论是动画风格、配乐和故事情节，都十分对口味。结尾的时候上字幕，还专门为各个角色做了各自的 staring scene ，电影感十足。

虽然现在查到当时各大游戏媒体的评分都只有大概80/100，可是我却全程沉浸在这个故事里头，乐此不疲。从此，这种游戏类型将稳居在我的剁手榜上（好像突然找到了手残的原因？）。

可惜CING这个公司，在开发了黄昏旅馆两代之后就倒闭了（果然是很小众的游戏啊……），市面上推理探案类型的文字冒险游戏也是少之又少呢。



## `:// Music`

菅野洋子

动画《COWBOY BEBOP》、《攻壳机动队》、《黑之契约者》等各种动漫配乐，爵士、流行、交响、电子等等各种风格都信手拈来的音乐人。

Top 3 （个人主观评价，排名不分先后）
- The Real Folk Blues
- Inner Universe
- 接触 、



## `:// Misc`

1. 相差一个字母，意思却完全不同的单词：

Business / Busyness

Meditation / Medication

<br>

2. [日本刀是如何制作的](https://www.seriouseats.com/2018/07/japanese-knife-village.html)

![japanKnife](@weeklyImages/issue1/japanKnife.jpg)
Daniel Gritzer

意外地从一个讲吃的网站看到一篇讲刀具的文章。作者也亲身到日本的这个刀具村进行了实况了解。文章中说，当下日本的刀具，已经融合了很多西方道具的特点，变成 hybrid 的了。唯一能够区分日本刀具和西方刀具的点，就是这些刀具，是日本产的而已。

作者身处的这个 Takefu Knife Village，在日本福井越前市。相传 1337年，京都有名的刀匠为了寻找适合制刀的地方而来到这里，制作刀剑的同时，也教附近的村民制作镰刀，技艺流传至今。

当地的每个制刀厂，都有自己的制刀风格，也会根据自家的设计和偏好，向当地的钢材厂购买不一样的钢材。

最让我惊讶的是，根据这篇文章的采访，这里出产的每一把刀具，都是纯人手制作的，完全没有机械化的步骤，然而他采访的这家刀厂的主刀匠（master knife-maker）竟然有3000单订单在等着！看来高质量的东西，即使贵，即使要等，也还是有很多人愿意埋单的呢。

<CopyRights />