# MetaMask 插件的安装和账户注册

<PublishDate date='2018-12-07' />

::: tip NOTE
此流程需要梯子
:::

## 为什么要安装这个插件？

MetaMask 为普通用户提供了一个他们能看懂的、易用的UI界面。

更重要的是，安装了这个插件之后，它会在浏览器中注入 web3.js 库。

目前只有通过 web3.js 的 API，网页端才能与智能合约进行交互。可以把它理解为前端界面和 Ethereum 之间的桥梁。

（代码中也有对未安装 MetaMask 插件的情况进行判断，同时给出了替代方案，然而这种方案一直报错，所以最佳的方案，还是安装一下插件。）

## 如何安装

到 [MetaMask 官网](https://metamask.io/)首页，点击 GET CHROME EXTENSION 

![metamask-site](@dappImages/metamask/metamask-site.png)

或者，直接到 [Chrome Web Store](https://chrome.google.com/webstore/category/extensions) 搜索 MetaMask 插件

![chrome-web-store](@dappImages/metamask/chrome-web-store.png)

## 账号注册

### Step 1 设置账号的密码

为你的账号设置密码（最少8位），并确认。

![create-password](@dappImages/metamask/create-password.png)

### Step 2 账号头像

目前账号图像是由 MetaMask 生成的，还不能上传自定义头像。

![account-image](@dappImages/metamask/account-image.png)

### Step 3 各种条款及说明

1. 使用条款

![terms-of-use](@dappImages/metamask/terms-of-use.png)

2. 隐私声明 

安装了插件之后，只要插件是处于登录状态，使用了 Web.js 的网站，就可以拿到你的账号地址，所以这里提醒用户，在不需要用到 MetaMask 的时候，就登出。

![privacy-notice](@dappImages/metamask/privacy-notice.png)

3. 钓鱼警告

已经出现过知名网站，如 BTC Manager 和 Games Workshop，用户账号被盗用。情况为：页面上出现伪造的 MetaMask 窗口，让用户输入他们的账号助记词。

MetaMask 绝对不会这样自动打开。

![phishing-warning](@dappImages/metamask/phishing-warning.png)

### Step 4 12个随机英文助记词

Ethereum 的账号地址，都是一串复杂的英文数字组合，例如下面这样：

**0x3EEAe60A57E620AB31cE3fcf021D44Cf6d371d75**

普通人想要记住是很难的。所以 MetaMask 就想出了这个方法，用12个简单的英文单词来辅助。

有了这12个英文助记词，可以让用户在任何地方恢复自己的账户。

比如，在你信任的网站上使用 MetaMask 账号。

比如，当你重新安装 MetaMask 插件之后，需要使用你原来的账号。

比如，你换了一台新电脑，所有东西都是新的，你需要恢复自己原来的账号。

所以，如果被任何人拿到了你这12个助记词，你的账号就算是没了。右手边的 Tips 也提供了好几种安全记录助记词的方法，还提供了下下载链接。

![secret-backup-phrase](@dappImages/metamask/secret-backup-phrase.png)

### Step 5 确认12个英文助记词

按顺序选出刚才的12个英文助记词。

![confirm-backup-phrase](@dappImages/metamask/confirm-backup-phrase.png)

至此，MetaMask 插件的安装和账号注册流程已完成。

<CopyRights />