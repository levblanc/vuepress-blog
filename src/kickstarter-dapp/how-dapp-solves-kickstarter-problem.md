# Kickstarter Dapp 解决了什么问题？

<PublishDate date='2018-12-06' />

## 当前众筹网站的问题

现在的众筹网站，项目的发起者发起项目之后，支持者的钱会直接进入平台的账户内。

![1](@dappImages/dapp-solves-problem/1.png)

当项目筹集的资金达到了最初设置的额度，发起者就可以直接从账户提钱了。这时候，如果发起者打坏主意，拿了钱之后不做事，支持者的钱就等于打水漂了。

![2](@dappImages/dapp-solves-problem/2.png)

## 智能合约如何解决问题

这个简易版的 Dapp 就是尝试用 Ethereum 的智能合约来解决这个问题。

大家的钱先是存到了合约中。

![3](@dappImages/dapp-solves-problem/3.png)

项目的发起者可以提起各种转账请求。新建请求的时候，必须说明要转账多少钱，用于做什么事。每个支持者，对于转账请求，也各有一票的投票权。

如果得不到超过半数（支持者总人数的半数）的同意票，那么这个请求就不能发起转账请求。

![4](@dappImages/dapp-solves-problem/4.png)

相反，如果同意的票数过半，那么这个请求就被通过了。

![5](@dappImages/dapp-solves-problem/5.png)

项目发起者可以执行转账。

![6](@dappImages/dapp-solves-problem/6.png)

## 智能合约和平台账户的区别

看到这里，你可能会问智能合约和平台账户有什么区别？

为什么用户先转账到智能合约里面，就比原来转到平台账户要好呢？

如果你已经跟随 [MetaMask 插件的安装和账户注册](https://levblanc.com/kickstarter-dapp/metamask-install-and-create-account.html) 的步骤，注册过 MetaMask 账号，你会发现每个账号其实就是一个地址，像这样：

**0x3EEAe60A57E620AB31cE3fcf021D44Cf6d371d75**

在 Ethereum 上，有两类地址都可以进行收款和转账。个人账号是其中一种地址类型，而另外一种类型，就是智能合约。

个人可以控制自己的账号，而智能合约，是由程序控制的。所以，把钱转入智能合约中之后，除非程序逻辑通过，否则钱是不能被转走的。上述的投票步骤，相对严格地限定了转账规则，从而就解决了项目发起人一次性卷款而逃的问题。

<CopyRights />