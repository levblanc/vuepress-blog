---
title: useful tips for front end development
---

# useful tips for front end development

## update versions of all dependencies in a project

当你 clone 了一个项目，希望有个工具可以帮你检查一下项目里面用到的依赖包新旧程度如何，

`npm-check-updates` 就是你要找的工具！

```bash
$ npm install -g npm-check-updates
```

它可以对比出目前项目的版本，和最新的版本：

```bash
$ ncu
Checking package.json
[====================] 5/5 100%
 
 express           4.12.x  →   4.13.x
 multer            ^0.1.8  →   ^1.0.1
 react-bootstrap  ^0.22.6  →  ^0.24.0
 react-a11y        ^0.1.1  →   ^0.2.6
 webpack          ~1.9.10  →  ~1.10.5
 
Run ncu -u to upgrade package.json
```

如果想要一键升级，直接`ncu -u`，然后跟着提示走就行了。

如果只想升级部分到最新的版本，也可以手动按照提示的最新版本来升级。

reference: [How do I update each dependency in package.json to the latest version?](https://stackoverflow.com/questions/16073603/how-do-i-update-each-dependency-in-package-json-to-the-latest-version)