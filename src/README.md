---
home: true
heroImage: /connectTheDots.png
footer: Copyright © 2018 - present Levblanc
---

<!-- 欢迎光临我的个人博客，我是留白。是一名爱折腾的程序员。 -->
<!-- 常用网络ID：levblanc，昵称：留白shiye。 -->

<!-- 作为一名爱折腾的程序员，我有5年开发的经验。欢迎查看[我的简历](https://levblanc.com/resume/)，里面有更详细的技术栈和项目经历。 -->

<!-- - 后端可使用 Golang 或 Node.js 配合 MySQL 或 MongoDB 进行后端接口开发，在业余时间正积极学习更加进阶 Golang 知识 -->
<!-- - 前端可搭建基于vue.js的项目开发框架，微信内H5开发、移动端H5开发、PC端网页开发均有丰富经验。 -->

<!-- 业余兴趣是捣鼓各种新奇有趣的东西：
  - [Golang RESTful API](https://github.com/levblanc/golang-restful-api)开发、[Golang 基础](https://levblanc.com/golang/)
  - [网站的运维](https://levblanc.com/devops/)（暂时只是入了门）
  - 新奇有趣的 [区块链 Dapp](https://levblanc.com/kickstarter-dapp/) 开发
  - 从看别人的视频学习新知识，到自己做视频[讲解前端框架基础知识](https://levblanc.com/vue2-basics/)
  - [浏览器 DevTools UI 自定义](https://levblanc.com/tooling/firefox-customize-browser-ui.html)
  - [IDE 编程字体、主题优化](https://levblanc.com/tooling/vscode-fonts-customization.html)

希望这个博客里记录的经验，对你有所帮助。 -->
