# Vue.js 2.0 基础 API 系列文章合集

<PublishDate date='2017-01-26' />

:::tip NOTE
这份攻略系列是 2017 年在知乎专栏上的旧作。以下是原文。
:::

在大家的鞭策和鼓励下，这个基础API的系列终于完成了。所幸是没有真的更到一百期才完结（笑）。最初是因为觉得录视频好玩，才挖的这个坑。也想过中途放弃，关掉专栏，但由于当日马上收到了暖心的评论，所以坚持了下来。写最后组件的两篇文案加上录制视频的几天，是最难熬的时间（幸好公司放假了），真的是做到想吐。当时强烈地觉得做完就要马上开机打游戏，就好像高考前信誓旦旦说考完一定要把所有书都撕烂，结果完成的那一刻却只感觉到无比的平静，只是深舒了一口气。

接下来因为会跟一个MongoDB的课程，它设定了严格的作业提交时间，新的一周课程材料更新前就会关闭，所以我必须专心把这个课程搞定，专栏最快的更新也将要在三月份了。

关于更新的频率，因为是我自己一个人在做，文案视频都准备好了才发的话，最快也只能一周一更。最后这几期可以密集地更新，完全是因为公司放假了。所以在准备内容的那几天，觉得如果靠分享内容可以养活自己，其实也不错嘛（笑）。虽然在这个年代好像拼数量会比较有优势，也有人说过做原创的最后都不会有好下场，但我还是想做点自己觉得有趣的事情吧。

## 本系列文章链接

[Part 1 跟世界打个招呼吧](https://levblanc.com/vue2-basics/)

[Part 2 双向绑定和vue-devtools](https://levblanc.com/vue2-basics/two-way-binding-and-vue-devtools.html)

[Part 3 常用指令合集](https://levblanc.com/vue2-basics/directive.html)

[Part 4 v-bind绑定元素属性和样式](https://levblanc.com/vue2-basics/v-bind.html)

[Part 5 列表渲染和事件监听](https://levblanc.com/vue2-basics/list-and-event-listener.html)

[Part 6 计算属性](https://levblanc.com/vue2-basics/computed-properties.html)

[Part 7 组件](https://levblanc.com/vue2-basics/component.html)

[Part 8 组件通信](https://levblanc.com/vue2-basics/component-communication.html)

## 视频 Playlist

[VueJS 2.0 新手向视频攻略](http://space.bilibili.com/22590215/#/channel/detail?cid=3695)

<CopyRights />