---
title: Difference Between Class & Prototypal Inheritance
date: 2019-03-05 
updated: 2019-03-05
categories: [front-end]
tags: [js, 'classical inheritance', 'prototypal inheritance']
---

# Difference Between Class & Prototypal Inheritance

<PublishDate date='2019-03-05' />

prototypal object-oriented programming languages are more powerful than classical object-oriented programming languages because:

  - There is only one type of abstraction.
  - Generalizations are simply objects.

By now you must have realized the difference between classical inheritance and prototypal inheritance: 
  - Classical inheritance is limited to classes inheriting from other classes
  - Prototypal inheritance includes not only prototypes inheriting from other prototypes but also objects inheriting from prototypes.

## Classical Inheritance

```js
class Human {}

class Man extends Human {}

const john = new Man()
```

## Prototypal Inheritance

```js
const human = {}

const man = Object.create(human)
const john = Object.create(man)
```


---
References:

[1] [classical inheritance vs prototypal inheritance in javascript](https://stackoverflow.com/questions/19633762/classical-inheritance-vs-prototypal-inheritance-in-javascript)

<CopyRights />