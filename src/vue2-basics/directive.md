# Part 3 常用指令合集

<PublishDate date='2017-01-13' />

本期跟大家分享的，是Vue里面除了`v-bind`和`v-on`之外的指令。因为都挺简单的，所以就一次性解决了。

## v-text
直接把文字绑定到html。之前我们一直使用大胡子语法往html里面绑定数据，如果数据是纯字符串的话，也可以使用`v-text`。

```html
<div id="app">
    <h1 v-text="info"></h1>
</div>
```

当然你也可以往里面绑定其它类型的数据，只不过它们全部会以纯文字的形式呈现出来。

![info-number](@vue2basicsImgs/p3/info-number.png)

![info-array](@vue2basicsImgs/p3/info-array.png)


## v-html
相当于 JQ 的 innerHTML 方法，把数据中的 html 字符串嵌入到目标元素里面。

```html
<div id="app">
    <h1 v-html="html"></h1>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            html: '<small>使用v-html渲染</small>'
        }
    });
</script>
```

注意`v-text`和`v-html`两者，往html里插入数据以后新数据`<small>`和使用指令的元素`<h1>`之间的层级关系。

![v-html](@vue2basicsImgs/p3/v-html.png)

## v-if/v-else-if/v-else
我会把这三兄弟描述为：在html中进行状态控制的快捷指令。

下面用一个简单的例子来演示。假设我们的数据中`status`可能会返回`loading`、`ready`、`fail`三者之一，在页面需要根据这个字段来显示不同的东西：

```html
<div id="app">
    <div v-if="status === 'loading'">
        <div class="alert alert-info">loading</div>
    </div>
    <div v-else-if="status === 'ready'">
        <div class="alert alert-success">ready</div>
    </div>
    <div v-else>
        <div class="alert alert-danger">fail</div>
    </div>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            status: 'loading'
        }
    });
</script>
```

在console中直接改变`status`值，可以看到效果：

![status-loading](@vue2basicsImgs/p3/status-loading.png)

![status-ready](@vue2basicsImgs/p3/status-ready.png)

![status-fail](@vue2basicsImgs/p3/status-fail.png)

同时要留意，使用这三兄弟的时候，vue只会生成判定为`true`的那个节点：

![v-if-dom](@vue2basicsImgs/p3/v-if-dom.png)

![v-elseif-dom](@vue2basicsImgs/p3/v-elseif-dom.png)

![v-else-dom](@vue2basicsImgs/p3/v-else-dom.png)


## v-show
根据布尔值决定目标元素的 css `display`值。

```html
<div id="app">
    <h1 v-show="showHeader">{{ info }}</h1>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            info: 'hello hacker cafe',
            showHeader: true
        }
    });
</script>
```

![show-header](@vue2basicsImgs/p3/show-header.png)

修改`showHeader`的值

```bash
> app.showHeader = false
```

可以看到元素中增加了 css style，原来的文字也被隐藏了：

![hide-header](@vue2basicsImgs/p3/hide-header.png)

当然`showHeader`也可以赋值为其它东西，但是最后都会转换为布尔值来决定目标元素是否显示。


## v-show和v-if的区别
打开chrome devtools来查看两者渲染出来的dom。可以看到`v-show`只改变元素的css，但是`v-if`会决定是否生成这个 dom 节点（可查看上方截图）。


## v-once
只根据数据渲染一次。往后数据改变时，目标元素不再重新渲染。

我们用`v-text`和`v-once`进行对比：

```html
<div id="app">
    <div class="alert alert-danger" v-once>v-once: {{ once }}</div>
    <div class="alert alert-info">mustache: {{ once }}</div>
</div>

<!-- ... ... -->

<script type="text/javascript">
    var app = new Vue({
        el: '#app',
        data: {
            once: '遵命主人，只渲染一次'
        }
    });
</script>
```

保存代码并刷新浏览器后，两句话是一样的：

![v-once](@vue2basicsImgs/p3/v-once.png)

对数据中的`once`值进行修改后，使用`v-once`的元素不更新：

![v-once-update](@vue2basicsImgs/p3/v-once-update.png)


## 写在最后

源码地址：[https://github.com/levblanc/vue-2-basics](https://github.com/levblanc/vue-2-basics)

视频攻略：自制 [本章视频攻略](http://www.bilibili.com/video/av7592560) 在此。

<CopyRights />
